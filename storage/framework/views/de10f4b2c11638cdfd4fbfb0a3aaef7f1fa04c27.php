

<?php $__env->startSection('title'); ?> Profile | Amzapay <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('assets/dashboard/css/style1.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Profile</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Setting</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Profile</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            <div class="m-portlet m-portlet--full-height  ">
                <div class="m-portlet__body">
                    <div class="m-card-profile">
                        <div class="m-card-profile__title m--hide">
                            Your Profile
                        </div>
                        <form action="<?php echo e(url('profile-pic-update')); ?>" method="post" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">
                                    <?php if(Sentinel::getUser()->profile != ""): ?>
                                    <img src="<?php echo e(url('/assets/profiles/'.Sentinel::getUser()->profile)); ?>" id="profile_pic" />
                                    <?php else: ?>
                                    <img src="<?php echo e(url('/assets/app/media/img/users/user.jpg')); ?>" alt=""  id="profile_pic"/>
                                    <?php endif; ?>
                                </div>
                                <div class="image-upload">
                                    <label for="file-input">
                                        <div class="profile-edit custom-profile">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </div>
                                    </label>
                                    <input id="file-input" type="file" accept="image/x-png,image/gif,image/jpeg" name="profile_image"
                                        onchange="readURL(this);">
                                </div>
                            </div>
                            <div class="m-card-profile__details">
                                <span class="m-card-profile__name"><?php echo e(Sentinel::getUser()->user_name); ?></span>
                                <a href="" class="m-card-profile__email m-link"><?php echo e(Sentinel::getUser()->email); ?></a>
                            </div>
                            <div class="col-lg-12 m--align-right">
                                <button type="submit" class="btn m-btn--pill m-btn--air btn-outline-accent m-btn m-btn--outline-2x align-right">Update</button>               
                            </div>
                        </form>
                    </div>
                    <!-- <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                        <li class="m-nav__separator m-nav__separator--fit"></li>
                        <li class="m-nav__section m--hide">
                            <span class="m-nav__section-text">Section</span>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                <span class="m-nav__link-title">
                                    <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">My Profile</span>
                                        <span class="m-nav__link-badge"></span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-share"></i>
                                <span class="m-nav__link-text">Activity</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                <span class="m-nav__link-text">Messages</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-graphic-2"></i>
                                <span class="m-nav__link-text">Sales</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-time-3"></i>
                                <span class="m-nav__link-text">Events</span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                <span class="m-nav__link-text">Support</span>
                            </a>
                        </li>
                    </ul> -->
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link <?php if(!session('validator')): ?> active <?php endif; ?>" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                    <i class="flaticon-share m--hide"></i>
                                    Update Profile
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link <?php if(session('validator')): ?> active <?php endif; ?>" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                    Social Links
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane <?php if(!session('validator')): ?> show active <?php endif; ?>" id="m_user_profile_tab_1">
                        <div class="m-portlet__body">
                            <div>
                            <form class="m-form m-form--fit m-form--label-align-right" action="<?php echo e(url('update-profile')); ?>" method="post">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">1. Profile Info</h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">First Name</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="first_name" value="<?php echo e($user->first_name); ?>">
                                        <?php if($errors->has('first_name')): ?>
                                        <span class="text-danger">
                                            <strong><?php echo e($errors->first('first_name')); ?></strong>
                                        </span> 
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Last Name</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="last_name" value="<?php echo e($user->last_name); ?>">
                                        <?php if($errors->has('last_name')): ?>
                                        <span class="text-danger">
                                            <strong><?php echo e($errors->first('last_name')); ?></strong>
                                        </span> <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Email address</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="email" value="<?php echo e($user->email); ?>" readonly="">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">User Name</label>
                                    <div class="col-7">
                                        <div class="input-group m-input-group m-input-group--square">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                            <input class="form-control m-input" type="text" value="<?php echo e($user->user_name); ?>" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-10 m--align-right">
                                    <button type="submit" class="btn m-btn--pill m-btn--air btn-outline-accent m-btn m-btn--outline-2x align-right">Save changes</button>               
                                </div>
                            </form>
                        </div>
                            <form class="m-form m-form--fit m-form--label-align-right" action="<?php echo e(url('update-address')); ?>" method="post">
                                <?php echo e(csrf_field()); ?>

                                <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">2. Address</h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Address</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="address" value="<?php echo e($user->address); ?>">
                                        <?php if($errors->has('address')): ?><span class="text-danger"><strong>
                                            <?php echo e($errors->first('address')); ?></strong></span> 
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">City</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="city" value="<?php echo e($user->city); ?>">
                                        <?php if($errors->has('city')): ?>
                                        <span class="text-danger">
                                            <strong><?php echo e($errors->first('city')); ?></strong>
                                        </span> <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">State</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="state" value="<?php echo e($user->state); ?>">
                                        <?php if($errors->has('state')): ?>
                                        <span class="text-danger">
                                            <strong><?php echo e($errors->first('state')); ?></strong>
                                        </span> <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Country</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="country" value="<?php echo e($user->country); ?>">
                                        <?php if($errors->has('country')): ?>
                                        <span class="text-danger">
                                            <strong><?php echo e($errors->first('country')); ?></strong>
                                        </span> <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Postal Code</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" name="postal_code" value="<?php echo e($user->zip_code); ?>" min="6">
                                        <?php if($errors->has('postal_code')): ?>
                                        <span class="text-danger">
                                            <strong><?php echo e($errors->first('postal_code')); ?></strong>
                                        </span> <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-lg-10 m--align-right">
                                    <button type="submit" class="btn m-btn--pill m-btn--air btn-outline-accent m-btn m-btn--outline-2x align-right">Save changes</button>               
                                </div>
                            </form>
                            
                        </div>
                    </div>
                    <div class="tab-pane <?php if(session('validator')): ?> show active <?php endif; ?>" id="m_user_profile_tab_2">
                        <div class="m-portlet__body">
                            <form class="m-form m-form--fit m-form--label-align-right" action="<?php echo e(url('update-links')); ?>" method="post">
                                    <?php echo e(csrf_field()); ?>

                                    <div class="form-group m-form__group row">
                                        <div class="col-10 ml-auto">
                                            <h3 class="m-form__section">1. Social Links</h3>
                                        </div>
                                    </div>

                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Linkedin</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="linkedin" value="<?php echo e($setting->linkedin_link); ?>">
                                            <?php if($errors->has('linkedin')): ?>
                                            <span class="text-danger">
                                                <strong><?php echo e($errors->first('linkedin')); ?></strong>
                                            </span> <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Facebook</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="facebook" value="<?php echo e($setting->facebook_link); ?>">
                                            <?php if($errors->has('facebook')): ?>
                                            <span class="text-danger">
                                                <strong><?php echo e($errors->first('facebook')); ?></strong>
                                            </span> <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Twitter</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="twitter" value="<?php echo e($setting->twitter_link); ?>">
                                            <?php if($errors->has('twitter')): ?>
                                            <span class="text-danger">
                                                <strong><?php echo e($errors->first('twitter')); ?></strong>
                                            </span> <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Instagram</label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" name="instagram" value="<?php echo e($setting->instagram_link); ?>">
                                            <?php if($errors->has('instagram')): ?>
                                            <span class="text-danger">
                                                <strong><?php echo e($errors->first('instagram')); ?></strong>
                                            </span> <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 m--align-right">
                                        <button type="submit" class="btn m-btn--pill m-btn--air btn-outline-accent m-btn m-btn--outline-2x align-right">Save changes</button>               
                                    </div>
                                </form>
                        </div>
                    </div>
                    <div class="tab-pane " id="m_user_profile_tab_3">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#profile_pic').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>