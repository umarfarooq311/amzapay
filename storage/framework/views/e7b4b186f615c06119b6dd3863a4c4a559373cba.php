<?php $__env->startSection('title'); ?> Store | Products <?php $__env->stopSection(); ?>



<?php $__env->startSection('style'); ?>



<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>



    <div class="m-subheader ">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="m-subheader__title m-subheader__title--separator">Store</h3>

                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

                    <li class="m-nav__item m-nav__item--home">

                        <a href="#" class="m-nav__link m-nav__link--icon">

                            <i class="m-nav__link-icon la la-home"></i>

                        </a>

                    </li>

                    <li class="m-nav__separator">-</li>

                    <li class="m-nav__item">

                        <a href="" class="m-nav__link">

                            <span class="m-nav__link-text">Product</span>

                        </a>

                    </li>

                </ul>

            </div>

        </div>

    </div>



    <div class="m-content">
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                        <h3 class="m-portlet__head-text">
                            Create New Product
                        </h3>
                    </div>
                </div>
            </div>
        <?php echo $__env->make('admin.notifications.errors', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right" action="<?php echo e(route('products.store')); ?>" method="post" enctype="multipart/form-data">
                <?php echo e(csrf_field()); ?>

                <div class="m-portlet__body">
                    <div class="form-group m-form__group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Enter Product Name" name="product_name">
                    </div>

                    <div class="form-group m-form__group">
                        <label for="">Category</label>
                        <select name="category" class="form-control m-input m-input--solid" id="category">
                            <?php if(!empty($categories)): ?>
                                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                <option value="">No Category Available</option>
                            <?php endif; ?>
                        </select>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="">Sub Category</label>
                        <select name="sub_category" class="form-control m-input m-input--solid" id="sub-category">

                        </select>
                    </div>

                    <div id="product-attributes-container" style="padding: 20px;">
                        <h4>Product Attributes</h4>
                        <div class="row">
                            <div class="form-group col-sm-3">
                                <label for="exampleInputEmail1">Size</label>
                                <select name="sizes[]" class="form-control m-input m-input--solid" id="sizes">
                                    <?php if(!empty($sizes)): ?>
                                        <?php $__currentLoopData = $sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($size->id); ?>"><?php echo e($size->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </select>
                            </div>

                            <div class="form-group col-sm-3">
                                <label for="exampleInputEmail1">Color</label>
                                <select name="colors[]" class="form-control m-input m-input--solid" id="colors">
                                    <?php if(!empty($colors)): ?>
                                        <?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($color->id); ?>"><?php echo e($color->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </select>
                            </div>

                            <div class="form-group col-sm-2">
                                <label for="exampleInputEmail1">Quantity</label>
                                <input type="number" class="form-control m-input m-input--solid" placeholder="quantity" name="quantity[]">
                            </div>

                            <div class="form-group col-sm-2">
                                <label for="exampleInputEmail1">Unit Price</label>
                                <input type="number" class="form-control m-input m-input--solid" placeholder="100" name="unit_price[]">
                            </div>

                            <div class="from-group col-sm-2">
                                <button class="btn btn-success btn-sm" style="margin-top:27px;" id="add-more-attribute">Add</button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="exampleInputEmail1">Bulk</label><br>
                        <input type="radio" class="m-input m-input--solid" value="yes" name="bulk" onchange="showBulkPrices()"> Yes
                        <input type="radio" class="m-input m-input--solid" value="no" name="bulk" onchange="showBulkPrices()"> No
                    </div>

                    <div id="bulk-price-container" style="display:none; padding: 20px;">
                        <h4>Bulk Prices</h4>
                        <div class="row">
                                <div class="form-group col-sm-3">
                                    <label for="exampleInputEmail1">From</label>
                                    <input type="number" class="form-control m-input m-input--solid" placeholder="Unit From" name="from[]">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label for="exampleInputEmail1">To</label>
                                    <input type="number" class="form-control m-input m-input--solid" placeholder="Unit To" name="to[]">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label for="exampleInputEmail1">Bulk Unit Price</label>
                                    <input type="Number" class="form-control m-input m-input--solid" placeholder="Bulk Unit Price" name="price[]">
                                </div>

                                <div class="from-group col-sm-3">
                                    <button class="btn btn-success btn-sm" style="margin-top:27px;" id="add-more">Add</button>
                                </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="exampleInputEmail1">Feature</label><br>
                        <input type="radio" class="m-input m-input--solid" value="1" name="featured"> Yes
                        <input type="radio" class="m-input m-input--solid" value="0" name="featured"> No
                    </div>

                    <div class="form-group m-form__group">
                        <label for="editor">Description</label><br>
                        <textarea name="description" class="form-control" id="editor" cols="10" rows="10"></textarea>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="exampleInputEmail1">Main Image</label><br>
                        <input type="file" class="m-input m-input--solid" name="main_image" onchange="readURL(this)">
                        <img src="" alt="main image" id="img" style="height: 100px; width: 100px; display: none;">
                    </div>

                    <div class="form-group m-form__group">
                        <label for="exampleInputEmail1">Gallery Image</label><br>
                        <input type="file" class="m-input m-input--solid" name="gallery_images[]" id="gallery-photo-add" style="padding: 0px; cursor: pointer" multiple>
                        <div class="gallery">

                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-success">Create</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

    </div>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('script'); ?>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#m_table_1').DataTable();
           // $("#editor").redactor();

            //bulk price add more
            $(document).ready(function() {
                var max_fields      = 10; //maximum input boxes allowed
                var wrapper   		= $("#bulk-price-container"); //Fields wrapper

                var x = 1; //initlal text box count
                $("#add-more").click(function(e){ //on add input button click
                    e.preventDefault();
                    if(x < max_fields){ //max input box allowed
                        x++; //text box increment
                        $(wrapper).append(' <div class="row">\n' +
                            '<div class="form-group col-sm-3">\n' +
                            '    <label for="exampleInputEmail1">From</label>\n' +
                            '    <input type="number" class="form-control m-input m-input--solid" placeholder="Unit From" name="from[]">\n' +
                            '</div>\n' +
                            '\n' +
                            '<div class="form-group col-sm-3">\n' +
                            '    <label for="exampleInputEmail1">To</label>\n' +
                            '    <input type="number" class="form-control m-input m-input--solid" placeholder="Unit To" name="to[]">\n' +
                            '</div>\n' +
                            '\n' +
                            '<div class="form-group col-sm-3">\n' +
                            '     <label for="exampleInputEmail1">Bulk Unit Price</label>\n' +
                            '     <input type="Number" class="form-control m-input m-input--solid" placeholder="Bulk Unit Price" name="price[]">\n' +
                            '</div>\n' +
                            '\n' +
                            '<a href="#" class="remove_field" style="margin-top:27px;">x</a>' +
                            '</div>');}
                });

                $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                    e.preventDefault(); $(this).parent('div').remove(); x--;
                })

                //product attributes
                var max_fields_attributes = 10; //maximum input boxes allowed
                var wrapper_attribute = $("#product-attributes-container"); //Fields wrapper
                var y = 1; //initlal text box count
                $("#add-more-attribute").click(function(e){ //on add input button click
                    e.preventDefault();
                    if(y < max_fields_attributes){ //max input box allowed
                        y++; //text box increment
                        $(wrapper_attribute).append('<div class="row">\n' +
                            '                            <div class="form-group col-sm-3">\n' +
                            '                                <label for="exampleInputEmail1">Size</label>\n' +
                            '                                <select name="sizes[]" class="form-control m-input m-input--solid" id="sizes">\n' +
                            '                                    <?php if(!empty($sizes)): ?>\n' +
                            '                                        <?php $__currentLoopData = $sizes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $size): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>\n' +
                            '                                            <option value="<?php echo e($size->id); ?>"><?php echo e($size->name); ?></option>\n' +
                            '                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>\n' +
                            '                                    <?php endif; ?>\n' +
                            '                                </select>\n' +
                            '                            </div>\n' +
                            '\n' +
                            '                            <div class="form-group col-sm-3">\n' +
                            '                                <label for="exampleInputEmail1">Color</label>\n' +
                            '                                <select name="colors[]" class="form-control m-input m-input--solid" id="colors">\n' +
                            '                                    <?php if(!empty($colors)): ?>\n' +
                            '                                        <?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>\n' +
                            '                                            <option value="<?php echo e($color->id); ?>"><?php echo e($color->name); ?></option>\n' +
                            '                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>\n' +
                            '                                    <?php endif; ?>    \n' +
                            '                                </select>\n' +
                            '                            </div>\n' +
                            '\n' +
                            '                            <div class="form-group col-sm-2">\n' +
                            '                                <label for="exampleInputEmail1">Quantity</label>\n' +
                            '                                <input type="number" class="form-control m-input m-input--solid" placeholder="Unit To" name="quantity[]">\n' +
                            '                            </div>\n' +
                            '\n' +
                            '                            <div class="form-group col-sm-2">\n' +
                            '                                <label for="exampleInputEmail1">Unit Price</label>\n' +
                            '                                <input type="number" class="form-control m-input m-input--solid" placeholder="100" name="unit_price[]">\n' +
                            '                            </div>\n' +
                            '\n' +
                                                         '<a href="#" class="remove_field" style="margin-top:27px;">x</a>' +
                            '                        </div>');}
                });

                $(wrapper_attribute).on("click",".remove_field", function(e){ //user click on remove text
                    e.preventDefault(); $(this).parent('div').remove(); x--;
                })
            });

            $("#category").change( function(){
               var cat_id = $(this).val();
             
               $.ajax({
                   type: "GET",
                   url: "<?php echo e(route('getSubCategories')); ?>",
                   data: { data: cat_id},
                   dataType: 'JSON',
                   cache: true,
                   success: function(response){
                       if(response.status == 'success'){
                           html = '';
                           $(response.data).each( function(i,d){
                               html += '<option value="'+ d.id +'">'+ d.name +'</option>';
                           });

                           $("#sub-category").html(html);
                       }else{
                           toastr.warning('No sub category belong to this category');
                       }
                   }
               });
            })

            $(function() {
                var imagesPreview = function(input, placeToInsertImagePreview) {
                    if (input.files) {
                        var filesAmount = input.files.length;
                        for (i = 0; i < filesAmount; i++) {
                            var reader = new FileReader();
                            reader.onload = function(event) {
                                $($.parseHTML('<img style="width=100px;height: 100px;margin: 10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            }
                            reader.readAsDataURL(input.files[i]);
                        }
                    }
                };

                $('#gallery-photo-add').on('change', function() {
                    $(".gallery").html('');
                    imagesPreview(this, 'div.gallery');
                });
            });
        });

        //show the bulk prices
        function showBulkPrices(){
            var is_bulk = $("input[name = 'bulk']:checked").val();
            if(is_bulk === "yes"){
                $("#bulk-price-container").show();
            }else{
                $("#bulk-price-container").hide();
            }
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img').attr('src', e.target.result);
                    $('#img').css("display","block");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        function confirmDelete() {
            var r = confirm("Are you sure you want to perform this action");
            if (r === true) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>