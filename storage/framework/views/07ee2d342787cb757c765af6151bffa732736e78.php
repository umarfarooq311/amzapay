
<?php $__env->startSection('content'); ?>
    <!-- Document Wrapper
	============================================= -->
    <div id="wrapper" class="clearfix">

        <div id="home" class="page-section" style="position:absolute;top:0;left:0;width:100%;height:200px;z-index:-2;"></div>

        <section id="slider" class="slider-element slider-parallax full-screen with-header swiper_wrapper clearfix">

            <div class="slider-parallax-inner">

                <div class="swiper-container swiper-parent">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide dark">
                            <div class="container clearfix">
                                <div class="slider-caption slider-caption-center">
                                    <h2 data-caption-animate="fadeInUp">Welcome to Amza</h2>
                                    <p class="d-none d-sm-block" data-caption-animate="fadeInUp" data-caption-delay="200">A unique business once you have it you love it.</p>
                                </div>
                            </div>
                            <div class="video-wrap">
                                <video poster="images/videos/explore.jpg" preload="auto" loop autoplay muted>
                                    <source src='<?php echo e(asset('stock_video/3.mp4')); ?>' type='video/mp4' />
                                    <source src='<?php echo e(asset('stock_video/3.mp4')); ?>' type='video/webm' />
                                </video>
                                <div class="video-overlay" style="background-color: rgba(0,0,0,0.55);"></div>
                            </div>
                        </div>
                        <div class="swiper-slide dark">
                            <div class="container clearfix">
                                <div class="slider-caption slider-caption-center">
                                    <h2 data-caption-animate="fadeInUp">Consultancy and Investment</h2>
                                    <p class="d-none d-sm-block" data-caption-animate="fadeInUp" data-caption-delay="200">We provide professional consultancy service so as you give the comprehensive understanding to our realistic plans.</p>
                                </div>
                            </div>
                            <div class="video-wrap">
                                <video poster="images/videos/explore.jpg" preload="auto" loop autoplay muted>
                                    <source src='<?php echo e(asset('stock_video/2.mp4')); ?>' type='video/mp4' />
                                    <source src='<?php echo e(asset('stock_video/2.mp4')); ?>' type='video/webm' />
                                </video>
                                <div class="video-overlay" style="background-color: rgba(0,0,0,0.55);"></div>
                            </div>
                        </div>
                        <div class="swiper-slide dark">
                            <div class="container clearfix">
                                <div class="slider-caption slider-caption-center">
                                    <h2 data-caption-animate="fadeInUp">We have a telented team</h2>
                                    <p class="d-none d-sm-block" data-caption-animate="fadeInUp" data-caption-delay="200">Our team members are our assets.They bring the best of their skills and experience and add value to our business.</p>
                                </div>
                            </div>
                            <div class="video-wrap">
                                <video poster="images/videos/explore.jpg" preload="auto" loop autoplay muted>
                                    <source src='<?php echo e(asset('stock_video/1.mp4')); ?>' type='video/mp4' />
                                    <source src='<?php echo e(asset('stock_video/1.mp4')); ?>' type='video/webm' />
                                </video>
                                <div class="video-overlay" style="background-color: rgba(0,0,0,0.55);"></div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
                    <div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
                    <div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div>
                </div>

            </div>

        </section>

        <!-- Header
        ============================================= -->
        <header id="header" class="full-header">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
                    ============================================= -->
                    <div id="logo">
                        <a href="#" class="standard-logo" data-dark-logo="<?php echo e(asset('frontend_assets/assets/logo-large.png')); ?>"><img src="<?php echo e(asset('frontend_assets/assets/logo-large.png')); ?>" alt="Amza Logo"></a>
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
                    ============================================= -->
                    <nav id="primary-menu">

                        <ul class="one-page-menu" data-easing="easeInOutExpo" data-speed="1500">
                            <li><a href="#" data-href="#home"><div>Home</div></a></li>
                            <li><a href="#" data-href="#section-about"><div>About</div></a></li>
                            <li><a href="#" data-href="#section-team"><div>Team</div></a></li>
                            <li><a href="#" data-href="#section-pricing"><div>Packages</div></a></li>
                            
                            <li><a href="#" data-href="#section-testimonials" data-offset="60"><div>Testimonials</div></a></li>
                            <li><a href="#" data-href="#section-faqs"><div>Faqs</div></a></li>
                            <li><a href="#" data-href="#section-services"><div>Services</div></a></li>
                            <li><a href="#" data-href="#section-contact"><div>Contact</div></a></li>
                            <li><a href="/login"><div>Sign In</div></a></li>
                        </ul>

                        <!-- Top Search
                        ============================================= -->
                        <div id="top-search">
                            <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                            <form action="search.html" method="get">
                                <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                            </form>
                        </div><!-- #top-search end -->

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->

        <div class="clear"></div>

        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="content-wrap">

                <section id="section-about" class="page-section">

                    <div class="container clearfix">

                        <div class="heading-block center">
                            <h2>We are first <span>Mover</span></h2>
                            <span>A unique business pointing in the right direction.</span>
                        </div>

                        <div class="col_one_third nobottommargin">
                            <div class="feature-box media-box">
                                <div class="fbox-media">
                                    <img src="<?php echo e(asset('frontend_assets/images/services/1.jpg')); ?>" alt="Why choose Us?">
                                </div>
                                <div class="fbox-desc">
                                    <h3>Market Forecasting<span class="subtitle">Because we are Reliable.</span></h3>
                                    <p>Detailed analysis of market by our expert marketers! They forecast on the basis of demand and competition of different products.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col_one_third nobottommargin">
                            <div class="feature-box media-box">
                                <div class="fbox-media">
                                    <img src="<?php echo e(asset('frontend_assets/images/services/2.jpg')); ?>" alt="Why choose Us?">
                                </div>
                                <div class="fbox-desc">
                                    <h3>Products selling & Drop shipping<span class="subtitle">We Drop the best one</span></h3>
                                    <p>We analyze the trends in the markets, make vocasting reports and than we offer profitable sales and tracking plans to our clients.</p>
                                </div>
                            </div>
                        </div>

                        <div class="col_one_third nobottommargin col_last">
                            <div class="feature-box media-box">
                                <div class="fbox-media">
                                    <img src="<?php echo e(asset('frontend_assets/images/services/3.jpg')); ?>" alt="Why choose Us?">
                                </div>
                                <div class="fbox-desc">
                                    <h3>Market Research<span class="subtitle">We Provide best market solutions.</span></h3>
                                    <p>Our professional markerters are pro at researching the market
                                        so that every penny invested can pay back the big profit to our clients.</p>
                                </div>
                            </div>
                        </div>

                        <br><br>
                        <div class="heading-block center" style="margin-top:30px">
                            <h2>About <span>US</span></h2>
                            <span>The company has been working since last 5 years. The company has E-commerce stores on wide level On Amazon, EBay, Ali Baba and Shopify. We also selling services on Amazon allow top products like Grocery, Electronics, Books, sports & outdoors, shoes and more to sell professional services directly to Amazon. </span>
                        </div>

                        <video poster="<?php echo e(asset('frontend_assets/images/thumbnail/warehouse-1.png')); ?>" preload="auto" controls style="display: block; width: 100%;">
                            <!-- <source src='images/videos/explore.webm' type='video/webm' /> -->
                            <source src="<?php echo e(asset('frontend_assets/videos/warehouse-1.mp4')); ?>" type='video/mp4' />
                        </video>
                        <div class="clear"></div>

                        <div class="container clearfix" style="margin-top:30px;">
                            <div class="heading-block center" style="margin-top:30px;">
                                <h2>We are working for many years on <span>finance & e-commerce</span></h2>
                            </div>
                            <div class="col_one_third nobottommargin">
                                <div class="feature-box fbox-rounded fbox-effect">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-screen i-alt"></i></a>
                                    </div>
                                    <h3>About Company</h3>
                                    <p>The company is purchasing products directly from manufacturing companies and authentic online stores in bulk quantity and selling those products at high prices in the markets having high demand and then we will be sharing the profit. Don’t you think that it is a realistic investment idea as compared to any other investment plan being offered in the internet world?</p>
                                </div>
                            </div>

                            <div class="col_one_third nobottommargin">
                                <div class="feature-box fbox-rounded fbox-effect">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-eye i-alt"></i></a>
                                    </div>
                                    <h3>About Company</h3>
                                    <p>Do you think that you are not skilled enough to generate profit from Amazon, EBay or other online stores? Don’t you have guts to sell the products? Are you even fed up of affiliate marketing because it pays back less and it requires a lot of time and hard work? We buy the products in bulk and at sales prices and then we sell them in the market where there is big demand so that maximum profit can be generated. Every product that we will be purchasing and selling will be providing guaranteed profit.</p>
                                </div>
                            </div>

                            <div class="col_one_third col_last">
                                <div class="feature-box fbox-rounded fbox-effect">
                                    <div class="fbox-icon">
                                        <a href="#"><i class="icon-beaker i-alt"></i></a>
                                    </div>
                                    <h3>About Company</h3>
                                    <p>In simple words, we will be working as an agent. Purchases will be yours and off course profit will be yours. For our services, we will only charge fixed profit. This safest investment plan has been liked by all of our clients so far because they have been earning real profit without any fear of loss. If you are also interested to invest in a platform with no risk of loss and even no risk of “no profit” then delay no more to choose any of the Plan.</p>
                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>

                        <div class="heading-block center" style="margin-top:30px">
                            <h2>The Ultimated <span>Dropshiping</span></h2>
                            <span>Moreover, we have also been dealing in drop shipping business. We have a big number of clients who have been partnering with us in this type of business for many years because they find their investment 100% safe and profitable here.</span>
                        </div>

                        <div class=".d-none d-md-block text-center">
                            <img src="<?php echo e(asset('frontend_assets/assets/dropshipping.png')); ?>" style="margin:auto;" class="img-responsive">
                        </div>
                    </div>
                </section>

                <section id="section-team" class="page-section topmargin-lg">

                    <div class="heading-block center">
                        <h2>Our Team</h2>
                        <span>People who have contributed enormously to our Company.</span>
                    </div>

                    <div class="container clearfix">

                        <div class="col_one_third">

                            <div class="team">
                                <div class="team-image">
                                    <img src="<?php echo e(asset('frontend_assets/assets/team5.jpg')); ?>" alt="Amza-team-member">
                                </div>
                                <div class="team-desc">
                                    <div class="team-title"><h4>Feofan</h4><span>Manager</span></div>
                                    <h5>National University of Science and Technology (MISIS)</h5>
                                    <h6>Russia</h6>

                                </div>
                            </div>

                        </div>

                        <div class="col_one_third">

                            <div class="team">
                                <div class="team-image">
                                    <img src="<?php echo e(asset('frontend_assets/assets/team3.jpg')); ?>" alt="Amza-team-member">
                                </div>
                                <div class="team-desc">
                                    <div class="team-title"><h4>Markus Milo</h4><span>Web Developer</span></div>
                                    <h5>University of Konstanz  Informatics & Information Sciences</h5>
                                    <h6>Germany</h6>
                                </div>
                            </div>

                        </div>

                        <div class="col_one_third col_last">

                            <div class="team">
                                <div class="team-image">
                                    <img src="<?php echo e(asset('frontend_assets/assets/team2.jpg')); ?>" alt="Amza-team-member">
                                </div>
                                <div class="team-desc">
                                    <div class="team-title"><h4>Tyler Glines</h4><span>Sales Manager</span></div>
                                    <h5>University of Maryland United State of America (UOM)</h5>
                                    <h6>USA</h6>
                                </div>
                            </div>

                        </div>

                        <div class="container clearfix">

                            <div class="col_one_third">

                                <div class="team">
                                    <div class="team-image">
                                        <img src="<?php echo e(asset('frontend_assets/assets/team1.jpg')); ?>" alt="Amza-team-member">
                                    </div>
                                    <div class="team-desc">
                                        <div class="team-title"><h4>Harrison Henry </h4><span>Marketing Manager</span></div>
                                        <h5>La Trobe University Albury</h5>
                                        <h6>Australia</h6>
                                    </div>
                                </div>

                            </div>

                            <div class="col_one_third">

                                <div class="team">
                                    <div class="team-image">
                                        <img src="<?php echo e(asset('frontend_assets/assets/team8.jpg')); ?>" alt="Amza-team-member" >
                                    </div>
                                    <div class="team-desc">
                                        <div class="team-title"><h4>Grey Hundson</h4><span>Digital Marketing Expert</span></div>
                                        <h5>University of Illinois</h5>
                                        <h6>Chicago</h6>
                                    </div>
                                </div>

                            </div>

                            <div class="col_one_third col_last">

                                <div class="team">
                                    <div class="team-image">
                                        <img src="<?php echo e(asset('frontend_assets/assets/team7.jpg')); ?>" alt="Amza-team-member">
                                    </div>
                                    <div class="team-desc">
                                        <div class="team-title"><h4>Mary Jane</h4><span>Support Helper</span></div>
                                        <h5>University of Amsterdam</h5>
                                        <h6>Netherland</h6>
                                    </div>
                                </div>

                            </div>
                        </div>

                </section>

                <section id="section-pricing" class="page-section topmargin-lg">

                    <div class="heading-block center">
                        <h2>Packages</h2>
                        <span>We offer Flexible Package Options.</span>
                    </div>

                    <div class="container clearfix">

                        <div class="row pricing bottommargin clearfix">

                            <div class="col-lg-3" data-animate="fadeInLeft">

                                <div class="pricing-box">
                                    <div class="pricing-title">
                                        <h3>Toddling Investor Plan</h3>
                                    </div>
                                    <div class="pricing-price">
                                        <span class="price-unit">upto</span>1.2%<span class="price-tenure">/day</span>
                                    </div>
                                    <div class="pricing-features">
                                        <ul>
                                            <li><strong>Min Investment</strong> $100</li>
                                            <li><strong>Max Investment</strong> $1000</li>
                                            <li><strong>Profit</strong> 1.2% Daily</li>
                                            <li><strong>35 weeks</strong> License</li>
                                            <li><i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricing-action">
                                        <a href="#" class="btn btn-danger btn-block btn-lg">Sign Up</a>
                                    </div>
                                    <div class="pricing-action">
                                        <a href="#" data-toggle="modal" data-target="#myModal1" style="color:#6d6161">See Details</a>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-3" data-animate="fadeInDown"  data-delay="250">

                                <div class="pricing-box best-price">
                                    <div class="pricing-title">
                                        <h3>Hand's Shaker Plan</h3>
                                        <span>Most Popular</span>
                                    </div>
                                    <div class="pricing-price">
                                        <span class="price-unit">upto</span>1.2%<span class="price-tenure">/day</span>
                                    </div>
                                    <div class="pricing-features">
                                        <ul>
                                            <li><strong>Min Investment</strong> $1200</li>
                                            <li><strong>Max Investment</strong> $3000</li>
                                            <li><strong>Profit</strong> 1.2% Daily</li>
                                            <li><strong>40 weeks</strong> License</li>
                                            <li><i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricing-action">
                                        <a href="#" class="btn btn-danger btn-block btn-lg bgcolor border-color">Sign Up</a>
                                    </div>

                                    <div class="pricing-action">
                                        <a href="javascript:void()" data-toggle="modal" data-target="#myModal2" style="color:#6d6161">See Details</a>
                                    </div>

                                </div>

                            </div>

                            <div class="col-lg-3" data-animate="fadeInUp" data-delay="500">

                                <div class="pricing-box">
                                    <div class="pricing-title">
                                        <h3>Pro Investor's Plan</h3>
                                    </div>
                                    <div class="pricing-price">
                                        <span class="price-unit">upto</span>1.2%<span class="price-tenure">/day</span>
                                    </div>
                                    <div class="pricing-features">
                                        <ul>
                                            <li><strong>Min Investment</strong> $3500</li>
                                            <li><strong>Max Investment</strong> $6000</li>
                                            <li><strong>Profit</strong> 1.2% Daily</li>
                                            <li><strong>45 weeks</strong> License</li>
                                            <li><i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricing-action">
                                        <a href="#" class="btn btn-danger btn-block btn-lg">Sign Up</a>
                                    </div>
                                    <div class="pricing-action">
                                        <a href="javascript:void();" data-toggle="modal" data-target="#myModal3" style="color:#6d6161">See Details</a>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-3" data-animate="fadeInRight" data-delay="250">

                                <div class="pricing-box">
                                    <div class="pricing-title">
                                        <h3>Premium Investor's Plan</h3>
                                    </div>
                                    <div class="pricing-price">
                                        <span class="price-unit">upto</span>1.2%<span class="price-tenure">/day</span>
                                    </div>
                                    <div class="pricing-features">
                                        <ul>
                                            <li><strong>Min Investment</strong> $6500</li>
                                            <li><strong>Max Investment</strong> $30000</li>
                                            <li><strong>Profit</strong> 1.2% Daily</li>
                                            <li><strong>50 weeks</strong> License</li>
                                            <li><i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i>
                                                <i class="icon-star3"></i></li>
                                        </ul>
                                    </div>
                                    <div class="pricing-action">
                                        <a href="#" class="btn btn-danger btn-block btn-lg">Sign Up</a>
                                    </div>
                                    <div class="pricing-action">
                                        <a href="javascript:void()" data-toggle="modal" data-target="#myModal4" style="color:#6d6161">See Details</a>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </section>

                <section id="section-testimonials" class="page-section section parallax dark" style="background-image: url(<?php echo e(asset('frontend_assets/assets/testimonial.png')); ?>); padding: 200px 0;" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;">

                    <div class="container clearfix">

                        <div class="col_half nobottommargin">&nbsp;</div>

                        <div class="col_half nobottommargin col_last">

                            <div class="heading-block center">
                                <h4>What Clients say?</h4>
                                <span>Some of our Clients love us &amp; so we do!</span>
                            </div>

                            <div class="fslider testimonial testimonial-full nobgcolor noborder noshadow nopadding" data-arrows="false">
                                <div class="flexslider">
                                    <div class="slider-wrap">
                                        <div class="slide">
                                            <div class="testi-content">
                                                <p>Highly recommended this business to everyone. I have become a regular client here and I have taken the best decision of my life to invest my money here. I have been investing my profit as well so as to grow my business.</p>
                                                <div class="testi-meta">
                                                    Sara Metcalfe
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide">
                                            <div class="testi-content">
                                                <p>Everything is bad until proven good, I believed in this always. I am an investor in this platform l and it has been proven not only good but simply fantastic to me.</p>
                                                <div class="testi-meta">
                                                    Jemmy Persson
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide">
                                            <div class="testi-content">
                                                <p>In every business, investors take the risk and I thought of taking the risk to invest in this platform. I highly appreciate not only their services but also their loyalty and sincerity. I have been paid what was promised. Feeling so proud to be a partner with them! </p>
                                                <div class="testi-meta">
                                                    Adam Lewis
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide">
                                            <div class="testi-content">
                                                <p>In every business, investors take the risk and I thought of taking the risk to I have different portfolios of investment and in this time where global markets are going through recession phase, I am glad to invest in this platform because it is paying me back handsome amount of money every week.  </p>
                                                <div class="testi-meta">
                                                    Mat Willey
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </section>

                <section id="section-services" class="page-section topmargin-lg">
                    <div class="heading-block center">
                        <h2>Our <span>Services</span></h2>
                        <span>The services we are provided.</span>
                    </div>
                    <div class="container clearfix">
                        <div class="section parallax nobottommargin noborder dark" style="height: 450px; padding: 120px 0;">

                            <div class="vertical-middle center" style="z-index: 2;">
                                <div class="container clearfix" data-animate="fadeInUp">
                                    <div class="heading-block nobottomborder nobottommargin">

                                    </div>
                                </div>
                            </div>

                            <div class="video-wrap" style="z-index: 1;">
                                <video poster="<?php echo e(asset('frontend_assets/images/thumbnail/logo-start-1.png')); ?>" preload="auto" loop autoplay muted>
                                    <source src="<?php echo e(asset('frontend_assets/videos/business-1.mp4')); ?>" type='video/mp4' />
                                    <!-- <source src='videos/warehouse.mp4' type='video/webm' /> -->
                                </video>
                                <div class="video-overlay" style="background-color: rgba(0,0,0,0.1);"></div>
                            </div>
                        </div>

                        <div class="row bottommargin-lg common-height">
                            <div class="col-lg-4 dark col-padding ohidden" style="background-color: rgb(26, 188, 156); height: 361px;">
                                <div>
                                    <h3 class="uppercase" style="font-weight: 600;">Agency Services</h3>
                                    <p style="line-height: 1.8;">At that Investment, we will be working as an agent on your behalf. Investment will be yours and off course, business will be yours. We will be making all the activities on your behalf and we will off course be using your money in the best possible way so as to provide you with good amount of profit.</p>
                                    <a href="#" class="button button-border button-light button-rounded button-reveal tright uppercase nomargin"><i class="icon-angle-right"></i><span>Read More</span></a>
                                    <i class="icon-bulb bgicon"></i>
                                </div>
                            </div>

                            <div class="col-lg-4 dark col-padding ohidden" style="background-color: rgb(52, 73, 94); height: 361px;">
                                <div>
                                    <h3 class="uppercase" style="font-weight: 600;">Best Marketing services </h3>
                                    <p style="line-height: 1.8;">You don't need to worry if you don't have marketing skills because here at this Platform Investment, we have a team of marketers who have the best marketing skills and you have up to date marketing strategies...</p>
                                    <a href="#" class="button button-border button-light button-rounded uppercase nomargin">Read More</a>
                                    <i class="icon-cog bgicon"></i>
                                </div>
                            </div>

                            <div class="col-lg-4 dark col-padding ohidden" style="background-color: rgb(231, 76, 60); height: 361px;">
                                <div>
                                    <h3 class="uppercase" style="font-weight: 600;">Quick Customer Support Services</h3>
                                    <p style="line-height: 1.8;">You will find the best customer support here. We have the team of experienced and friendly numbers in our customer support. These members are always present to guide you in the best possible way and to answer your queries...</p>
                                    <a href="#" class="button button-border button-light button-rounded uppercase nomargin">Read More</a>
                                    <i class="icon-thumbs-up bgicon"></i>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </section>

                <section id="section-faqs" class="page-section topmargin-lg">
                    <div class="content-wrap">
                        <div class="container clearfix">

                            <div class="heading-block center">
                                <h2>FAQ'<span>s</span></h2>
                                <span>Here are some general question asked by our clients.</span>
                            </div>

                            <div class="divider"></div>

                            <div class="col_half nobottommargin">

                                <h4>General Questions  <small>(8)</small></h4>

                                <div class="accordion accordion-border clearfix" data-state="closed">

                                    <div class="acctitle"><i class="acc-closed icon-question-sign"></i><i class="acc-open icon-question-sign"></i>What are our Investment Plans ?</div>
                                    <div class="acc_content clearfix">Our Investment plans are very realistic source of investment where you can invest your money. In this investment platform, you will be offered different investment plans. You will be paying us any amount of money and out of that money; we will be purchasing high quality products at reasonable or discounted prices from very authentic online stores. Our marketers will analyse the markets where those products are highly demanded and are being sold at high rates. Then we will be selling those profits by creating profit margin and finally, we will be sharing the profits with our clients.</div>

                                    <div class="acctitle"><i class="acc-closed icon-comments-alt"></i><i class="acc-open icon-comments-alt"></i>How many investment plans are being offered ?</div>
                                    <div class="acc_content clearfix"><p>We are basically offering four different Investment plans.</p>
                                        <ul>
                                            <li><strong>Toddling investor’s plan :</strong> 1.2% Daily for 35 weeks investment limit is 100$-1000$</li>
                                            <li><strong>Hand shaker’s plan :</strong> 1.2% Daily for 40weeks investment limit is 1200$-3000$</li>
                                            <li><strong>Pro investor’s plan :</strong> 1.2% Daily for 45 weeks investment limit is 35000$-6000$</li>
                                            <li><strong>Premium Investor’s plan :</strong> 1.2% Daily for 50weeks investment limit is 6500$-30000$</li>
                                        </ul></div>

                                    <div class="acctitle"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-lock3"></i>Who can invest in this investment plan ?</div>
                                    <div class="acc_content clearfix">There isn’t any sort of restriction. Anyone who has some amount in hands and wants to put it in some business can invest here. If you don’t have any skills even then you can buy an investment plan.</div>

                                    <div class="acctitle"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-lock3"></i>How can you deposit ?</div>
                                    <div class="acc_content clearfix">To activate any plan first you have to deposit through any payment gateway whatever you feel easy to use.</div>

                                    <div class="acctitle"><i class="acc-closed icon-comments-alt"></i><i class="acc-open icon-lock3"></i>What payment methods we are offering ?</div>
                                    <div class="acc_content clearfix"><p>We are offering now three payment gateway</p>
                                        <ul>
                                            <li>Bit coin</li>
                                            <li>Perfect Money</li>
                                            <li>Bank wire (coming soon)</li>
                                        </ul>
                                    </div>

                                    <div class="acctitle"><i class="acc-closed icon-question-sign"></i><i class="acc-open icon-lock3"></i>What’s about profit ?</div>
                                    <div class="acc_content clearfix">You will be paid 1.2 % daily on your whole investment the duration will depend upon your investment plan.</div>

                                    <div class="acctitle"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-lock3"></i>After how long this investment will start generating profit ?</div>
                                    <div class="acc_content clearfix">Anyways, you don't need to wait for long like any other business but you will start generating profit on daily basis.</div>

                                    <div class="acctitle"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-lock3"></i>What’s about withdraw ?</div>
                                    <div class="acc_content clearfix">1.2 % profit will be show in your dashboard on daily basis but all your profit will be funded weekly through payment method which you use.</div>
                                </div>
                            </div>

                            <div class="col_half nobottommargin col_last">
                                <h4 class="topmargin">Referral Questions <small>(8)</small></h4>

                                <div class="accordion accordion-border nobottommargin clearfix" data-state="closed">

                                    <div class="acctitle"><i class="acc-closed icon-comments-alt"></i><i class="acc-open icon-lock3"></i>How much profit one can earn by investing here ?</div>
                                    <div class="acc_content clearfix">The amount of profit may fluctuate on the basis of different factors. For example, it depends on the amount of money that you will be investment. Big Investors will be earning big profit off course. It may also depend on the basis of profit margin a product will be providing. Some of the products can be sold at very expensive rates and they create big difference in purchase and sale amount. However, there are some products that do not create such huge margin. Either more or less but the profit will be guaranteed.</div>

                                    <div class="acctitle"><i class="acc-closed icon-download-alt"></i><i class="acc-open icon-download-alt"></i>Do we are offering any referral or affiliated program ?</div>
                                    <div class="acc_content clearfix">Yes off course its big opportunity to everyone to get more profit through referral program.</div>

                                    <div class="acctitle"><i class="acc-closed icon-ok"></i><i class="acc-open icon-ok"></i>Why we offering referral program ?</div>
                                    <div class="acc_content clearfix">Referral program is a opportunity for members to earn more profit and it will helps us to expand our business very quicklely and fast.</div>

                                    <div class="acctitle"><i class="acc-closed icon-credit"></i><i class="acc-open icon-credit"></i>What’s about referral beneficial ?</div>
                                    <div class="acc_content clearfix">The user will be paid 10% direct bonus on all investment of your referral at the spot.</div>

                                    <div class="acctitle"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-credit"></i>I don’t have any expertise but can I still invest in this plan ?</div>
                                    <div class="acc_content clearfix">Almost all of our clients are not having expertise or any skills to start their own business. Hence you can also invest here even if you don't have any expertise or skills. We just want you to invest your money here and all the business related skills will be provided by the expert marketers that we have in our team.</div>

                                    <div class="acctitle"><i class="acc-closed icon-credit"></i><i class="acc-open icon-credit"></i>How many clients have been working with you already ?</div>
                                    <div class="acc_content clearfix">Thousands of clients have already been working with ask and they have pooled their money here because they are earning a lot of profit. They keep on investing more and more money because they trust on us blindly. We have proven yourself and our skills and that's why we have earned good reputation among our clients.</div>

                                    <div class="acctitle"><i class="acc-closed icon-comments-alt"></i><i class="acc-open icon-credit"></i>Is it totally an online investment ?</div>
                                    <div class="acc_content clearfix">It will be a real business but all the business activities will be held online. We will find the buyers and the sellers of the products online, build long term relationship with them and we will be expanding our business. All these activities will be done online.</div>

                                    <div class="acctitle"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-credit"></i>What to do if we have any queries or questions ?</div>
                                    <div class="acc_content clearfix">We have provided our contact details on contact us page. You can contact us any time through email or contact number. Our customer support team will be there to answer to your questions or queries and to solve your problem. What do you want to know about Investment plans or you want to explore anything else, do not hesitate to contact us.</div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </section>

                <section id="section-contact" class="page-section">

                    <div class="heading-block title-center">
                        <h2>Get in Touch with us</h2>
                        <span>Still have Questions? Contact Us using the Form below.</span>
                    </div>

                    <div class="container clearfix">

                        <!-- Contact Form
                        ============================================= -->
                        <div class="col">

                            <div class="fancy-title title-dotted-border">
                                <h3>Send us an Email</h3>
                            </div>

                            <div class="contact-widget">

                                <div class="contact-form-result"></div>

                                <form class="nobottommargin" id="template-contactform" name="template-contactform" action="include/sendemail.php" method="post">

                                    <div class="form-process"></div>

                                    <div class="col_one_third">
                                        <label for="template-contactform-name">Name <small>*</small></label>
                                        <input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />
                                    </div>

                                    <div class="col_one_third">
                                        <label for="template-contactform-email">Email <small>*</small></label>
                                        <input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
                                    </div>

                                    <div class="col_one_third col_last">
                                        <label for="template-contactform-phone">Phone</label>
                                        <input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control" />
                                    </div>

                                    <div class="clear"></div>

                                    <div class="col_two_third">
                                        <label for="template-contactform-subject">Subject <small>*</small></label>
                                        <input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control" />
                                    </div>

                                    <div class="col_one_third col_last">
                                        <label for="template-contactform-service">Services</label>
                                        <select id="template-contactform-service" name="template-contactform-service" class="sm-form-control">
                                            <option value="">-- Select One --</option>
                                            <option value="Wordpress">Wordpress</option>
                                            <option value="PHP / MySQL">PHP / MySQL</option>
                                            <option value="HTML5 / CSS3">HTML5 / CSS3</option>
                                            <option value="Graphic Design">Graphic Design</option>
                                        </select>
                                    </div>

                                    <div class="clear"></div>

                                    <div class="col_full">
                                        <label for="template-contactform-message">Message <small>*</small></label>
                                        <textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
                                    </div>

                                    <div class="col_full hidden">
                                        <input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
                                    </div>

                                    <div class="col_full">
                                        <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Send Message</button>
                                    </div>

                                </form>

                            </div>


                        </div><!-- Contact Form End -->

                        <!-- Google Map
                        ============================================= -->
                        

                            


                        

                        <!-- Contact Info
                        ============================================= -->
                        <div class="col_full nobottommargin clearfix">
                            <div class="heading-block center">
                                <h2>Best Seller</h2>
                                <span>People who have contributed enormously to our Company.</span>
                            </div>
                            <div class="col_one_fourth">
                                <div class="feature-box fbox-center fbox-bg fbox-plain">
                                    <div class="fbox-icon">
                                        <a href="#"><img src="<?php echo e(asset('frontend_assets/assets/amzon.png')); ?>"></a>
                                    </div>
                                    <h3>Amazon Best Seller</h3>
                                </div>
                            </div>

                            <div class="col_one_fourth">
                                <div class="feature-box fbox-center fbox-bg fbox-plain">
                                    <div class="fbox-icon">
                                        <a href="#"><img src="<?php echo e(asset('frontend_assets/assets/alibabaicon.png')); ?>"></a>
                                    </div>
                                    <h3>We also deal with Ali-baba</h3>
                                </div>
                            </div>

                            <div class="col_one_fourth">
                                <div class="feature-box fbox-center fbox-bg fbox-plain">
                                    <div class="fbox-icon">
                                        <a href="#"><img src="<?php echo e(asset('frontend_assets/assets/ebay.png')); ?>"></a>
                                    </div>
                                    <h3>One of the best seller in Ebay</h3>
                                </div>
                            </div>

                            <div class="col_one_fourth col_last">
                                <div class="feature-box fbox-center fbox-bg fbox-plain">
                                    <div class="fbox-icon">
                                        <a href="#"><img src="<?php echo e(asset('frontend_assets/assets/walmart.png')); ?>"></a>
                                    </div>
                                    <h3>Walmart Best Dealer</h3>
                                </div>
                            </div>

                        </div><!-- Contact Info End -->

                    </div>
                </section>

            </div>
        </section><!-- #content end -->

        <!-- Footer
        ============================================= -->
        <footer id="footer" class="dark">

            <div class="container">

                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap clearfix">

                    <div class="col_two_third">

                        <div class="col_one_third">

                            <div class="widget clearfix">
                                
                                <h2>Amzapay</h2>
                                <p>A unique business pointing in the right direction</p>
                                <div style="background: url(<?php echo e(asset('frontend_assets/images/world-map.png')); ?>) no-repeat center center; background-size: 100%;">
                                    <address>
                                        <strong>Headquarters:</strong><br>
                                        404 Lacy Ln Las Vegas, Nevada<br>
                                    </address>
                                    <abbr title="Phone Number"><strong>Phone:</strong></abbr> +14805367373<br>
                                    <abbr title="Email Address"><strong>Email:</strong></abbr> support@amzapay.com
                                </div>

                            </div>

                        </div>

                        <div class="col_one_third">

                            <div class="widget widget_links clearfix">

                                <h4>Our Links</h4>

                                <ul>
                                    <li><a href="#" data-href="#home"><div>Home</div></a></li>
                                    <li><a href="#" data-href="#section-about"><div>About</div></a></li>
                                    <li><a href="#" data-href="#section-team"><div>Team</div></a></li>
                                    <li><a href="#" data-href="#section-pricing"><div>Packages</div></a></li>
                                    
                                    <li><a href="#" data-href="#section-testimonials" data-offset="60"><div>Testimonials</div></a></li>
                                    <li><a href="#" data-href="#section-faqs"><div>Faqs</div></a></li>
                                    <li><a href="#" data-href="#section-services"><div>Services</div></a></li>
                                    <li><a href="#" data-href="#section-contact"><div>Contact</div></a></li>
                                    <li><a href="<?php echo e(route('login')); ?>"><div>Login</div></a></li>
                                </ul>

                            </div>

                        </div>

                        <div class="col_one_third col_last">

                            <div class="widget clearfix">
                                <h4>Recent Posts</h4>

                                <div id="post-list-footer">
                                    <div class="spost clearfix">
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="#">Our Blog coming soon</a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li>10th July 2014</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="spost clearfix">
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="#">Get ready for something cool</a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li>10th July 2014</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="spost clearfix">
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="#">Our online store coming soon</a></h4>
                                            </div>
                                            <ul class="entry-meta">
                                                <li>10th July 2014</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col_one_third col_last">

                        <div class="widget clearfix" style="margin-bottom: -20px;">

                            <div class="row">

                                <p>The company has E-commerce stores on wide level On Amazon,
                                    EBay, Ali Baba and Shopify. We also selling services on Amazon allow top products like Grocery, Electronics,
                                    Books, sports & outdoors, shoes and more to sell professional services directly to Amazon.</p>

                            </div>

                        </div>

                        <div class="widget subscribe-widget clearfix">
                            <h5><strong>Subscribe</strong> to Our Newsletter to get Important News, Amazing Offers &amp; Inside Scoops:</h5>
                            <div class="widget-subscribe-form-result"></div>
                            <form id="widget-subscribe-form" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
                                <div class="input-group divcenter">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="icon-email2"></i></div>
                                    </div>
                                    <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email">
                                    <div class="input-group-append">
                                        <button class="btn btn-success" type="submit">Subscribe</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="widget clearfix" style="margin-bottom: -20px;">

                            <div class="row">

                                <div class="col-lg-6 clearfix bottommargin-sm">
                                    <a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>on Facebook</small></a>
                                </div>
                                <div class="col-lg-6 clearfix">
                                    <a href="#" class="social-icon si-dark si-colored si-rss nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-rss"></i>
                                        <i class="icon-rss"></i>
                                    </a>
                                    <a href="#"><small style="display: block; margin-top: 3px;"><strong>Subscribe</strong><br>to RSS Feeds</small></a>
                                </div>

                            </div>

                        </div>

                    </div>

                </div><!-- .footer-widgets-wrap end -->

            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        Copyrights &copy; 2014 All Rights Reserved by Amza Inc.<br>
                        <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
                    </div>

                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <a href="#" class="social-icon si-small si-borderless si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-gplus">
                                <i class="icon-gplus"></i>
                                <i class="icon-gplus"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-pinterest">
                                <i class="icon-pinterest"></i>
                                <i class="icon-pinterest"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-vimeo">
                                <i class="icon-vimeo"></i>
                                <i class="icon-vimeo"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-github">
                                <i class="icon-github"></i>
                                <i class="icon-github"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-yahoo">
                                <i class="icon-yahoo"></i>
                                <i class="icon-yahoo"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-linkedin">
                                <i class="icon-linkedin"></i>
                                <i class="icon-linkedin"></i>
                            </a>
                        </div>

                        <div class="clear"></div>

                        <i class="icon-envelope2"></i> info@Amza.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> +91-11-6541-6369 <span class="middot">&middot;</span> <i class="icon-skype2"></i> AmzaOnSkype
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- Packages Modals -->

    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-body">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">TODDLING INVESTOR PLAN</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h4>Complete Plan Details</h4>
                        <p>For small investors who have small amount to invest or who are going to invest for first time. The first plan that we are offering is for small investors who have limited amount of money in their hands. In this plan you can invest as minimum as $100 and as maximum as $1000. You will be receiving up to 1.2% profit per day. Profit summary is provided to the investors on daily basis. This plan is validate for 35 weeks. In this plan we have been dealing in toys, beauty items, video games, arts and crafts, kitchen items and books. </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-body">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">HAND'S SHAKER PLAN</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h4>Complete Plan Details</h4>
                        <p>Those people will move to this plan that will start trusting the company. They will like to have long term relation with your company. If your investment amount will range from $1200 to $3000 then you will be considered as a member of this plan. </p>Profit rate will be up to 1.2% per day and even in this plan, you will be able to receive profit on daily basics.  This plan is validate for 40 weeks. The products that we are dealing in according to this plan are clothes, apparels, textile & accessories, furniture, kids wear, books, shoes and security cameras.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-body">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">PRO INVESTOR'S PLAN</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h4>Complete Plan Details</h4>
                        <p>This plan is for professional investors. If you are interested in this plan and your investment range varies from $3500 to $6000. Plan will be expired in 45 weeks. You will be receiving up to 1.2% profit per day.</p> Off course, we have been selling all the products of above two plans here as well but besides that, this plan also includes the sales of mobiles, gadgets, smart devices, jewelry, smart watches, printing machines, Indoor plants, machines tools equipment, solar energy products  and herbal accessories. This plan has flexibility with some extra weeks.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-body">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">PREMIUM INVESTOR'S PLAN</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h4>Complete Plan Details</h4>
                        <p>Big investors who can bring in maximum amount. Become a premium investor and a leading investor by choosing this package! This investment plan that we are offering is for big investors who can invest from $6500 to $30,000. You will be receiving up to 1.2% profit per day. You will be investing big sum of money and off course, you will be receiving maximum profit in this plan.</p> Your money will be retained with the company for 50 weeks. We have been selling home exercise machines, supplements, portable devices, automotive, backpack, home security items, car accessories, industrial hardware, robotics, networking products, scanners, external components, cutting tools and all the products of other plans.
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>