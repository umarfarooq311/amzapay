<?php $__env->startSection('title'); ?> Products Reviews | Amzapay <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="m-subheader ">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="m-subheader__title m-subheader__title--separator">Store</h3>

                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

                    <li class="m-nav__item m-nav__item--home">

                        <a href="#" class="m-nav__link m-nav__link--icon">

                            <i class="m-nav__link-icon la la-home"></i>

                        </a>

                    </li>

                    <li class="m-nav__separator">-</li>

                    <li class="m-nav__item">

                        <a href="" class="m-nav__link">

                            <span class="m-nav__link-text">Product Reviews</span>

                        </a>

                    </li>

                </ul>

            </div>

        </div>

    </div>

    <div class="m-content">

        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            All Reviews
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="<?php echo e(route('product.reviews.create')); ?>" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                            <span>
                                                <i class="la la-plus"></i>
                                                <span>New record</span>
                                            </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <?php echo $__env->make('admin.notifications.errors', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="m-portlet__body">

                <div class="table-responsive">
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable text-center" id="m_table_1">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Product</th>
                            <th>User</th>
                            <th>Rating</th>
                            <th>Review</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($reviews)): ?>
                            <?php $__currentLoopData = $reviews; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $review): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($loop->iteration); ?></td>
                                    <td><?php echo e($review->product->name ?? '--'); ?></td>
                                    <?php if($review->user_id == null): ?>
                                            <td><?php echo e($review->username); ?></td>
                                        <?php else: ?>
                                            <td><?php echo e($review->user->name ?? '--'); ?></td>
                                    <?php endif; ?>
                                    <td><?php echo e($review->rating); ?></td>
                                    <td><?php echo e($review->review); ?></td>
                                    <td><?php echo e($review->created_at->diffForHumans()); ?></td>
                                    <td class="text-center">
                                        <a href="<?php echo e(route('product.reviews.edit', $review->id)); ?>" class="btn btn-success btn-sm">Edit</a> |
                                        <form action="<?php echo e(route('product.reviews.destroy',$review->id)); ?>" method="post">
                                            <?php echo e(csrf_field()); ?>

                                            <?php echo e(method_field('DELETE')); ?>

                                            <button class="btn btn-sm btn-danger" type="submit" style="margin:-51px 0 0 118px;">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                        </tbody>
                    </table>

                </div>

            </div>

        </div>

    </div>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('script'); ?>

    <script type="text/javascript">

        $(document).ready(function() {

            $('#m_table_1').DataTable();

        } );

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master_new', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>