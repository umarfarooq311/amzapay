<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />
    <link rel="icon" href="<?php echo e(asset('frontend_assets/assets/logo-large.png')); ?>" type="image/gif" sizes="16x16">

    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(asset('frontend_assets/css/bootstrap.css')); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(asset('frontend_assets/css/style.css')); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(asset('frontend_assets/css/swiper.css')); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(asset('frontend_assets/css/dark.css')); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(asset('frontend_assets/css/font-icons.css')); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(asset('frontend_assets/css/animate.css')); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo e(asset('frontend_assets/css/magnific-popup.css')); ?>" type="text/css" />

    <link rel="stylesheet" href="<?php echo e(asset('frontend_assets/css/responsive.css')); ?>" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Document Title
    ============================================= -->
    <title>Amzapay</title>

</head>
<body class="stretched">

    
    <?php echo $__env->yieldContent('content'); ?>

    <!-- Go To Top
	============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
    ============================================= -->
    <script src="<?php echo e(asset('frontend_assets/js/jquery.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend_assets/js/plugins.js')); ?>"></script>

    <!-- Footer Scripts
    ============================================= -->
    <script src="<?php echo e(asset('frontend_assets/js/functions.js')); ?>"></script>

</body>
</html>