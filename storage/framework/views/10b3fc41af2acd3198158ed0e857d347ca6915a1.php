<?php $__env->startSection('title'); ?> Reviews | Amzapay <?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

    <div class="m-subheader ">



        <div class="d-flex align-items-center">



            <div class="mr-auto">



                <h3 class="m-subheader__title m-subheader__title--separator">Store</h3>



                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">



                    <li class="m-nav__item m-nav__item--home">



                        <a href="#" class="m-nav__link m-nav__link--icon">



                            <i class="m-nav__link-icon la la-home"></i>



                        </a>



                    </li>



                    <li class="m-nav__separator">-</li>



                    <li class="m-nav__item">



                        <a href="" class="m-nav__link">



                            <span class="m-nav__link-text">Product Reviews</span>



                        </a>



                    </li>



                </ul>



            </div>



        </div>



    </div>







    <div class="m-content">

        <div class="m-portlet m-portlet--tab">

            <div class="m-portlet__head">

                <div class="m-portlet__head-caption">

                    <div class="m-portlet__head-title">

												<span class="m-portlet__head-icon m--hide">

													<i class="la la-gear"></i>

												</span>

                        <h3 class="m-portlet__head-text">

                            Create Review

                        </h3>

                    </div>

                </div>

            </div>

        <?php echo $__env->make('admin.notifications.errors', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <!--begin::Form-->

            <form class="m-form m-form--fit m-form--label-align-right" action="<?php echo e(route('product.reviews.store')); ?>" method="post">

                <?php echo e(csrf_field()); ?>


                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputEmail1">Product</label>
                                <select name="product_id" class="form-control m-input m-input--solid">
                                    <option value="">--Select Product--</option>
                                    <?php if(!empty($products)): ?>
                                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($key); ?>"><?php echo e($product); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                        <option value="">No Product Available</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="text" class="form-control m-input m-input--solid"  placeholder="Enter Username" name="username">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputEmail1">Rating(Out of 5)</label>
                                <input type="number" min="1" max="5" name="rating" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputEmail1">Review</label>
                                <textarea name="review" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">

                    <div class="m-form__actions">

                        <button type="submit" class="btn btn-success">Create</button>

                        <button type="reset" class="btn btn-secondary">Cancel</button>

                    </div>

                </div>

            </form>



            <!--end::Form-->

        </div>



    </div>



<?php $__env->stopSection(); ?>







<?php $__env->startSection('script'); ?>



    <script type="text/javascript">

        $(document).ready(function() {

            $('#m_table_1').DataTable();

        } );

    </script>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master_new', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>