<?php $__env->startSection('title'); ?> Dashboard | Amzapay <?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>





	<div class="m-content">

		<div class="col-xl-4">

			<div class="m-portlet m-portlet--mobile">



				<div class="m-portlet__head">



					<div class="m-portlet__head-caption">



						<div class="m-portlet__head-title">



							<h3 class="m-portlet__head-text">



								Investing Amount



							</h3>



						</div>



					</div>



				</div>







				<div class="m-portlet__body">



					<div class="row">



						<form action="https://perfectmoney.is/api/step1.asp" method="POST">

							<input type="hidden" name="PAYEE_ACCOUNT" value="<?php echo e($PAYEE_ACCOUNT); ?>">

							<input type="hidden" name="PAYEE_NAME" value="<?php echo e($PAYEE_NAME); ?>">

							<div class="form-group">

								<label>Enter Amount</label><span>(Minimun Amount 100)</span>

								<input type="number"  min="100" placeholder="100" name="PAYMENT_AMOUNT" value="<?php echo e($PAYMENT_AMOUNT); ?>" class="form-control">

							</div>

							<input type="hidden" name="PAYMENT_UNITS" value="<?php echo e($PAYMENT_UNITS); ?>">

							<input type="hidden" name="PAYMENT_URL" value="<?php echo e($PAYMENT_URL); ?>">

							<input type="hidden" name="NOPAYMENT_URL" value="<?php echo e($NOPAYMENT_URL); ?>">

							<?php if($PAYMENT_ID): ?>

								<input type="hidden" name="PAYMENT_ID" value="<?php echo e($PAYMENT_ID); ?>">

							<?php endif; ?>

							<?php if($STATUS_URL): ?>

								<input type="hidden" name="STATUS_URL" value="<?php echo e($STATUS_URL); ?>">

							<?php endif; ?>

							<?php if($PAYMENT_URL_METHOD): ?>

								<input type="hidden" name="PAYMENT_URL_METHOD" value="<?php echo e($PAYMENT_URL_METHOD); ?>">

							<?php endif; ?>

							<?php if( $NOPAYMENT_URL_METHOD ): ?>

								<input type="hidden" name="NOPAYMENT_URL_METHOD" value="<?php echo e($NOPAYMENT_URL_METHOD); ?>">

							<?php endif; ?>



							<?php if( $MEMO ): ?>

								<input type="hidden" name="SUGGESTED_MEMO" value="<?php echo e($MEMO); ?>">

							<?php endif; ?>

							<div class="from-group">

								<input type="submit" class="btn btn-sm btn-primary" value="Proceed">

							</div>

						</form>



					</div>



				</div>



			</div>

		</div>



	</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.back.master_new', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>