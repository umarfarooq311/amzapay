
<?php $__env->startSection('content'); ?>
    <header id="header" class="full-header">

        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <!-- Logo
                ============================================= -->
                <div id="logo">
                    <a href="<?php echo e(route('home')); ?>" class="standard-logo" data-dark-logo="<?php echo e(asset('frontend_assets/assets/logo-large.png')); ?>"><img src="<?php echo e(asset('frontend_assets/assets/logo-large.png')); ?>" alt="Amza Logo"></a>
                </div><!-- #logo end -->

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu">

                    <ul>
                        <li><a href="<?php echo e(route('home')); ?>" data-href="#home"><div>Home</div></a></li>
                        <li><a href="<?php echo e(route('home')); ?>" data-href="#section-about"><div>About</div></a></li>
                        <li><a href="<?php echo e(route('home')); ?>" data-href="#section-team"><div>Team</div></a></li>
                        <li><a href="<?php echo e(route('home')); ?>" data-href="#section-pricing"><div>Packages</div></a></li>
                        
                        <li><a href="<?php echo e(route('home')); ?>" data-href="#section-testimonials" data-offset="60"><div>Testimonials</div></a></li>
                        <li><a href="<?php echo e(route('home')); ?>" data-href="#section-faqs"><div>Faqs</div></a></li>
                        <li><a href="<?php echo e(route('home')); ?>" data-href="#section-services"><div>Services</div></a></li>
                        <li><a href="<?php echo e(route('home')); ?>" data-href="#section-contact"><div>Contact</div></a></li>
                    </ul>

                    <!-- Top Cart
                    ============================================= -->
                    
                        
                        
                            
                                
                            
                            
                                
                                    
                                        
                                    
                                    
                                        
                                        
                                        
                                    
                                
                                
                                    
                                        
                                    
                                    
                                        
                                        
                                        
                                    
                                
                            
                            
                                
                                
                            
                        
                    

                    <!-- Top Search
                    ============================================= -->
                    <div id="top-search">
                        <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                        <form action="search.html" method="get">
                            <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                        </form>
                    </div><!-- #top-search end -->

                </nav><!-- #primary-menu end -->

            </div>

        </div>

    </header>
    <section id="page-title">

        <div class="container clearfix">
            <h1>My Account</h1>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">Login</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">
                <?php if(session()->has('success')): ?>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="alert alert-success">
                                <?php echo e(session()->get('success')); ?>

                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="accordion accordion-lg divcenter nobottommargin clearfix" style="max-width: 550px;">

                    <div class="acctitle"><i class="acc-closed icon-lock3"></i><i class="acc-open icon-unlock"></i>Login
                        to your Account
                    </div>
                    <div class="acc_content clearfix">
                        <form id="login-form" name="login-form" class="nobottommargin" action="<?php echo e(route('login')); ?>" method="post">
                            <?php echo csrf_field(); ?>
                            <div class="col_full">
                                <label for="login-form-username">Email:</label>
                                <input type="text" id="login-form-username" name="email" value="<?php echo e(old('email')); ?>"
                                       class="form-control"/>
                            </div>

                            <div class="col_full">
                                <label for="login-form-password">Password:</label>
                                <input type="password" id="login-form-password" name="password" value="<?php echo e(old('password')); ?>"
                                       class="form-control"/>
                            </div>

                            <div class="col_full nobottommargin">
                                <button class="button button-3d button-black nomargin" id="login-form-submit"
                                        name="login-form-submit" value="login">Login
                                </button>
                                <a href="#" class="fright">Forgot Password?</a>
                            </div>
                        </form>
                    </div>

                    <div class="acctitle"><i class="acc-closed icon-user4"></i><i class="acc-open icon-ok-sign"></i>New
                        Signup? Register for an Account
                    </div>
                    <div class="acc_content clearfix">
                        <form id="register-form" name="register-form" class="nobottommargin" action="<?php echo e(route('register')); ?>" method="post">
                            <?php echo csrf_field(); ?>
                            <div class="col_full">
                                <label for="register-form-name">Name:</label>
                                <input type="text" id="register-form-name" name="user_name" value="<?php echo e(old('user_name')); ?>"
                                       class="form-control"/>
                            </div>

                            <div class="col_full">
                                <label for="register-form-email">Email Address:</label>
                                <input type="text" id="register-form-email" name="email" value="<?php echo e(old('email')); ?>"
                                       class="form-control"/>
                            </div>

                            
                                
                                
                                       
                            

                            
                                
                                
                                       
                            

                            <div class="col_full">
                                <label for="register-form-password">Choose Password:</label>
                                <input type="password" id="register-form-password" name="password"
                                       value="" class="form-control"/>
                            </div>

                            <div class="col_full">
                                <label for="register-form-repassword">Re-enter Password:</label>
                                <input type="password" id="register-form-repassword" name="confirm_password"
                                       value="" class="form-control"/>
                            </div>

                            <div class="col_full nobottommargin">
                                <button class="button button-3d button-black nomargin" id="register-form-submit"
                                        name="register-form-submit" value="register">Register Now
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>

    </section><!-- #content end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>