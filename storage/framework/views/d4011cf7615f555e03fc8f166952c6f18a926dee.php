

<?php $__env->startSection('title'); ?> Deposit <?php echo e(strtoupper($coin)); ?> | Perfect Money  <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<style type="text/css" media="screen">
.error {
    margin: 0px!important;
    color: #ff2b2b!important;
}
p {
    font-size: 16px!important;
}   
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Deposit <?php echo e(strtoupper($coin)); ?></h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Deposit via Perfect Money</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-xl-4">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                              Deposit with Perfect Money
                            </h3>
                        </div>
                    </div>
                </div>
                <form class="m-form m-form--fit m-form--label-align-right" id="pmform" action="https://perfectmoney.is/api/step1.asp" method="POST">
                    <?php echo e(csrf_field()); ?>

                    <div class="m-portlet__body">
                        <!-- <div class="form-group m-form__group m--margin-top-10">
                            <div class="alert m-alert m-alert--default" role="alert">
                                <code>Please enter your partner correct email.</code>
                            </div>
                        </div> -->
                        <div class="form-group m-form__group row">
                            
                            <label for="example-text-input" class="col-4 col-form-label">Deposit Amount</label>
                                
                            <div class="col-8">
                                <input class="form-control m-input" type="text" name="PAYMENT_AMOUNT" id="amount" placeholder="Deposit Amount" autocomplete="off" maxlength="10">
                                <?php if($errors->has('PAYMENT_AMOUNT')): ?><strong class="text-danger"><?php echo e($errors->first('PAYMENT_AMOUNT')); ?></strong><?php endif; ?>
                            </div>
                        </div>
                        <input type="hidden" name="PAYEE_ACCOUNT" value="<?php echo e($PAYEE_ACCOUNT); ?>">
                        <input type="hidden" name="PAYEE_NAME" value="<?php echo e($PAYEE_NAME); ?>">
                        <input type="hidden" name="PAYMENT_UNITS" value="<?php echo e($PAYMENT_UNITS); ?>">
                        <input type="hidden" name="PAYMENT_URL" value="<?php echo e($PAYMENT_URL); ?>">
                        <input type="hidden" name="NOPAYMENT_URL" value="<?php echo e($NOPAYMENT_URL); ?>">
                        <?php if($PAYMENT_ID): ?>
                            <input type="hidden" name="PAYMENT_ID" value="<?php echo e($PAYMENT_ID); ?>">
                        <?php endif; ?>
                        <?php if($STATUS_URL): ?>
                            <input type="hidden" name="STATUS_URL" value="<?php echo e($STATUS_URL); ?>">
                        <?php endif; ?>
                        <?php if($PAYMENT_URL_METHOD): ?>
                            <input type="hidden" name="PAYMENT_URL_METHOD" value="<?php echo e($PAYMENT_URL_METHOD); ?>">
                        <?php endif; ?>
                        <?php if( $NOPAYMENT_URL_METHOD ): ?>
                            <input type="hidden" name="NOPAYMENT_URL_METHOD" value="<?php echo e($NOPAYMENT_URL_METHOD); ?>">
                        <?php endif; ?>
                        
                        <?php if( $MEMO ): ?>
                            <input type="hidden" name="SUGGESTED_MEMO" value="<?php echo e($MEMO); ?>">
                        <?php endif; ?>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit ">
                        <div class="m-form__actions">
                            <div class="row">
                                <div class="col-12 m--align-right">
                                    <button type="submit" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-success m-btn--gradient-to-accent" id="submit">Deposit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-xl-8">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                <?php echo e(strtoupper($coin)); ?> Deposit
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="table-responsive">
                    <!--begin: Datatable -->
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="deposit-table">
                        <thead align="center">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Amount</th>            
                                <th scope="col">Payment Batch Num</th>            
                                <th scope="col">Transaction Id</th>            
                                <th scope="col">Payment Status</th>
                                <th scope="col">Payment Date</th>
                            </tr>
                        </thead>
                        <tbody align="center">
                            <?php $i=1; ?>
                            <?php $__currentLoopData = $deposit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $depo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e($i++); ?></td>
                                    <td><?php echo e($depo->amount); ?></td>
                                    <td><?php echo e($depo->payer_account); ?> <?php if($depo->payer_account == Null): ?> N/A <?php endif; ?></td>
                                    <td><?php echo e($depo->txid); ?> <?php if($depo->txid == Null): ?> N/A <?php endif; ?></td>
                                    <td><?php if($depo->status == 0): ?><span class="badge badge-warning">Pending</span>
                                        <?php elseif($depo->status == 1): ?><span class="badge badge-success">Complete</span>
                                        <?php elseif($depo->status == 2): ?><span class="badge badge-danger">Cancelled</span>
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo e($depo->created_at->format('d M Y')); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#deposit-table').DataTable();
} );
</script>
<script>
    $('#amount').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });

</script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {

    $('#pmform').validate({ // initialize the plugin
        rules: {
            PAYMENT_AMOUNT: {
                required: true,
            }
        }
    });

});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>