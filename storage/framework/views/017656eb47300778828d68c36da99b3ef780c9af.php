

<?php $__env->startSection('title'); ?> My Network | Amzapay <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo e(url('assets/dashboard/css/orgchart.css')); ?>"> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 


<link href="<?php echo e(URL::asset('assets/orgchart/getorgchart.css')); ?>" rel="stylesheet" />
<style>
#people {
    width: 100%;
    height: 100%;
}
.col-md-3.bonus-top {
    margin-top: 62px;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Network</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Referrals</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="row">
        <div class="col-md-9">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Referrals
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="<?php echo e(url('refferal-tree')); ?>" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    <span>
                                        <i class="fa fa-tree" aria-hidden="true"></i>
                                        <span>Show Tree</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="m-portlet__body table-responsive">

                    <table class="table table-striped- table-bordered table-hover table-checkable ">

                     <!--      <div class="m-portlet__body"> -->
                        <div class="table-responsive">
                            <table class="table table-striped- table-bordered table-hover table-checkable"  id="referral-table">
                                <thead align="center">
                                    <tr>
                                        <th width="20%">#</th>
                                        <th>Name</th>
                                        <th>Parent</th>
                                        <th>Position</th>
                                        <th>Assigning</th>
                                    </tr>
                                </thead>
                                <tbody align="center">
                                    <?php $i=1 ?>
                                    <?php $__currentLoopData = $referral_list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ru): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <tr>
                                        <td><?php echo e($i++); ?></td>
                                        <td class="text-uppercase"><?php echo e($ru->user_name); ?></td>
                                        <td> <?php echo e($ru->parentuser['user_name']); ?></td>
                                        <td><?php if($ru->position == 1): ?><span class="m-badge m-badge--brand m-badge--wide">Left</span><?php elseif($ru->position == 2): ?><span class="m-badge m-badge--primary m-badge--wide">Right</span><?php endif; ?></td>
                                        <td><?php if($ru->position == ''): ?><a href="<?php echo e(url('assign-position')); ?>/<?php echo e($ru->id); ?>" class="btn btn-primary">Assign</a><?php else: ?> Assigned <?php endif; ?></td>
                                    </tr>                                        
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            <div class="col-md-3">
                <div class="bonus-center" >
                    <div class="">
                       <div class="m-portlet m-portlet--fit ">
                        <div class="m-portlet__body">
                         <!--begin::Widget5-->
                         <div class="m-widget4 m-widget4--chart-bottom">

                          <div class="m-widget4__item">
                           <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                PV Points Left
                            </span>
                        </div>
                        <span class="m-widget4__ext">
                           <span class="m-widget4__number m--font-brand"><?php if(Sentinel::getUser()->left_pv > 0): ?> <?php echo e(Sentinel::getUser()->left_pv); ?> <?php else: ?> 0 <?php endif; ?></span>
                       </span>
                   </div>
                   <div class="m-widget4__item">
                       <div class="m-widget4__info">
                        <span class="m-widget4__title">
                            PV Points Right
                        </span>
                    </div>
                    <span class="m-widget4__ext">
                       <span class="m-widget4__number m--font-brand"><?php if(Sentinel::getUser()->right_pv > 0): ?> <?php echo e(Sentinel::getUser()->right_pv); ?> <?php else: ?> 0 <?php endif; ?></span>
                   </span>
               </div>
           </div>
           <!--end::Widget 5-->
       </div>
   </div>
</div>
<div class="">
    <div class="m-portlet m-portlet--fit ">
        <div class="m-portlet__body">
         <div class="m-widget4 m-widget4--chart-bottom">

          <div class="m-widget4__item">
           <div class="m-widget4__info">
            <span class="m-widget4__title">
                Total Final Bonus
            </span>
        </div>
        <span class="m-widget4__ext">
           <span class="m-widget4__number m--font-brand"><?php echo e($final_bonus); ?></span>
       </span>
   </div>
</div>
</div>
</div>
</div>
<div class="">
   <div class="m-portlet m-portlet--fit ">
    <div class="m-portlet__body">
     <!--begin::Widget5-->
     <div class="m-widget4 m-widget4--chart-bottom">

      <div class="m-widget4__item">
       <div class="m-widget4__info">
        <span class="m-widget4__title">
            Total Direct Bonus
        </span>
    </div>
    <span class="m-widget4__ext">
       <span class="m-widget4__number m--font-brand"><?php if(Sentinel::getUser()->referral_amount > 0): ?> <?php echo e(Sentinel::getUser()->referral_amount); ?> <?php else: ?> 0 <?php endif; ?></span>
   </span>
</div>
<div class="m-widget4__item">
   <div class="m-widget4__info">
    <span class="m-widget4__title">
        Total PV Bnous
    </span>
</div>
<span class="m-widget4__ext">
   <span class="m-widget4__number m--font-brand"><?php echo e($pv_bonus); ?></span>
</span>
</div>
</div>
<!--end::Widget 5-->
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<!-- <script src="<?php echo e(url('assets/dashboard/js/jquery.orgchart.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(url('assets/dashboard/js/tree.js')); ?>" type="text/javascript"></script> -->

    <script type="text/javascript">
        $(document).ready(function() {
            $('#referral-table').DataTable({
                "searching": false,
            });
        } );
    </script>
    <?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>