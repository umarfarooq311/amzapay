

<?php $__env->startSection('title'); ?> Deposit History | Amzapay <?php $__env->stopSection(); ?>

<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Deposit History</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="<?php echo e(url('/admin-dashboard')); ?>" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                        <span class="m-nav__link-text">Deposit History</span>
                    </a>
                </li>           
            </ul>
        </div>
    </div>
</div>

<div class="m-content">
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Deposit History
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="table-responsive">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable text-center" id="m_table_1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Payer Account</th>
                        <th>Type</th>
                        <th>Amount</th>            
                        <th>Payment Batch Num</th>            
                        <th>Payment Status</th>
                        <th>Payment Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; ?>
                    <?php $__currentLoopData = $deposit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $depo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($i++); ?></td>
                            <td><?php echo e(ucfirst($depo->deposit_user->user_name)); ?></td>
                            <td><?php if($depo->payer_account == Null): ?> N/A <?php else: ?><?php echo e(strtoupper($depo->payer_account)); ?><?php endif; ?></td>
                            <td><?php echo e(strtoupper($depo->coin)); ?></td>
                            <td><?php echo e($depo->amount); ?></td>
                            <td><?php if($depo->txid == Null): ?> N/A <?php else: ?><?php echo e($depo->txid); ?><?php endif; ?></td>
                            <td><?php if($depo->status == 0): ?><span class="m-badge m-badge--warning m-badge--wide"><i class="fas fa-spinner"></i> Pending</span>
                                <?php elseif($depo->status == 1): ?><span class="m-badge m-badge--success m-badge--wide"><i class="fa fa-check"></i> Complete</span>
                                <?php elseif($depo->status == 2): ?><span class="m-badge m-badge--danger m-badge--wide"><i class="fa fa-times"></i> Cancelled</span>
                                <?php endif; ?>
                            </td>
                            <td><?php echo e($depo->created_at->format('d M Y')); ?></td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript">
$(document).ready(function() {
    $('#m_table_1').DataTable();
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.back.master', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>