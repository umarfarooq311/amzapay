<?php

/*



|--------------------------------------------------------------------------



| Web Routes



|--------------------------------------------------------------------------



|



| Here is where you can register web routes for your application. These



| routes are loaded by the RouteServiceProvider within a group which



| contains the "web" middleware group. Now create something great!



|



*/

Route::get('/cache-clear', function(){
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    return 'Cache Clear';
});

Route::get('/', function () {

    return view('frontend.index');

});

Route::get('/home', function () {
    return view('frontend.index');
})->name('home');

Route::get('/blog', function () {
    return view('frontend.blog');
});

//Route for shop

Route::get('/shop', 'ShopController@index')->name('shop.index');
Route::get('/shop/single/{id}', 'ShopController@single')->name('shop.single');

Route::get('/shop/search_by_category/{category_id}', 'ShopController@categorySearch')->name('shop.search_by_category');

Route::get('/shop/search_by_color/{color_id}', 'ShopController@colorSearch')->name('shop.search_by_color');

Route::get('/shop/cartview', 'ShopController@cartView')->name('shop.cartview');

Route::get('/shop/add_to_cart/{id}', 'ShopController@cartAdd')->name('shop.add_to_cart');
Route::POST('/shop/add_to_cart_single/{id}', 'ShopController@cartAddsingle')->name('shop.add_to_cart_single');

Route::get('/shop/remove_item/{id}', 'ShopController@cartRemove')->name('shop.remove_item');

Route::get('/shop/decrement_item/{id}', 'ShopController@cartDecrement')->name('shop.decrement_item');

Route::get('/shop/increment_item/{id}', 'ShopController@cartIncrement')->name('shop.increment_item');
Route::get('/shop/update_cart', 'ShopController@cartUpdate')->name('shop.update_cart');
Route::get('/shop/checkout', 'ShopController@checkout')->name('shop.checkout');

Route::get('/single', function () {

    return view('frontend.single_product');
});

Route::get('our-products','HomeController@package');

Route::get('daily-roi','PackageController@dailyroi');

// registration
Route::post('register','RegistrationController@postRegister')->name('register');

Route::get('activate/{email}/{code}','RegistrationController@activationComplete');

// login

Route::get('/login', function(){

   return view('frontend.login');

});

Route::post('login','LoginController@login')->name('login');

Route::post('logout','LoginController@logout');

Route::post('forgot-password','ForgotPasswordController@forgotPassword')->name('forgot.password');

Route::get('reset-password/{email}/{resetcode}','ForgotPasswordController@restetPassword');

Route::post('reset-password/{email}/{resetcode}','ForgotPasswordController@postResetPassword');

Route::get('reset-password', function () {

    return view('mails.reset_password');
});

Route::get('about-us','HomeController@about_us');

Route::get('services','HomeController@services');

Route::post('subscribe-user','SubscribeController@sendmail');

Route::get('exist','SubscribeController@existpost');

Route::get('send','SubscribeController@send');

Route::post('general_message', 'GeneralMessageController@store')->name('general.message.store');

//admin

Route::group(['middleware'=>['admin']],function(){

	Route::get('admin-dashboard','DashboardController@index');

	// user manage
	Route::get('users-manage','AdminController@usersManage');

	Route::get('status-change/{id}','AdminController@userStatus');

	Route::get('user-delete/{id}','AdminController@userDelete');

	// profile
	Route::get('myprofile','ProfileController@adminprofile');

	Route::post('update-address','ProfileController@updateAddress');

	Route::post('update-profile','ProfileController@profileinfo');

	Route::post('profile-pic-update','ProfileController@profilepic'); //worke

	//password
	Route::get('password-change','ProfileController@passwordChange');

	Route::post('change/password','ProfileController@changepassword');

	//history
    Route::get('deposit-history','AdminController@depositHistory');

	Route::get('invest-history','AdminController@purchaseHistory');

	Route::get('withdraw-history','WithdrawalController@withdrawHistory');

	//company

	Route::get('company-setting','AdminController@company');

	Route::post('company-details','AdminController@companyDetails');

	Route::post('update-links','AdminController@linksupdated');

	Route::get('accounts-setting','AdminController@accountDetails');

	Route::post('update-accounts','AdminController@paymentAccounts');

	//withdraw
    Route::get('withdraw-manage','WithdrawalController@withdreaManage');

	Route::get('withdraw-reject/{id}','WithdrawalController@withdrawReject');

	Route::post('withdraw-approve','WithdrawalController@withdrawApprove');

	Route::get('withdraw-partner-approve/{id}','WithdrawalController@approveWithdraPartner');

    //my work admin side routes
    Route::get('/categories', 'CategoryController@index')->name('category.index');

    Route::get('/category/create', 'CategoryController@create')->name('category.create');

    Route::post('/category/store', 'CategoryController@store')->name('category.store');

    Route::get('/category/edit/{category}', 'CategoryController@edit')->name('category.edit');

    Route::put('/category/update/{category}', 'CategoryController@update')->name('category.update');

    Route::delete('/category/{category}', 'CategoryController@destroy')->name('category.destroy');

    Route::resource('/sub-categories', 'SubCategoryController');
    Route::get('/all/sub-categories/{category_id}', 'SubCategoryController@subcategoryshow')->name('all.sub-categories');

    Route::resource('/sizes', 'SizeController');

    Route::resource('/colors', 'ColorController');

    Route::resource('/products', 'ProductController');



    Route::get('/category/sub-categories', 'ProductController@getSubCategories')->name('getSubCategories');

    Route::get('product/reviews', 'ProductReviewController@index')->name('product.reviews.index');
    Route::get('product/reviews/create', 'ProductReviewController@create')->name('product.reviews.create');
    Route::post('product/reviews', 'ProductReviewController@store')->name('product.reviews.store');
    Route::get('product/reviews/{id}', 'ProductReviewController@edit')->name('product.reviews.edit');
    Route::put('product/reviews/{id}', 'ProductReviewController@update')->name('product.reviews.update');
    Route::delete('product/reviews/{id}', 'ProductReviewController@destroy')->name('product.reviews.destroy');

    Route::get('/hiding/transactions/purchase', 'HideController@loadAllPurchases')->name('hide.purchases.index');
    Route::get('/hiding/transactions/withdrawal', 'HideController@loadAllWithdrawals')->name('hide.withdrawals.index');
    Route::get('/purchase/hide/{purchase_id}', 'HideController@hidePurchase')->name('hide.purchase');
    Route::get('purchase/show/{purchase_id}', 'HideController@showPurchase')->name('show.purchase');
    Route::get('withdrawal/hide/{withdrawal_id}', 'HideController@hideWithdrawal')->name('hide.withdrawal');
    Route::get('withdrawal/show/{withdrawal_id}', 'HideController@showWithdrawal')->name('show.withdrawal');

});



//user

Route::group(['middleware'=>['user']],function(){

	Route::get('dashboard','DashboardController@index');

	// profile
	Route::get('profile','ProfileController@userprofile');
	Route::post('update-profile-info','ProfileController@profileinfo');
	Route::post('update-personal-info','ProfileController@personalinfo');
	Route::post('update-profile-pic','ProfileController@profilepic');

	Route::post('update/address','ProfileController@userAddressUpdate');

	Route::post('change-password','ProfileController@changepassword');

	Route::get('my-network','ReferralController@myNetwork');

	Route::get('my-network-data','ReferralController@myNetworkData');

	Route::get('package-list','PackageController@packages');

	Route::get('deposit','UserController@wallet');

	Route::get('invest','UserController@invested');

	Route::post('deposit-type','DepositController@depositType');

	//coinbase
	Route::get('deposit/{coin}','CoinpaymentController@coinaddress');

	Route::get('/call-back','CoinpaymentController@callback');

	// perfect money
	Route::get('deposit-pm/{coin}','DepositController@depositPm');

	Route::post('deposit-pm','DepositController@postDepositPm');

	Route::post('payment-success','DepositController@paymentSuccess');

	Route::get('payment-success','DepositController@paymentSuccess');

	Route::get('payment-fail','DepositController@paymentFail');

	Route::get('payment-status','DepositController@paymentStatus');

	//skrill
	Route::get('deposit-skrill/{coin}','DepositController@depositSkrill');

	Route::post('deposit-skrill-return','DepositController@depositSkrillreturn');

	Route::get('deposit-skrill-return','DepositController@depositSkrillreturn');

	Route::post('pay-skrill','DepositController@paySkrill');

	//withdraw

	Route::get('withdraw','WithdrawalController@withdraw');

	Route::post('withdraw-coin','WithdrawalController@withdraCoin');

	Route::get('withdraw/{type}/{coin}','WithdrawalController@withdrawal');

	Route::post('withdraw/{type}/{coin}','WithdrawalController@postWithdraw');

	//withdraw partner
	Route::get('withdraw-partner/{coin}','WithdrawalController@withdrawPartner');

	Route::post('withdraw-partner/{coin}','WithdrawalController@postwithdrawPartner');

	//package
	Route::post('package-select','PackageController@packageSelect')->name('package.select');

	Route::post('preview-package','PackageController@previewPackage')->name('package.preview');

	Route::get('balance','PackageController@currencyBalance');

	Route::get('pay/{id}','PackageController@pay');

	//reffreal
	Route::post('referred-position','ReferralController@referrelPosition');

	Route::get('assign-position/{id}','ReferralController@assignPosition');

	Route::get('refferal-tree','ReferralController@refferalDownline');

	Route::post('assign-position','ReferralController@postassignPosition');

	Route::post('alredy-assign-position','ReferralController@alredyassginPosition');

	Route::post('referral-packages','ReferralController@referralsPackages');

	//History
	// Route::get('history/deposit','UserController@depositHistory');
	// Route::get('history/withdraw','UserController@withdrawHistory');
	Route::get('history/commission','UserController@commissionHistory');

	//changepassword
	Route::get('setting/change-password','UserController@changePassword');

	Route::get('demo-tree/{id}','ReferralController@demotree');

	Route::get('gettreedata/{id}','ReferralController@gettreedata');

});

Route::get('coinbase-notification','CoinpaymentController@notification');
