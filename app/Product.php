<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $table = 'products';
    protected $guarded = [];

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function setSlugAttribute($value){
        return $this->attributes['slug'] = str_slug($value);
    }

    public function getIsFeatured(){
        return ($this->is_feature == 1) ? 'Yes' : 'No';
    }

    public function getIsBulk(){
        return ($this->is_bulk == 1) ? "Yes" : 'No';
    }

    public function getPriceWithSymbol(){
        return $this->unit_price.'$';
    }

    public function product_attributes(){
        return $this->hasMany(ProductAttribute::class, 'product_id', 'id');
    }

    public function bulk_products(){
        return $this->hasMany(BulkProduct::class, 'product_id', 'id');
    }

    public function setIsBulkAttribute($value){
        $this->attributes['is_bulk'] = ($value == 'yes') ? 1 : 0;
    }
}
