<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BulkProduct extends Model
{
    protected $table = 'bulk_product';
    protected $guarded = [];
    public function products(){
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}

