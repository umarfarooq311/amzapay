<?php

namespace App\Http\Controllers;

use App\Models\Purchase;
use App\Models\Withdrawal;
use Illuminate\Http\Request;

class HideController extends Controller
{
    public function loadAllPurchases(){
        $invest = Purchase::where('status', 0)->orderBy('id','desc')->get();

        return view('admin.hiding.purchase', compact('invest'));
    }

    public function hidePurchase(Purchase $purchase, $id){
        $purchase->find($id)->update([
            'is_hide' => 1
        ]);

        return back();
    }

    public function showPurchase(Purchase $purchase, $id){
        $purchase->find($id)->update([
            'is_hide' => 0
        ]);

        return back();
    }

    public function loadAllWithdrawals(){
        $withdraw = Withdrawal::orderBy('id', 'desc')->get();

        return view('admin.hiding.withdrawal', compact('withdraw'));
    }

    public function hideWithdrawal(Withdrawal $withdrawal, $id){
        $withdrawal->find($id)->update([
           'is_hide' => 1
        ]);

        return back();
    }

    public function showWithdrawal(Withdrawal $withdrawal, $id){
        $withdrawal->find($id)->update([
            'is_hide' => 0
        ]);

        return back();
    }
}
