<?php

namespace App\Http\Controllers;

use App\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Since;

class SizeController extends Controller
{
    public function index(){
        $sizes = Size::all();

        return view('admin.store.size.index', compact('sizes'));
    }

    public function create(){
        return view('admin.store.size.create');
    }

    public function store(Request $request){
        Validator::make($request->all(), [
           'size_name' => 'required|string|min:2'
        ]);

        Size::create([
            'name' => $request->size_name,
            'short' => $request->size_name
        ]);

        flash('Size has been added successfully', 'success');
        return redirect()->route('sizes.index');
    }

    public function edit($id){
        $size = Size::findOrFail($id);

        return view('admin.store.size.edit', compact('size'));
    }

    public function update(Request $request, $id){
        $size = Size::findOrFail($id);
        $size->update([
            'name' => $request->size_name,
            'short' => $request->size_name
        ]);
        flash('Size has been updated successfully', 'success');
        return redirect()->back();
    }
}
