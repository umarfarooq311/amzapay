<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubCategoryController extends Controller
{
    public function index(){
        $sub_categories = SubCategory::all();

        return view('admin.store.sub-category.index', compact('sub_categories'));
    }

    public function create(){
        $categories = Category::all();

        return view('admin.store.sub-category.create', compact('categories'));
    }

    public function store(Request $request){
        $this->validator($request->all())->validate();
        SubCategory::create([
            'category_id' => $request->category,
            'name' => $request->sub_category_name,
            'slug' => $request->sub_category_name,
        ]);

        flash('Sub category has been created successfully', 'success');
        return redirect()->route('sub-categories.index');
    }

    public function edit($id){
        $sub_category = SubCategory::findOrFail($id);
        $categories = Category::all();

        return view('admin.store.sub-category.edit', compact('categories', 'sub_category'));
    }

    public function update(Request $request, $id){
        $this->validator($request->all())->validate();
        $category = SubCategory::findOrFail($id);
        $category->update([
            'category_id' => $request->category,
            'name' => $request->sub_category_name,
            'slug' => $request->sub_category_name,
        ]);

        flash('Sub category has been updated successfully', 'success');
        return back();
    }

    public function destroy($id){
        $sub_category = SubCategory::findOrFail($id);
        $sub_category->delete();

        flash('Sub category has been deleted successfully', 'success');
        return back();
    }

    public function subcategoryshow(Request $request, $category_id){
        

        $sub_categories=SubCategory::where(['category_id' => $request->category_id])->get();
        
         return response()->json($sub_categories);

    }

    public function validator(Array $data){
        return Validator::make($data, [
            'sub_category_name' => 'string|required|min:3',
            'category' => 'integer|required|exists:categories,id',
        ]);
    }
}
