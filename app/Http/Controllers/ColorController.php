<?php

namespace App\Http\Controllers;

use App\Color;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ColorController extends Controller
{
    public function index(){
        $colors = Color::all();

        return view('admin.store.color.index', compact('colors'));
    }

    public function create(){
        return view('admin.store.color.create');
    }

    public function store(Request $request){
        Validator::make($request->all(), [
            'color_name' => 'required|string|min:2'
        ]);

        Color::create([
            'name' => $request->color_name,
        ]);

        flash('Color has been added successfully', 'success');
        return redirect()->route('colors.index');
    }

    public function edit($id){
        $color = Color::findOrFail($id);

        return view('admin.store.color.edit', compact('color'));
    }

    public function update(Request $request, $id){
        $color = Color::findOrFail($id);
        $color->update([
            'name' => $request->color_name,
        ]);
        flash('Color has been updated successfully', 'success');
        return redirect()->back();
    }
}
