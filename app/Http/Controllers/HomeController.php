<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Package;
use Sentinel;

class HomeController extends Controller
{
    public function about_us()
    {
    	return view('home_pages.about_us');
    }

    public function services()
    {
    	return view('home_pages.services');
    }

    public function package()
    {
    	$package = Package::get();
    	return view('home_pages.products',compact('package'));
    }
}
