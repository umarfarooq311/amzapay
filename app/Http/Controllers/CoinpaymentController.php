<?php

namespace App\Http\Controllers;

use App\Models\Deposit;
use Illuminate\Http\Request;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Address;
use Coinbase\Wallet\Enum\CurrencyCode;
use Coinbase\Wallet\Value\Money;
use App\Models\Setting;
use App\User;
use App\Models\CoinAddress;
use Sentinel;
use Storage;

use Coinbase\Wallet\Resource\Transaction;

class CoinpaymentController extends Controller
{
	public function coinaddress($coin)
    {
        $coin = strtoupper($coin);
        $setting = Setting::first();
        $user_id = Sentinel::getUser()->id;
        $deposit = Deposit::where('user_id',$user_id)->where('coin',$coin)->orderBy('id','desc')->get();
        $coinaddress = CoinAddress::where('user_id',$user_id)->first();

        if (!$coinaddress) {

            $configuration = Configuration::apiKey($setting->api_key,$setting->api_secret);
            $client = Client::create($configuration);
            $account = $client->getPrimaryAccount();
            $address = new Address([
                'name' => Sentinel::getUser()->email,
                'callback_url'  =>  url('/call-back'),
            ]);
            $adress = $client->createAccountAddress($account, $address);
                     
            $address_id = $adress->getId();
            $adress = $adress->getAddress();
            
            $coinaddress = new CoinAddress;
            $coinaddress->user_id = $user_id;
            $coinaddress->address = $adress;
            $coinaddress->address_id = $address_id;
            $coinaddress->coin = $coin;
            $coinaddress->save();

            $qrcode = $this->generateGoogleQRCodeUrl('https://chart.googleapis.com/', 'chart', 'chs=200x200&chld=M|0&cht=qr&chl=',$adress);
            // return view('user.deposit.deposit',compact('coin','qrcode','address'));
        }else{
            $qrcode = $this->generateGoogleQRCodeUrl('https://chart.googleapis.com/', 'chart', 'chs=200x200&chld=M|0&cht=qr&chl=',$coinaddress->address);
        }
        
        return view('user.deposit.deposit',compact('coin','qrcode','coinaddress','deposit'));
    }

	 public function generateGoogleQRCodeUrl($domain, $page, $queryParameters, $qrCodeUrl) {
        $url = $domain .
        rawurlencode($page) .
        '?' . $queryParameters .
        urlencode($qrCodeUrl);

        return $url;
    }

    public function callback(Request $request)
    {
    	Storage::disk('local')->put('deposit.txt', $request->all());
    	if ($request->address) {
    		
    		$address = CoinAddress::where('address',$request->address)->first();
    		if ($address) {
    			$deposit = new Deposit;
    			$deposit->user_id = $address->user_id;
    			$deposit->address = $request->address;
    			$deposit->coin = 'BTC';
    			$deposit->amount = $request->amount;
    			//$deposit->txid = $request->transaction->id;
    			$deposit->type = 'Coinbase';
	    		$deposit->status = 0;	    			
	    		$deposit->save();
	    		// if ($request->status == 'created') {
	    		// }
    		}
    	}
    }

    public function notification(){
        $setting = Setting::first();
        $configuration = Configuration::apiKey($setting->api_key,$setting->api_secret);
        $client = Client::create($configuration);
        $account = $client->getPrimaryAccount();
        
        $address = CoinAddress::get();
        foreach ($address as $value) {
            
            $address = $client->getAccountAddress($account,$value->address_id);
            $transactions = $client->getAddressTransactions($address);            
            $transactions = $client->decodeLastResponse();
            $hisotry = $transactions['data'];


            print_r($hisotry);
            foreach ($hisotry as $hist) {
                
                $deposit = Deposit::where('txid',$hist['id'])->first();
                 if ($deposit) {
                    if ($deposit->status!=1 && $deposit->status != 2) {
                       
                       if ($hist['status'] == 'pending') {
                            $deposit->status = 0;
                        }elseif ($hist['status'] == 'completed') {
                            $deposit->status = 1;

                            $user = User::where('id',$value->user_id)->first();
                            $user->btc_balance = $user->btc_balance + $deposit->amount;
                            $user->update();

                        }elseif ($hist['status'] == 'failed') {
                            $deposit->status = 2;
                        }elseif ($hist['status'] == 'canceled') {
                            $deposit->status = 2;
                        }elseif ($hist['status'] == 'expired') {
                            $deposit->status = 2;
                        }
                        $deposit->update();
                    }
                    
                }else{
                    $deposit = new Deposit;
                    $deposit->user_id = $value->user_id;
                    $deposit->address = $value->address;
                    $deposit->coin = $hist['amount']['currency'];
                    $deposit->amount = $hist['amount']['amount'];
                    $deposit->txid = $hist['id'];
                    $deposit->type = 'Coinbase';
                    if ($hist['status'] == 'pending') {
                        $deposit->status = 0;
                    }elseif ($hist['status'] == 'completed') {
                        $deposit->status = 1;

                        $user = User::where('id',$value->user_id)->first();
                        $user->btc_balance = $user->btc_balance + $deposit->amount;
                        $user->update();
                    }elseif ($hist['status'] == 'failed') {
                        $deposit->status = 2;
                    }elseif ($hist['status'] == 'canceled') {
                        $deposit->status = 2;
                    }elseif ($hist['status'] == 'expired') {
                        $deposit->status = 2;
                    }
                    $deposit->save();
                }
            }
        }
    }
}
