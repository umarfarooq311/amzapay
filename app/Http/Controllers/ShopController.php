<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Color;
use Cart;
use Auth;
use Session;
use App\ProductAttribute;
use App\BulkProduct;
use App\Order;
use App\Orderdetail;



class ShopController extends Controller
{
    public function index(){

		$all_products=Product::with('product_attributes')->where('is_feature', 1)->paginate(9);
		$all_categories= Category::All();
                $all_colors= Color::All();

    	return view('frontend.shop', compact('all_products','all_categories','all_colors'));
    }

    public function categorySearch(Request $request){

        $catogories_search= Product::with('product_attributes')->where('category_id',$request->category_id)->get();

        return response()->json($catogories_search);
    }

    public function colorSearch(Request $request){

        $color_search=ProductAttribute::with('products')->where('color_id',$request->color_id)->get();

        return response()->json($color_search);
    }

    public function cartView(){

        return view('frontend.cart');

    }

    public function cartAdd($id){

        $products=Product::with('product_attributes')->where('is_feature', 1)->find($id);

        $prices=$products->product_attributes;

        foreach ($prices as $p) {
        $price=$p->unit_price;

    }


        Cart::add(array(
            'id' => $products->id,
            'name' => $products->name,
            'price' => $price,
            'quantity' => 1,
            'image' => $products->main_image,
            'attributes' => array()

         ));


            return redirect()->back();

    }
    public function cartAddsingle(Request $request,$id){



        $products=Product::with('product_attributes')->where('is_feature', 1)->find($id);
        $quantity=$request->qunatity;
        $bulk=$products->is_bulk;
        $prices=$products->product_attributes;

        foreach ($prices as $price) {
        $price=$price->unit_price;

       }

        if($bulk==1){

            $bulk_product=BulkProduct::with('products')->where('product_id',$request->id)->get();

            foreach($bulk_product as $bulk){

                $from= $bulk->from;
                $to= $bulk->to;
                $bulk_price=$bulk->price;
            }



            if($quantity>=$from && $quantity<=$to){

                Cart::add(array(
            'id' => $products->id,
            'name' => $products->name,
            'price' => $bulk_price,
            'quantity' => $quantity,
            'image' => $products->main_image,
            'attributes' => array()

         ));

            }

            else{
                Cart::add(array(
            'id' => $products->id,
            'name' => $products->name,
            'price' => $price,
            'quantity' => $quantity,
            'image' => $products->main_image,
            'attributes' => array()

         ));

            }

        }

        else{

            Cart::add(array(
            'id' => $products->id,
            'name' => $products->name,
            'price' => $price,
            'quantity' => $quantity,
            'image' => $products->main_image,
            'attributes' => array()

         ));

        }

            return redirect()->back();

    }

    public function cartRemove($id){

          Cart::remove($id);
          Session::flash('message','removed');

          return redirect()->back();

    }

    public function cartDecrement($id){
          Cart::update($id, array('quantity' => -1,));

           return redirect()->back();
    }

    public function cartIncrement($id){
          Cart::update($id, array('quantity' => +1,));

           return redirect()->back();
    }
    public function cartUpdate(){

        Cart::clear();

        return redirect()->back();
    }

    public function single($id){
        $single_product=Product::with('product_attributes')->where('is_feature', 1)->find($id);
        return view('frontend.single', compact('single_product'));
    }

    public function checkout(){
        $cartCollection = Cart::getContent();
        $total_price=Cart::getTotal();
        $user_id=3;
        foreach ($cartCollection as $collection){

        $order_id=  Order::create([

                'user_id' => $user_id,

                'product_id' => $collection->id,

                'quantity' => $collection->quantity,

                'total_price' =>$total_price,

        ]);

            Orderdetail::create([

                 'order_id' => $order_id->id,

                 'name' => $collection->name,

                 'quantity' => $collection->quantity,

                 'price' => $collection->price,

                 'total_price' =>$total_price,

             ]);
       }

        Cart::clear();

        return redirect()->back();
    }

}
