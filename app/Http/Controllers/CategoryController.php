<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PhpParser\Node\Expr\Array_;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::all();

        return view('admin.store.category.index', compact('categories'));
    }

    public function create(){
        return view('admin.store.category.create');
    }

    public function store(Request $request){
        $this->validator($request->all())->validate();
        Category::create([
           'name' => $request->category_name,
           'slug' => $request->category_name,
           'is_published' => ($request->is_published == 'yes') ? 1 : 0
        ]);

        flash('Category has been created successfully', 'success');
        return redirect()->route('category.index');
    }

    public function edit($id){
        $category = Category::findOrFail($id);

        return view('admin.store.category.edit', compact('category'));
    }

    public function update(Request $request, $id){
        $this->validator($request->all())->validate();
        $cateory = Category::findOrFail($id);
        $cateory->update([
            'name' => $request->category_name,
            'slug' => $request->category_name,
            'is_published' => ($request->published == 'yes') ? 1 : 0
        ]);

        flash('Category has been updated successfully', 'success');
        return back();
    }

    public function destroy($id){
        $category = Category::findOrFail($id);
        $category->delete();

        flash('Category has been deleted successfully', 'success');
        return back();
    }

    public function validator(Array $data){
        return Validator::make($data, [
           'category_name' => 'string|required|min:3',
           'published' => 'string|required'
        ]);
    }
}
