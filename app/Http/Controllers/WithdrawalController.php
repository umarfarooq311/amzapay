<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Withdrawal;
use App\User;
use Sentinel;

class WithdrawalController extends Controller
{
    public function withdraw()
    {
    	return view('user.withdraw.withdraw');
    }

    public function withdraCoin(Request $request)
    {
        // return $request->all();
        $type = $request->withdraw;
        $coin = $request->currency;

        if($type == 'partner') {
            return redirect('withdraw-partner/'.$request->currency);
        }

        return redirect('withdraw/'.$type.'/'.$coin);
    }

    public function withdrawal($type,$coin)
    {   
    	$user = Sentinel::getUser();
        if ($type == 'partner') {

            $withdraw = Withdrawal::where('user_id',$user->id)->whereNotNull('partner_email')->where('coin',$coin)->where('type',$type)->orderBy('id')->get();
            return view('user.withdraw.withdraw_partner',compact('withdraw','coin','user'));
        }
        if ($coin == 'perfect_money') {
            $withdraw = Withdrawal::where('user_id',$user->id)->where('coin','PERFECT')->whereNull('partner_email')->where('type','Perfect Money Roi')->orderBy('id')->get();
        }elseif ($coin == 'coinbase') {
            $withdraw = Withdrawal::where('user_id',$user->id)->where('coin','COINBASE ROI')->whereNull('partner_email')->where('type','Coinbase Roi')->orderBy('id')->get();
        }else{

    	   $withdraw = Withdrawal::where('user_id',$user->id)->where('coin',$coin)->whereNull('partner_email')->where('type',$type)->orderBy('id')->get();
        }
        if ($type == 'perfect_money') {
            $type = 'Perfect Money';
                if ($coin == 'perfect_money') {
                $coin = 'Perfect Money';
            }
        }

    	return view('user.withdraw.withdrawal_coin',compact('coin','type','user','withdraw'));
    }

    public function postWithdraw(Request $request,$type,$coin)
    {
        $this->validate($request,[
            'withdrawal_address'   =>  'required',
            'amount'    =>  'required',
        ]);
        if ($coin == 'eur') { $coin = 'euro'; }
        if ($coin == 'Perfect Money') {
            $coin = 'Perfect Money Roi';
            $type = 'Perfect Money Roi';
            $balance = 'roi_amount';
        }elseif ($coin == 'coinbase') {
            $coin = 'Coinbase Roi';
            $type = 'Coinbase Roi';
            $balance = 'coinbase_usd';
        }else{
            $balance = $coin.'_balance';
            $balance = strtolower($balance);
        }
        $user = Sentinel::getUser();

        if ($request->amount > 0) {
            if ($coin == 'euro') { $coin = 'eur'; }
            $coin = strtoupper($coin);
            if ($user->$balance < $request->amount) {
                $notification = array(
                    'message' => 'You dont have sufficient balance to Withdraw.', 
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }
            $user->$balance = $user->$balance - $request->amount;
            $user->update();

            $withdraw = new Withdrawal;
            $withdraw->user_id = $user->id;
            $withdraw->coin = $coin;
            $withdraw->address = $request->withdrawal_address;
            $withdraw->amount = $request->amount;
            $withdraw->type = $type;
            $withdraw->save();

            $notification = array(
                'message' => 'Withdrawal request success wait for confirmation.', 
                'alert-type' => 'success'
            );

            return redirect()->back()->with($notification);
        }

        return redirect()->back()->with('error','Please enter the valid amount');
    }

    public function withdrawHistory()
   {
      $withdraw = Withdrawal::with('withdr_user')->where('status','<>','0')->orderBy('id','desc')->where('is_hide',0)->get();
      return view('admin.withdraw_history',compact('withdraw'));
   }

   public function withdreaManage()
   {
        $withdraw = Withdrawal::with('withdr_user')->where('status','0')->orderBy('id','desc')->get();
        return view('admin.withdraw_manage',compact('withdraw'));
   }

   public function withdrawReject(Request $request,$id)
   {
        $withdraw = Withdrawal::where('id',$id)->first();
        $withdraw->status = 2;
        $withdraw->update();
        $coin = $withdraw->coin;
        if ($coin == 'EUR') {
            $coin = 'EURO';
        }
        if ($coin == 'ROI COINBASE') {
            $coin = 'coinbase_usd';
        }elseif ($coin == 'COINBASE ROI') {
            $coin = 'coinbase_usd';
        }
        else{
            $coin = strtolower($coin).'_balance';
        }
        
        $user = User::where('id',$withdraw->user_id)->first();
        $user->$coin = $user->$coin + $withdraw->amount;
        $user->update();

        $notification = array(
            'message' => 'Withdrawal request reject.', 
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
   }

   public function withdrawApprove(Request $request)
   {
        $id = $request->id;
        
        $withdra = Withdrawal::find($id);
        $withdra->status = 1;
        $withdra->txid = $request->txid;
        $withdra->update();

        $notification = array(
            'message' => 'Withdrawal request approved.', 
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
   }

   public function withdrawPartner($coin)
   {
        $user = Sentinel::getUser();
        $coin = strtoupper($coin);
        if ($coin == 'COINBASE') {
           $coin = 'ROI COINBASE';
        }
        $withdraw = Withdrawal::where('user_id',$user->id)->whereNotNull('partner_email')->where('coin',$coin)->get();
        return view('user.withdraw.withdraw_partner',compact('withdraw','coin','user'));
   }

   public function postwithdrawPartner(Request $request,$coin)
   {

        $this->validate($request,[
            'email'   =>  'required',
            'amount'    =>  'required',
        ]);
        if ($coin == 'eur') {
            $coin = 'euro';
        }if ($coin == 'coinbase') {
            $balance = 'coinbase_usd';    
        }elseif ($coin == 'ROI COINBASE') {
            $balance = 'coinbase_usd';
        }
        else{
            $balance = $coin.'_balance';
            $balance = strtolower($balance);
        }
        $user = Sentinel::getUser();
        $partner_email = User::whereEmail($request->email)->first();
        if ($request->email == $user->email) {
            $notification = array(
                'message' => 'This your email.', 
                'alert-type' => 'warning'
            );

            return redirect()->back()->with($notification);
        }
        if ($partner_email) {
            if ($request->amount > 0) {
                if ($user->$balance < $request->amount) {
                    $notification = array(
                        'message' => 'You dont have sufficient balance to Withdraw.', 
                        'alert-type' => 'error'
                    );
                    return redirect()->back()->with($notification);
                }
                $user->$balance = $user->$balance - $request->amount;
                $user->update();
                if ($coin == 'euro') {
                    $coin = 'eur';
                }
                $coin = strtoupper($coin);
                $withdraw = new Withdrawal;
                $withdraw->user_id = $user->id;
                $withdraw->coin = $coin;
                $withdraw->partner_email = $request->email;
                $withdraw->amount = $request->amount;
                $withdraw->type = 'partner';
                $withdraw->save();

                $notification = array(
                    'message' => 'Withdrawal request success wait for confirmation.', 
                    'alert-type' => 'success'
                );
                return redirect()->back()->with($notification);
            }
            return redirect()->back()->with('error','Please enter the valid amount');
        }

        $notification = array(
            'message' => 'This email not valid.', 
            'alert-type' => 'error'
        );

        return redirect()->back()->with($notification);
   }

   public function approveWithdraPartner($id)
   {

        $withdra = Withdrawal::find($id);
        $withdra->status = 1;
        $withdra->update();

        $coin = $withdra->coin;
        if ($coin == 'EUR') {
            $coin = 'euro';
        }
        if ($coin == 'ROI COINBASE') {
            $coin = 'coinbase_usd';
        }else{
            $coin = strtolower($coin).'_balance';
        }
        $user = User::whereEmail($withdra->partner_email)->first();
        $user->$coin = $user->$coin + $withdra->amount;
        $user->update();

        $notification = array(
            'message' => 'Withdrawal for partner request approved.', 
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
   }
}
