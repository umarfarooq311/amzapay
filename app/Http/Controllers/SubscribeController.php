<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscribe;
use Session;
use Mail;

class SubscribeController extends Controller
{
    public function sendmail(Request $request)
    {	$email=$request->email;
    	$subscribe=Subscribe::where('email',$email)->first();
    	if(empty($subscribe))
    	{
		$subscribe= new Subscribe();
    	$subscribe->email=$request->email;
    	$subscribe->save();
    	
    	//return response()->json( array('success' => true, 'html'=>view('mails.subscribe')) );
    	
    	$this->sendEmail($email);
    	return 1;	
    	}
    }

    public function existpost(Request $request)
    {	$email=$request->email;
    	 $subscribe=Subscribe::where('email',$email)->first();
    	if($subscribe)
    	{	return json_encode($subscribe = false);
    	}
    	return json_encode($subscribe = true);
    }
    public function sendEmail($email)
        {		
        	Mail::send('mails.subscribe',['email'=>$email],
        		function($message)use($email)
        		{
        			$message->to($email);
        			$message->subject("Hello ".$email." ,subscribe");
        		});
        }
        public function send()
        {
        	return view('mails.subscribe');
        }
    }
