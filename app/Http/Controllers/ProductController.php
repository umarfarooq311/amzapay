<?php



namespace App\Http\Controllers;



use App\BulkProduct;

use App\Category;

use App\Color;

use App\Product;

use App\ProductAttribute;

use App\Size;

use App\SubCategory;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Validator;



class ProductController extends Controller

{

    public function index(){

        $products = Product::all();

        return view('admin.store.product.index', compact('products'));

    }

    public function create(){

        $categories = Category::All();

        $colors = Color::all();

        $sizes = Size::all();



        return view('admin.store.product.create', compact('categories', 'colors', 'sizes'));

    }



    public function store(Request $request){

        Validator::make($request->all(), [

            'product_name' => 'required|string|min:3',

            "sub_category" => "required|integer|exists:sub_categories,id",

            "sizes.*" => 'required|integer|exists:sizes,id',

            "colors.*" => 'required|integer|exists:colors,id',

            "quantity.*" => 'required|integer',

            "unit_price.*" => 'sometimes|nullable',

            "bulk" => 'required|string',

            "from.*" => 'sometimes|nullable',

            "to.*" => 'sometimes|nullable',

            "price.*" => 'sometimes|nullable',

            "published" => "required|string",

            "description" => 'string',

            "main_image" => "required|mimes:png,jpeg,jpg",

            "gallery_images.*" => "mimes:png,jpeg,jpg"

        ]);



        if($request->file('main_image')) {

            $files[] = $request->file('main_image');

            $main_image = $request->file('main_image')->getClientOriginalName();

        }else{

            $main_image = '';

        }

        if($request->file('gallery_images')) {

            foreach ($request->file('gallery_images') as $gallery_image) {

                $files[] = $gallery_image;

                $gallery_image_name[] = $gallery_image->getClientOriginalName();

            }

        }else{

            $gallery_image_name = [];

        }

        if(!empty($files)){

            foreach ($files as $file){

                $imageName = $file->getClientOriginalName();

                $path = Storage::disk('public')->putFileAs(

                    'products', $file, $imageName

                );

            }

        }



        $product = Product::create([

            'sub_category_id' => $request->sub_category,
            'category_id' => $request->category,

            'name' => $request->product_name,

            'slug' => $request->product_name,

            'is_bulk' => $request->bulk,

            'is_feature' => $request->featured,

            'description' => $request->description,

            'main_image' => $main_image,

            'gallery_images' => json_encode($gallery_image_name)

        ]);

        $i = 0;

        foreach ($request->sizes as $size){

            ProductAttribute::create([
                'product_id' => $product->id,

                'size_id' => $size,

                'color_id' => $request->colors[$i],

                'quantity' => $request->quantity[$i],

                'unit_price' => $request->unit_price[$i]

            ]);

            $i++;

        }



        $a = 0;

        foreach ($request->from as $from){

            BulkProduct::create([

                'product_id' => $product->id,

                'from' => $from,

                'to' => $request->to[$a],

                'price' => $request->price[$a]

            ]);

            $a++;

        }



        // flash('Product has been added successfully', 'success');

        return redirect()->route('products.index');

    }



    public function edit($id){

        $product = Product::findOrFail($id);

        $categories = Category::all();

        $sub_categories = SubCategory::all();

        $sizes = Size::all();

        $colors = Color::all();



        return view('admin.store.product.edit', compact('product', 'sub_categories', 'categories', 'colors', 'sizes'));

    }



    public function update(Request $request, $id){

        $product = Product::findOrFail($id);

        Validator::make($request->all(), [

            'product_name' => 'required|string|min:3',

            "sub_category" => "required|integer|exists:sub_categories,id",

            "sizes.*" => 'required|integer|exists:sizes,id',

            "colors.*" => 'required|integer|exists:colors,id',

            "quantity.*" => 'required|integer',

            "unit_price.*" => 'sometimes|nullable',

            "bulk" => 'required|string',

            "from.*" => 'sometimes|nullable',

            "to.*" => 'sometimes|nullable',

            "price.*" => 'sometimes|nullable',

            "published" => "required|string",

            "description" => 'string',

            "main_image" => "mimes:png,jpeg,jpg",

            "gallery_images.*" => "mimes:png,jpeg,jpg"

        ]);



        if($request->file('main_image')) {

            $files[] = $request->file('main_image');

            $main_image = $request->file('main_image')->getClientOriginalName();

        }else{

            $main_image = $product->main_image;

        }

        if($request->file('gallery_images')) {

            foreach ($request->file('gallery_images') as $gallery_image) {

                $files[] = $gallery_image;

                $gallery_image_name[] = $gallery_image->getClientOriginalName();

            }

        }else{

            $gallery_image_name = json_decode($product->gallery_image);

        }



        $product->update([

            'sub_category_id' => $request->sub_category,

            'name' => $request->product_name,

            'slug' => $request->product_name,

            'is_bulk' => $request->bulk,

            'is_feature' => $request->featured,

            'description' => $request->description,

            'main_image' => $main_image,

            'gallery_images' => json_encode($gallery_image_name)

        ]);

        //delete the already existing product attributes

        ProductAttribute::where('product_id', $product->id)->delete();

        $i = 0;

        foreach ($request->sizes as $size){

            ProductAttribute::create([

                'product_id' => $product->id,

                'size_id' => $size,

                'color_id' => $request->colors[$i],

                'quantity' => $request->quantity[$i],

                'unit_price' => $request->unit_price[$i]

            ]);

            $i++;

        }



        if($request['bulk'] == 1){

            BulkProduct::where('product_id', $product->id)->delete();

            $a = 0;

            foreach ($request->from as $from){

                BulkProduct::create([

                    'product_id' => $product->id,

                    'from' => $from,

                    'to' => $request->to[$a],

                    'price' => $request->price[$a]

                ]);

                $a++;

            }

        }

        flash("Product has been updated successfully", "success");

        return back();

    }



    public function getSubCategories(Request $request){

        $subCategories = SubCategory::where('category_id', $request->data)->get();

        if($subCategories->isNotEmpty()){

            $status = 'success';

            $data = $subCategories;

        }else{

            $status = 'fail';

            $data = [];

        }



        return response()->json(['data' => $data, 'status' => $status]);

    }

}

//=======
//<?php
//
//
//
//namespace App\Http\Controllers;
//
//
//
//use App\BulkProduct;
//
//use App\Category;
//
//use App\Color;
//
//use App\Product;
//
//use App\ProductAttribute;
//
//use App\Size;
//
//use App\SubCategory;
//
//use Illuminate\Http\Request;
//
//use Illuminate\Support\Facades\Storage;
//
//use Illuminate\Support\Facades\Validator;
//
//
//
//class ProductController extends Controller
//
//{
//
//    public function index(){
//
//        $products = Product::all();
//
//        return view('admin.store.product.index', compact('products'));
//
//    }
//
//    public function create(){
//
//        $categories = Category::All();
//
//        $colors = Color::all();
//
//        $sizes = Size::all();
//
//
//
//        return view('admin.store.product.create', compact('categories', 'colors', 'sizes'));
//
//    }
//
//
//
//    public function store(Request $request){
//
//        Validator::make($request->all(), [
//
//            'product_name' => 'required|string|min:3',
//
//            "sub_category" => "required|integer|exists:sub_categories,id",
//
//            "sizes.*" => 'required|integer|exists:sizes,id',
//
//            "colors.*" => 'required|integer|exists:colors,id',
//
//            "quantity.*" => 'required|integer',
//
//            "unit_price.*" => 'sometimes|nullable',
//
//            "bulk" => 'required|string',
//
//            "from.*" => 'sometimes|nullable',
//
//            "to.*" => 'sometimes|nullable',
//
//            "price.*" => 'sometimes|nullable',
//
//            "published" => "required|string",
//
//            "description" => 'string',
//
//            "main_image" => "required|mimes:png,jpeg,jpg",
//
//            "gallery_images.*" => "mimes:png,jpeg,jpg"
//
//        ]);
//
//
//
//        if($request->file('main_image')) {
//
//            $files[] = $request->file('main_image');
//
//            $main_image = $request->file('main_image')->getClientOriginalName();
//
//        }else{
//
//            $main_image = '';
//
//        }
//
//        if($request->file('gallery_images')) {
//
//            foreach ($request->file('gallery_images') as $gallery_image) {
//
//                $files[] = $gallery_image;
//
//                $gallery_image_name[] = $gallery_image->getClientOriginalName();
//
//            }
//
//        }else{
//
//            $gallery_image_name = [];
//
//        }
//
//        if(!empty($files)){
//
//            foreach ($files as $file){
//
//                $imageName = $file->getClientOriginalName();
//
//                $path = Storage::disk('public')->putFileAs(
//
//                    'products', $file, $imageName
//
//                );
//
//            }
//
//        }
//
//
//
//        $product = Product::create([
//
//            'sub_category_id' => $request->sub_category,
//            'category_id' => $request->category,
//
//            'name' => $request->product_name,
//
//            'slug' => $request->product_name,
//
//            'is_bulk' => $request->bulk,
//
//            'is_feature' => $request->featured,
//
//            'description' => $request->description,
//
//            'main_image' => $main_image,
//
//            'gallery_images' => json_encode($gallery_image_name)
//
//        ]);
//
//        $i = 0;
//
//        foreach ($request->sizes as $size){
//
//            ProductAttribute::create([
//                'product_id' => $product->id,
//
//                'size_id' => $size,
//
//                'color_id' => $request->colors[$i],
//
//                'quantity' => $request->quantity[$i],
//
//                'unit_price' => $request->unit_price[$i]
//
//            ]);
//
//            $i++;
//
//        }
//
//
//
//        $a = 0;
//
//        foreach ($request->from as $from){
//
//            BulkProduct::create([
//
//                'product_id' => $product->id,
//
//                'from' => $from,
//
//                'to' => $request->to[$a],
//
//                'price' => $request->price[$a]
//
//            ]);
//
//            $a++;
//
//        }
//
//
//
//        // flash('Product has been added successfully', 'success');
//
//        return redirect()->route('products.index');
//
//    }
//
//
//
//    public function edit($id){
//
//        $product = Product::findOrFail($id);
//
//        $categories = Category::all();
//
//        $sub_categories = SubCategory::all();
//
//        $sizes = Size::all();
//
//        $colors = Color::all();
//
//
//
//        return view('admin.store.product.edit', compact('product', 'sub_categories', 'categories', 'colors', 'sizes'));
//
//    }
//
//
//
//    public function update(Request $request, $id){
//
//        $product = Product::findOrFail($id);
//
//        Validator::make($request->all(), [
//
//            'product_name' => 'required|string|min:3',
//
//            "sub_category" => "required|integer|exists:sub_categories,id",
//
//            "sizes.*" => 'required|integer|exists:sizes,id',
//
//            "colors.*" => 'required|integer|exists:colors,id',
//
//            "quantity.*" => 'required|integer',
//
//            "unit_price.*" => 'sometimes|nullable',
//
//            "bulk" => 'required|string',
//
//            "from.*" => 'sometimes|nullable',
//
//            "to.*" => 'sometimes|nullable',
//
//            "price.*" => 'sometimes|nullable',
//
//            "published" => "required|string",
//
//            "description" => 'string',
//
//            "main_image" => "mimes:png,jpeg,jpg",
//
//            "gallery_images.*" => "mimes:png,jpeg,jpg"
//
//        ]);
//
//
//
//        if($request->file('main_image')) {
//
//            $files[] = $request->file('main_image');
//
//            $main_image = $request->file('main_image')->getClientOriginalName();
//
//        }else{
//
//            $main_image = $product->main_image;
//
//        }
//
//        if($request->file('gallery_images')) {
//
//            foreach ($request->file('gallery_images') as $gallery_image) {
//
//                $files[] = $gallery_image;
//
//                $gallery_image_name[] = $gallery_image->getClientOriginalName();
//
//            }
//
//        }else{
//
//            $gallery_image_name = json_decode($product->gallery_image);
//
//        }
//
//
//
//        $product->update([
//
//            'sub_category_id' => $request->sub_category,
//
//            'name' => $request->product_name,
//
//            'slug' => $request->product_name,
//
//            'is_bulk' => $request->bulk,
//
//            'is_feature' => $request->featured,
//
//            'description' => $request->description,
//
//            'main_image' => $main_image,
//
//            'gallery_images' => json_encode($gallery_image_name)
//
//        ]);
//
//        //delete the already existing product attributes
//
//        ProductAttribute::where('product_id', $product->id)->delete();
//
//        $i = 0;
//
//        foreach ($request->sizes as $size){
//
//            ProductAttribute::create([
//
//                'product_id' => $product->id,
//
//                'size_id' => $size,
//
//                'color_id' => $request->colors[$i],
//
//                'quantity' => $request->quantity[$i],
//
//                'unit_price' => $request->unit_price[$i]
//
//            ]);
//
//            $i++;
//
//        }
//
//
//
//        if($request['bulk'] == 1){
//
//            BulkProduct::where('product_id', $product->id)->delete();
//
//            $a = 0;
//
//            foreach ($request->from as $from){
//
//                BulkProduct::create([
//
//                    'product_id' => $product->id,
//
//                    'from' => $from,
//
//                    'to' => $request->to[$a],
//
//                    'price' => $request->price[$a]
//
//                ]);
//
//                $a++;
//
//            }
//
//        }
//
//        flash("Product has been updated successfully", "success");
//
//        return back();
//
//    }
//
//
//
//    public function getSubCategories(Request $request){
//
//        $subCategories = SubCategory::where('category_id', $request->data)->get();
//
//        if($subCategories->isNotEmpty()){
//
//            $status = 'success';
//
//            $data = $subCategories;
//
//        }else{
//
//            $status = 'fail';
//
//            $data = [];
//
//        }
//
//
//
//        return response()->json(['data' => $data, 'status' => $status]);
//
//    }
//
//}
//
//>>>>>>> 28df7d3a93604457270ac32c44459b19fa01513b
