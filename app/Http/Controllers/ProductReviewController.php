<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductReview;
use Illuminate\Http\Request;

class ProductReviewController extends Controller
{
    public function index(){
        $reviews = ProductReview::all();

        return view('admin.store.product_reviews.index', compact('reviews'));
    }

    public function create(){
        $products = Product::all()->pluck('name','id');

        return view('admin.store.product_reviews.create', compact('products'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'product_id' => 'required|integer|exists:products,id',
            'username' => 'required|string',
            'rating' => 'required|integer',
            'review' => 'required|string'
        ]);

        ProductReview::create([
           'product_id' => $request->product_id,
           'username' => $request->username,
           'rating' => $request->rating,
           'review' => $request->review
        ]);

        return redirect()->route('product.reviews.index')->with('success', 'Review has been added');
    }

    public function edit($id){
        $review = ProductReview::findOrFail($id);
        $products = Product::all()->pluck('name', 'id');

        return view('admin.store.product_reviews.edit', compact('review', 'products'));
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'product_id' => 'required|integer|exists:products,id',
            'username' => 'required|string',
            'rating' => 'required|integer',
            'review' => 'required|string'
        ]);

        $review = ProductReview::findOrFail($id);
        $review->update([
            'product_id' => $request->product_id,
            'username' => $request->username,
            'rating' => $request->rating,
            'review' => $request->review
        ]);

        return redirect()->route('product.reviews.index')->with('success', 'Review has been edit');
    }

    public function destroy($id){
        $review = ProductReview::findOrFail($id);
        $review->delete();

        return redirect()->route('product.reviews.index')->with('success', 'Review has been deleted');
    }
}
