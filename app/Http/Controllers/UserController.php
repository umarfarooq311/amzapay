<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Purchase;
use App\Models\Deposit;
use App\Models\Withdrawal;
use App\Models\PvBonus;
use App\Models\PvPoint;
use App\Models\FinalBonus;
use Sentinel;

class UserController extends Controller
{
    public function wallet()
    {
        return view('user.deposit.wallet');
    }

    public function invested()
    {
    	$user_id = Sentinel::getUser()->id;
    	$invested = Purchase::with('package')->where('user_id',$user_id)->orderBy('id','desc')->get();
    	return view('user.invested',compact('invested'));
    }

    public function depositHistory()
    {
        $user_id = Sentinel::getUser()->id;
        $deposit = Deposit::where('user_id',$user_id)->orderBy('id','desc')->get();
        return view('user.deposit_history',compact('deposit'));
    }

    public function withdrawHistory()
    {
        $user_id = Sentinel::getUser()->id;
        $withdraw = Withdrawal::where('user_id',$user_id)->orderBy('id','desc')->get();
        return view('user.withdraw_history',compact('withdraw'));
    }

    public function commissionHistory()
    {
        $user_id = Sentinel::getUser()->id;
        $pv_bonus = PvBonus::where('user_id',$user_id)->orderBy('id','desc')->get();
        $pv_points = PvPoint::with('givepoints')->where('parent_id',$user_id)->orderBy('created_at','desc')->get();
        $final_bonus = FinalBonus::where('user_id',$user_id)->orderBy('created_at','desc')->get();
        return view('user.commission_history',compact('pv_bonus','pv_points','final_bonus'));
    }

    public function changePassword()
    {
        return view('user.change_password');
    }
}
