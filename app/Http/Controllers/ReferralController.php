<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PvBonus;
use App\Models\FinalBonus;
use App\Models\Purchase;
use App\User;
use Sentinel;

class ReferralController extends Controller
{
    public function myNetwork()
    {
    	$user = Sentinel::getUser();
        $pv_bonus = PvBonus::where('user_id',$user->id)->sum('amount');
        $final_bonus = FinalBonus::where('user_id',$user->id)->sum('bonus_amount');
    	$referral_list = User::with('parentuser')->where('referral_id',$user->id)->get();
    	return view('user.network',compact('referral_list','user','pv_bonus','final_bonus'));
    }

    public function myNetworkData()
    {
    	$i = 0;
        $j = 0;
        $user = Sentinel::getuser();
        $parent_id = $user->id;
        $arr1 = array();

        // Parent:
        $refData = User::where('status', 1)->where('parent_id', $parent_id)->get();
        foreach ($refData as $key => $value) {
            $arr1[$j]['name'] = $value['user_name'];
            $arr1[$j]['title'] = 'Ref. Friend';
            $arr1[$j]['className'] = 'middle-level';
            $j++;
            // $parent_id = $value['parent_id'];
            // $refData = User::where('status', 1)->where('parent_id', $parent_id)->get();
            
        }
        // Goto Parent;
        print_r(json_encode($arr1));
    }

    public function referrelPosition(Request $request)
    {
        $position = Sentinel::getUser();
        $position->position = $request->position;
        $position->update();

        $notification = array(
          'message' => 'Referral Position Set.', 
          'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }

    public function demotree()
    {
        // $parent_id = Sentinel::getUser()->id;
        
        // $data = User::where('parent_id',$parent_id)->get();

        // $children = array();
        // foreach($data as $row) {
        //     $child_id = $row->id;
        //     $children[$child_id] = $row;
        //     $children = array_merge($children, $this->getAllDownlines($child_id));
        // }
        // // return $children;
        // $children = json_encode($children);
        return view('user.demo-tree',compact('user','parent_id','children'));
    }

    public function gettreedata($id){
        $mainuser = Sentinel::getUser();
        $data = User::where('parent_id',$id)->get();

        $children = array();
        foreach($data as $row) {
            $child_id = $row->id;
            $children[$child_id] = $row;
            $children = array_merge($children, $this->getAllDownlines($child_id));
        }
        $data11 = array();
        $i=0;
        foreach ($children as  $value) {
           $data11[$i]['id'] =  $value['id'];
           $data11[$i]['parentId'] =  $value['parent_id'];
           $data11[$i]['name'] =  $value['user_name'];
           $data11[$i]['email'] =  $value['email'];
           if ($value['profile'] == Null) {
                $data11[$i]['image'] =  url('/')."/assets/profiles/1545731671.jpg";
           }else{
                $data11[$i]['image'] =  url('/')."/assets/profiles/".$value['profile'];
           }
           $i++;
        }
           $data11[$i]['id'] =  $mainuser->id;
           $data11[$i]['parentId'] = null;
           $data11[$i]['name'] = $mainuser->user_name;
           $data11[$i]['email'] = $mainuser->email;
           if ($mainuser->profile == Null) {
                $data11[$i]['image'] =  url('/')."/assets/profiles/1545731671.jpg";
           }else{

                $data11[$i]['image'] = url('/')."/assets/profiles/".$mainuser->profile; 
           }
           //$data11[$i]['image'] = $mainuser->profile; 
        // return $data11;
        return json_encode($data11);
    }

    public function assignPosition($id)
    {
        $user = Sentinel::getUser();
        $pv_bonus = PvBonus::where('user_id',$user->id)->sum('amount');
        $final_bonus = FinalBonus::where('user_id',$user->id)->sum('bonus_amount');

        $parent_id = Sentinel::getUser()->id;
        $data = User::where('parent_id',$parent_id)->get();

        $children = array();
        foreach($data as $row) {
            $child_id = $row->id;
            $children[$child_id] = $row;
            $children = array_merge($children, $this->getAllDownlines($child_id));
        }
        // return $children;
        $children = json_encode($children);
        $user = User::where('id',$id)->first();
        return view('user.asaign_position',compact('user','parent_id','children','pv_bonus','final_bonus'));
    }

    public function refferalDownline()
    {
        $user = Sentinel::getUser();
        $pv_bonus = PvBonus::where('user_id',$user->id)->sum('amount');
        $final_bonus = FinalBonus::where('user_id',$user->id)->sum('bonus_amount');

        $parent_id = Sentinel::getUser()->id;
        $data = User::where('parent_id',$parent_id)->get();

        $children = User::where('id',$parent_id)->get()->toArray();
        foreach($data as $row) {
            $child_id = $row->id;
            $children[$child_id] = $row;
            $children = array_merge($children, $this->getAllDownlines($child_id));
        }
        
        $children = json_encode($children);

        return view('user.referral_tree',compact('parent_id','children','pv_bonus','final_bonus'));
    }

    public function getAllDownlines($fathers) {
        if(is_array($fathers))
            $data = User::whereIn('parent_id',$fathers)->get();
        else
            $data = User::where('parent_id',$fathers)->get();

        $new_father_ids = array();
        $children = array();
        foreach ($data as $child) {
            $children[$child->id] = $child; // etc

            $new_father_ids[] = $child->id;
        }
        if($new_father_ids)
            $children = array_merge($children, $this->getAllDownlines($new_father_ids));
        
        return $children;
    }

    public function postassignPosition(Request $request)
    {
        $this->validate($request,[
            'position' => 'required',
            'parent'   => 'required',
        ]);

        // return $request->all();
        $checkassgin = User::where('parent_id',$request->parent)->where('position',$request->position)->first();
        if ($checkassgin) {
            $notification = array(
              'message' => 'Alredy set this position.', 
              'alert-type' => 'warning'
            );
            return redirect()->back()->with($notification);            
        }else{
            $user = User::find($request->user_id);
            $user->parent_id = $request->parent;
            $user->position  =  $request->position;
            $user->update();
        }

        $notification = array(
          'message' => 'Referral Position Set.', 
          'alert-type' => 'success'
        );
        return redirect('refferal-tree')->with($notification);
    }

    public function alredyassginPosition(Request $request)
    {
        $parent_id = $request->parent_id;
        $position  = $request->position;

        $parentcheck = User::where('parent_id',$parent_id)->where('position',$position)->first();
        if ($parentcheck) {
            return 1;
        }
        else{
            return 0;
        }
    }

    public function referralsPackages(Request $request)
    {
        // return $request->refe_id;
        $purchase = Purchase::with('package')->where('user_id',$request->refe_id)->where(array('final_status' =>1, 'is_delete' =>0, 'status' =>0))->get();
        if (!empty($purchase)) {
            return $purchase;
        }else{
            return 0;
        }
    }
    
}
