<?php

namespace App\Http\Controllers;

use App\GeneralMessage;
use Illuminate\Http\Request;

class GeneralMessageController extends Controller
{
    public function store(Request $request){
        $this->validate($request, [
           'email' => 'required',
           'message' => 'required|string',
           'service' => 'required|string' ,
            'subject' => 'required|string'
        ]);

        GeneralMessage::create([
           'name' => $request->name,
           'email' => $request->email,
           'phone' => $request->phone,
           'service' => $request->service,
           'subject' => $request->subject,
           'message' => $request->message,
        ]);

        return back()->with('success', 'Message has been sent.');
    }
}
