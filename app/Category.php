<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    public function getStatus(){
        return ($this->is_published == 1) ? 'published' : 'not published';
    }

    public function setSlugAttribute($value){
        $this->attributes['slug'] = str_slug($value);
    }

    public function product()
    {
        return $this->hasMany(Product::class);
    }

    public function SubCategory()
    {
        return $this->hasMany(SubCategory::class,'category_id', 'id');
    }

}

