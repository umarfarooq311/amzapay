<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneralMessage extends Model
{
    protected $table = 'general_messages';
    protected $guarded = [];
}
