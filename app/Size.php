<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $table = 'sizes';
    protected $guarded = [];

    public function setShortAttribute($value){
        $this->attributes['short'] = strtoupper(substr($value, 0, 1));
    }
}
