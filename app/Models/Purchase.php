<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $guarded = [];
    public function package()
    {
    	return $this->hasOne('App\Models\Package','id','packg_id');
    }

    public function invest_user()
    {
    	return $this->hasOne('App\User','id','user_id');	
    }
}
