<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Http\Request;
use Config;
use Illuminate\Support\Facades\Auth;
use View;
use App\Models\Package;
use App\Models\Purchase;
use App\Models\DailyRoi;
use Carbon\Carbon;
use App\User;
use Storage;
use Sentinel;

use App\Models\Deposit;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Address;
use Coinbase\Wallet\Enum\CurrencyCode;
use Coinbase\Wallet\Value\Money;
use App\Models\Setting;
use App\Models\CoinAddress;

class test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:autotread';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $today = Carbon::now();

        $today = $today->toDateString();
        $purchase = Purchase::where(array('final_status' => 1, 'is_delete' => 0, 'status' => 0))->get();
        Storage::disk('local')->put('purchase - new.txt', json_encode($purchase));

        foreach ($purchase as $purch) {
            $validate = $purch->valid_date;
            $tid = $purch->txid;
            $purchs_id = $purch->id;
            $roi_amount = $purch->usd_roi_amount;
            $user_id = $purch->user_id;

            if (strtotime($today) > strtotime($validate)) {
                Storage::disk('local')->put('roi - new.txt', json_encode($purch));
                $complete = Purchase::find($purch->id);
                $complete->status = 1;
                $complete->update();
            }
            if ($today < $validate) {

                $saturday = Carbon::now()->isSaturday();
                if (!$saturday) {

                    $sunday = Carbon::now()->isSunday();
                    if (!$sunday) {
                        $dailyroi = DailyRoi::whereDate('created_at', '=', date('Y-m-d'))->where('tid', $tid)->get();

                        if (sizeof($dailyroi) == 0) {

                            $daily_roi = new DailyRoi;
                            $daily_roi->user_id = $user_id;
                            $daily_roi->purch_id = $purchs_id;
                            $daily_roi->tid = $tid;
                            $daily_roi->roi_amount = $roi_amount;
                            $daily_roi->save();

                            $purchase = Purchase::where('id', $purchs_id)->first();
                            $purchase->date = date('Y-m-d');
                            $purchase->update();

                            $user = User::where('id', $user_id)->first();
                            if ($purch->type == 'btc') {
                                $user->coinbase_usd = $user->coinbase_usd + $roi_amount;
                            } else {
                                $user->usd_balance = $user->usd_balance + $roi_amount;
                                $user->roi_amount = $user->roi_amount + $roi_amount;
                            }
                            $user->update();
                        }
                    }
                }
            }
        }

        $setting = Setting::first();
        $configuration = Configuration::apiKey($setting->api_key, $setting->api_secret);
        $client = Client::create($configuration);
        $account = $client->getPrimaryAccount();

        $address = CoinAddress::get();

        Storage::disk('local')->put('deposi-call-back - new.txt', json_encode($address));
        foreach ($address as $value) {

            $address = $client->getAccountAddress($account, $value->address_id);
            $transactions = $client->getAddressTransactions($address);
            $transactions = $client->decodeLastResponse();
            $hisotry = $transactions['data'];

            foreach ($hisotry as $hist) {

                $deposit = Deposit::where('txid', $hist['id'])->first();
                if ($deposit) {
                    if ($deposit->status != 1 && $deposit->status != 2) {

                        if ($hist['status'] == 'pending') {
                            $deposit->status = 0;
                        } elseif ($hist['status'] == 'completed') {
                            $deposit->status = 1;

                            $user = User::where('id', $value->user_id)->first();
                            $user->btc_balance = $user->btc_balance + $deposit->amount;
                            $user->update();

                        } elseif ($hist['status'] == 'failed') {
                            $deposit->status = 2;
                        } elseif ($hist['status'] == 'canceled') {
                            $deposit->status = 2;
                        } elseif ($hist['status'] == 'expired') {
                            $deposit->status = 2;
                        }
                        $deposit->update();
                    }

                } else {
                    $deposit = new Deposit;
                    $deposit->user_id = $value->user_id;
                    $deposit->address = $value->address;
                    $deposit->coin = $hist['amount']['currency'];
                    $deposit->amount = $hist['amount']['amount'];
                    $deposit->txid = $hist['id'];
                    $deposit->type = 'Coinbase';
                    if ($hist['status'] == 'pending') {
                        $deposit->status = 0;
                    } elseif ($hist['status'] == 'completed') {
                        $deposit->status = 1;
                    } elseif ($hist['status'] == 'failed') {
                        $deposit->status = 2;
                    } elseif ($hist['status'] == 'canceled') {
                        $deposit->status = 2;
                    } elseif ($hist['status'] == 'expired') {
                        $deposit->status = 2;
                    }
                    $deposit->save();

                }
            }

        }

    }

}
