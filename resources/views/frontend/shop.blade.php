@extends('frontend.master')

@section('content')

    <!-- Document Wrapper

	============================================= -->
    <head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>

    <div id="wrapper" class="clearfix">



        <!-- Header

        ============================================= -->

        <header id="header" class="full-header">



            <div id="header-wrap">



                <div class="container clearfix">



                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>



                    <!-- Logo

                    ============================================= -->

                    <div id="logo">

                        <a href="index.html" class="standard-logo" data-dark-logo="{{ asset('frontend_assets/assets/logo.png') }}"><img src="{{ asset('frontend_assets/assets/logo.png') }}" alt="Amza Logo"></a>

                        <a href="index.html" class="retina-logo" data-dark-logo="{{ asset('frontend_assets/images/logo-dark2x.png') }}"><img src="{{ asset('frontend_asset/images/logo@2x.png') }}" alt="Amza Logo"></a>

                    </div><!-- #logo end -->



                    <!-- Primary Navigation

                    ============================================= -->

                    <nav id="primary-menu">



                        <ul>

                            <li><a href="#" data-href="#home"><div>Home</div></a></li>

                            <li><a href="#" data-href="#section-about"><div>About</div></a></li>

                            <li><a href="#" data-href="#section-team"><div>Team</div></a></li>

                            <li><a href="#" data-href="#section-pricing"><div>Packages</div></a></li>

                            <li><a href="blog.html"><div>Blog</div></a></li>

                            <li><a href="#" data-href="#section-testimonials" data-offset="60"><div>Testimonials</div></a></li>

                            <li><a href="#" data-href="#section-faqs"><div>Faqs</div></a></li>

                            <li><a href="#" data-href="#section-services"><div>Services</div></a></li>

                            <li><a href="#" data-href="#section-contact"><div>Contact</div></a></li>

                        </ul>



                        <!-- Top Cart

                        ============================================= -->

                         <div id="top-cart">

                            <a href="{{URL::to('')}}/shop/cartview" id="top-cart-trigger"><i class="icon-shopping-cart"></i><span>{{$cartTotalQuantity = Cart::getTotalQuantity()}}</span></a>

                            <div class="top-cart-content">

                                <div class="top-cart-title">

                                    <h4>Shopping Cart</h4>

                                </div>
                          <?php $cartcollection=Cart::getContent(); ?>
                                @foreach ($cartcollection as $product)

                                <div class="top-cart-items">

                                    <div class="top-cart-item clearfix">

                                        <div class="top-cart-item-image">

                                            <a href="#"><img src="{{asset('storage/products/'.$product->image) }}" alt="Blue Round-Neck Tshirt" /></a>

                                        </div>

                                        <div class="top-cart-item-desc">

                                            <a href="#">{{$product->name}}</a>

                                            <span class="top-cart-item-price">{{$product->price}}</span>

                                            <span class="top-cart-item-quantity">x {{$product->quantity}}</span>

                                        </div>

                                    </div>



                                </div>
                                @endforeach
                                <div class="top-cart-action clearfix">

                                    <span class="fleft top-checkout-price">${{Cart::getSubTotal()}}</span>

                                    <a href="{{URL::to('')}}/shop/cartview" class="button button-3d button-small nomargin fright" >View Cart</a>

                                </div>

                            </div>

                        </div><!-- #top-cart end -->




                        <!-- Top Search

                        ============================================= -->

                        <div id="top-search">

                            <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>

                            <form action="search.html" method="get">

                                <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">

                            </form>

                        </div><!-- #top-search end -->



                    </nav><!-- #primary-menu end -->



                </div>



            </div>



        </header><!-- #header end -->

        <!-- Page Title

		============================================= -->

        <section id="page-title">



            <div class="container clearfix">

                <h1>Shop</h1>

                <span>Start Buying your Favourite Theme</span>

                <ol class="breadcrumb">

                    <li class="breadcrumb-item"><a href="#">Home</a></li>

                    <li class="breadcrumb-item active" aria-current="page">Shop</li>

                </ol>

            </div>



        </section><!-- #page-title end -->



        <!-- Content

        ============================================= -->

        <section id="content">



 <div class="content-wrap">

                <div class="container clearfix">

                    <!-- Post Content
                    ============================================= -->
                    <div class="postcontent nobottommargin col_last" id="category_search">

                        <!-- Shop
                        ============================================= -->
                        <div id="shop" class="shop product-3 grid-container clearfix">
                                @if(!empty('all_products'))

                                  @foreach($all_products as $product)

                                  <div id="hide_row" class="product sf-dress clearfix">
                                <div class="product-image">
                                    <a href="{{URL::to('')}}/shop/single/{{$product->id}}"><img src="{{asset('storage/products/'.$product->main_image) }}" alt="Checked Short Dress"></a>
                                    <a href="{{URL::to('')}}/shop/single/{{$product->id}}"><img src="{{asset('storage/products/'.$product->main_image) }}" alt="Checked Short Dress"></a>
                                    <div class="sale-flash">50% Off*</div>
                                    <div class="product-overlay">
                                        <a href="{{URL::to('')}}/shop/add_to_cart/{{$product->id}}" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Checked Short Dress</a></h3></div>
                                    <div class="product-price"><del>$24.99</del>
                                        <ins>
                                            @foreach ($product->product_attributes as $resource)
                                                {{ $resource->unit_price }}
                                          @endforeach
                                       </ins>
                                   </div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-half-full"></i>
                                    </div>
                                </div>
                            </div>

                           @endforeach

                        @endif

                        </div><!-- #shop end -->
                       {{ $all_products->links() }}
                    </div><!-- .postcontent end -->



                    <!-- Sidebar
                    ============================================= -->
                    <div class="sidebar nobottommargin">
                        <div class="sidebar-widgets-wrap">

                            <div class="widget widget-filter-links clearfix">

                                <h4>Select Category</h4>
                                <ul class="custom-filter" data-container="#shop" data-active-class="active-filter">
                                    <li class="widget-filter-reset active-filter"><a href="#" data-filter="*">Clear</a></li>
                                    @foreach($all_categories as $categories)
                                    <li  ><a href="#" id="{{$categories->id}}" class="category_search" data-filter=".sf-dress">{{$categories->name}}</a></li>
                                    @endforeach
                                </ul>

                            </div>
                             <div class="widget widget-filter-links clearfix">

                                <h4>Select Color</h4>
                                <ul class="custom-filter" data-container="#shop" data-active-class="active-filter">
                                    <li class="widget-filter-reset active-filter"><a href="#" data-filter="*">Clear</a></li>
                                    @if(!empty('all_colors'))
                                    @foreach($all_colors as $color)
                                    <li  ><a href="#" id="{{$color->id}}" class="color_search" data-filter=".sf-dress">{{$color->name}}</a></li>
                                    @endforeach
                                    @endif
                                </ul>

                            </div>


                        </div>
                    </div><!-- .sidebar end -->
                </div>
            </div>
        </section><!-- #content end -->
    </div>
    <script>

        $(document).ready(function(){
  $(".category_search").on("click",function(){

    var category_id = $(this).attr('id');

    $.ajax({
                method: 'get',
                url: '{{ URL::route('shop.search_by_category','category_id') }}',
                datatype:'json',
                data: {'category_id' : category_id},
                success: function(response){

                 $("#shop").hide();
                 var html ='';
                     html +='<div  class="shop product-3 grid-container clearfix">';
		       $.each(response,function(value,data){




		       	 html +='<div class="product sf-dress clearfix">';
                         html +='<div class="product-image">';
                         html +='<a href="#"><img src="storage/products/'+data.main_image+'" alt="Checked Short Dress"></a>';
                         html +=' <a href="#"><img src="storage/products/'+data.main_image+'" alt="Checked Short Dress"></a>';
                         html +='<div class="sale-flash">50% Off*</div>';
                         html +='<div class="product-overlay">';
                         html +=' <a href="{{URL::to('')}}/shop/add_to_cart/'+data.id+'" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>';
                         html +='<a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>';
                         html +='<a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>';
 			             html +='</div>';
                         html +='</div>';
                         html +='<div class="product-desc center">';
                         html +='<div class="product-title"><h3><a href="#">'+data.name+'</a></h3></div>';
                         html +='<div class="product-price"><del>$24.99</del>';
                         $.each(data.product_attributes,function (val,mydata) {
                         html +='<ins>$'+mydata.unit_price+'</ins>';
                         });
                         html +='</div>';
                         html +='<div class="product-rating">';
                         html +=' <i class="icon-star3"></i>';
                         html +='<i class="icon-star3"></i>';
                         html +='<i class="icon-star3"></i>';
                         html +='<i class="icon-star3"></i>';
                         html +='<i class="icon-star-half-full"></i>';
                         html +='</div>';
                         html +='</div>';
                         html +='</div>';


		       });
                       html +='</div>';


		       $( "#category_search" ).html( html );
                },
               });

  });


            $(".color_search").on("click",function(){

         var  color_id = $(this).attr('id');
         $.ajax({

                method: 'get',
                url: '{{ URL::route('shop.search_by_color','color_id') }}',
                datatype:'json',
                data: {'color_id' : color_id},
                success: function(response){

                    $("#shop").hide();
                 var html ='';
                     html +='<div  class="shop product-3 grid-container clearfix">';
		       $.each(response,function(value,data){


                console.log(data.products.main_image);





		       	 html +='<div class="product sf-dress clearfix">';
                         html +='<div class="product-image">';
                         html +='<a href="#"><img src="storage/products/'+data.main_image+'" alt="Checked Short Dress"></a>';
                         html +=' <a href="#"><img src="storage/products/'+data.main_image+'" alt="Checked Short Dress"></a>';
                         html +='<div class="sale-flash">50% Off*</div>';
                         html +='<div class="product-overlay">';
                         html +='<a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>';
                         html +='<a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>';
 			 html +='</div>';
                         html +='</div>';
                         html +='<div class="product-desc center">';
                         html +='<div class="product-title"><h3><a href="#">'+data.name+'</a></h3></div>';
                         html +='<div class="product-price"><del>$24.99</del>';
                         html +='<ins>$221</ins>';
                         html +='</div>';
                         html +='<div class="product-rating">';
                         html +=' <i class="icon-star3"></i>';
                         html +='<i class="icon-star3"></i>';
                         html +='<i class="icon-star3"></i>';
                         html +='<i class="icon-star3"></i>';
                         html +='<i class="icon-star-half-full"></i>';
                         html +='</div>';
                         html +='</div>';
                         html +='</div>';


		       });
                       html +='</div>';


		       $( "#category_search" ).html( html );

            },
         });
    });
});


        </script>

@endsection

{{--@extends('frontend.master')--}}

{{--@section('content')--}}

{{--    <!-- Document Wrapper--}}

{{--	============================================= -->--}}
{{--    <head>--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>--}}
{{--</head>--}}

{{--    <div id="wrapper" class="clearfix">--}}



{{--        <!-- Header--}}

{{--        ============================================= -->--}}

{{--        <header id="header" class="full-header">--}}



{{--            <div id="header-wrap">--}}



{{--                <div class="container clearfix">--}}



{{--                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>--}}



{{--                    <!-- Logo--}}

{{--                    ============================================= -->--}}

{{--                    <div id="logo">--}}

{{--                        <a href="index.html" class="standard-logo" data-dark-logo="{{ asset('frontend_assets/assets/logo.png') }}"><img src="{{ asset('frontend_assets/assets/logo.png') }}" alt="Amza Logo"></a>--}}

{{--                        <a href="index.html" class="retina-logo" data-dark-logo="{{ asset('frontend_assets/images/logo-dark2x.png') }}"><img src="{{ asset('frontend_asset/images/logo@2x.png') }}" alt="Amza Logo"></a>--}}

{{--                    </div><!-- #logo end -->--}}



{{--                    <!-- Primary Navigation--}}

{{--                    ============================================= -->--}}

{{--                    <nav id="primary-menu">--}}



{{--                        <ul>--}}

{{--                            <li><a href="#" data-href="#home"><div>Home</div></a></li>--}}

{{--                            <li><a href="#" data-href="#section-about"><div>About</div></a></li>--}}

{{--                            <li><a href="#" data-href="#section-team"><div>Team</div></a></li>--}}

{{--                            <li><a href="#" data-href="#section-pricing"><div>Packages</div></a></li>--}}

{{--                            <li><a href="blog.html"><div>Blog</div></a></li>--}}

{{--                            <li><a href="#" data-href="#section-testimonials" data-offset="60"><div>Testimonials</div></a></li>--}}

{{--                            <li><a href="#" data-href="#section-faqs"><div>Faqs</div></a></li>--}}

{{--                            <li><a href="#" data-href="#section-services"><div>Services</div></a></li>--}}

{{--                            <li><a href="#" data-href="#section-contact"><div>Contact</div></a></li>--}}

{{--                        </ul>--}}



{{--                        <!-- Top Cart--}}

{{--                        ============================================= -->--}}

{{--                         <div id="top-cart">--}}

{{--                            <a href="{{URL::to('')}}/shop/cartview" id="top-cart-trigger"><i class="icon-shopping-cart"></i><span>{{$cartTotalQuantity = Cart::getTotalQuantity()}}</span></a>--}}

{{--                            <div class="top-cart-content">--}}

{{--                                <div class="top-cart-title">--}}

{{--                                    <h4>Shopping Cart</h4>--}}

{{--                                </div>--}}
{{--                          <?php $cartcollection=Cart::getContent(); ?>--}}
{{--                                @foreach ($cartcollection as $product)--}}

{{--                                <div class="top-cart-items">--}}

{{--                                    <div class="top-cart-item clearfix">--}}

{{--                                        <div class="top-cart-item-image">--}}

{{--                                            <a href="#"><img src="{{asset('storage/products/'.$product->image) }}" alt="Blue Round-Neck Tshirt" /></a>--}}

{{--                                        </div>--}}

{{--                                        <div class="top-cart-item-desc">--}}

{{--                                            <a href="#">{{$product->name}}</a>--}}

{{--                                            <span class="top-cart-item-price">{{$product->price}}</span>--}}

{{--                                            <span class="top-cart-item-quantity">x {{$product->quantity}}</span>--}}

{{--                                        </div>--}}

{{--                                    </div>--}}



{{--                                </div>--}}
{{--                                @endforeach--}}
{{--                                <div class="top-cart-action clearfix">--}}

{{--                                    <span class="fleft top-checkout-price">${{Cart::getSubTotal()}}</span>--}}

{{--                                    <a href="{{URL::to('')}}/shop/cartview" class="button button-3d button-small nomargin fright" >View Cart</a>--}}

{{--                                </div>--}}

{{--                            </div>--}}

{{--                        </div><!-- #top-cart end -->--}}




{{--                        <!-- Top Search--}}

{{--                        ============================================= -->--}}

{{--                        <div id="top-search">--}}

{{--                            <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>--}}

{{--                            <form action="search.html" method="get">--}}

{{--                                <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">--}}

{{--                            </form>--}}

{{--                        </div><!-- #top-search end -->--}}



{{--                    </nav><!-- #primary-menu end -->--}}



{{--                </div>--}}



{{--            </div>--}}



{{--        </header><!-- #header end -->--}}

{{--        <!-- Page Title--}}

{{--		============================================= -->--}}

{{--        <section id="page-title">--}}



{{--            <div class="container clearfix">--}}

{{--                <h1>Shop</h1>--}}

{{--                <span>Start Buying your Favourite Theme</span>--}}

{{--                <ol class="breadcrumb">--}}

{{--                    <li class="breadcrumb-item"><a href="#">Home</a></li>--}}

{{--                    <li class="breadcrumb-item active" aria-current="page">Shop</li>--}}

{{--                </ol>--}}

{{--            </div>--}}



{{--        </section><!-- #page-title end -->--}}



{{--        <!-- Content--}}

{{--        ============================================= -->--}}

{{--        <section id="content">--}}



{{-- <div class="content-wrap">--}}

{{--                <div class="container clearfix">--}}

{{--                    <!-- Post Content--}}
{{--                    ============================================= -->--}}
{{--                    <div class="postcontent nobottommargin col_last" id="category_search">--}}

{{--                        <!-- Shop--}}
{{--                        ============================================= -->--}}
{{--                        <div id="shop" class="shop product-3 grid-container clearfix">--}}
{{--                                @if(!empty('all_products'))--}}

{{--                                  @foreach($all_products as $product)--}}

{{--                                  <div id="hide_row" class="product sf-dress clearfix">--}}
{{--                                <div class="product-image">--}}
{{--                                    <a href="{{URL::to('')}}/shop/single/{{$product->id}}"><img src="{{asset('storage/products/'.$product->main_image) }}" alt="Checked Short Dress"></a>--}}
{{--                                    <a href="{{URL::to('')}}/shop/single/{{$product->id}}"><img src="{{asset('storage/products/'.$product->main_image) }}" alt="Checked Short Dress"></a>--}}
{{--                                    <div class="sale-flash">50% Off*</div>--}}
{{--                                    <div class="product-overlay">--}}
{{--                                        <a href="{{URL::to('')}}/shop/add_to_cart/{{$product->id}}" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>--}}
{{--                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="product-desc center">--}}
{{--                                    <div class="product-title"><h3><a href="#">Checked Short Dress</a></h3></div>--}}
{{--                                    <div class="product-price"><del>$24.99</del>--}}
{{--                                        <ins>--}}
{{--                                            @foreach ($product->product_attributes as $resource)--}}
{{--                                                {{ $resource->unit_price }}--}}
{{--                                          @endforeach--}}
{{--                                       </ins>--}}
{{--                                   </div>--}}
{{--                                    <div class="product-rating">--}}
{{--                                        <i class="icon-star3"></i>--}}
{{--                                        <i class="icon-star3"></i>--}}
{{--                                        <i class="icon-star3"></i>--}}
{{--                                        <i class="icon-star3"></i>--}}
{{--                                        <i class="icon-star-half-full"></i>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                           @endforeach--}}

{{--                        @endif--}}

{{--                        </div><!-- #shop end -->--}}
{{--                       {{ $all_products->links() }}--}}
{{--                    </div><!-- .postcontent end -->--}}



{{--                    <!-- Sidebar--}}
{{--                    ============================================= -->--}}
{{--                    <div class="sidebar nobottommargin">--}}
{{--                        <div class="sidebar-widgets-wrap">--}}

{{--                            <div class="widget widget-filter-links clearfix">--}}

{{--                                <h4>Select Category</h4>--}}
{{--                                <ul class="custom-filter" data-container="#shop" data-active-class="active-filter">--}}
{{--                                    <li class="widget-filter-reset active-filter"><a href="#" data-filter="*">Clear</a></li>--}}
{{--                                    @foreach($all_categories as $categories)--}}
{{--                                    <li  ><a href="#" id="{{$categories->id}}" class="category_search" data-filter=".sf-dress">{{$categories->name}}</a></li>--}}
{{--                                    @endforeach--}}
{{--                                </ul>--}}

{{--                            </div>--}}
{{--                             <div class="widget widget-filter-links clearfix">--}}

{{--                                <h4>Select Color</h4>--}}
{{--                                <ul class="custom-filter" data-container="#shop" data-active-class="active-filter">--}}
{{--                                    <li class="widget-filter-reset active-filter"><a href="#" data-filter="*">Clear</a></li>--}}
{{--                                    @if(!empty('all_colors'))--}}
{{--                                    @foreach($all_colors as $color)--}}
{{--                                    <li  ><a href="#" id="{{$color->id}}" class="color_search" data-filter=".sf-dress">{{$color->name}}</a></li>--}}
{{--                                    @endforeach--}}
{{--                                    @endif--}}
{{--                                </ul>--}}

{{--                            </div>--}}


{{--                        </div>--}}
{{--                    </div><!-- .sidebar end -->--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section><!-- #content end -->--}}
{{--    </div>--}}
{{--    <script>--}}

{{--        $(document).ready(function(){--}}
{{--  $(".category_search").on("click",function(){--}}

{{--    var category_id = $(this).attr('id');--}}

{{--    $.ajax({--}}
{{--                method: 'get',--}}
{{--                url: '{{ URL::route('shop.search_by_category','category_id') }}',--}}
{{--                datatype:'json',--}}
{{--                data: {'category_id' : category_id},--}}
{{--                success: function(response){--}}

{{--                 $("#shop").hide();--}}
{{--                 var html ='';--}}
{{--                     html +='<div  class="shop product-3 grid-container clearfix">';--}}
{{--		       $.each(response,function(value,data){--}}




{{--		       	 html +='<div class="product sf-dress clearfix">';--}}
{{--                         html +='<div class="product-image">';--}}
{{--                         html +='<a href="#"><img src="storage/products/'+data.main_image+'" alt="Checked Short Dress"></a>';--}}
{{--                         html +=' <a href="#"><img src="storage/products/'+data.main_image+'" alt="Checked Short Dress"></a>';--}}
{{--                         html +='<div class="sale-flash">50% Off*</div>';--}}
{{--                         html +='<div class="product-overlay">';--}}
{{--                         html +=' <a href="{{URL::to('')}}/shop/add_to_cart/'+data.id+'" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>';--}}
{{--                         html +='<a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>';--}}
{{--                         html +='<a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>';--}}
{{-- 			             html +='</div>';--}}
{{--                         html +='</div>';--}}
{{--                         html +='<div class="product-desc center">';--}}
{{--                         html +='<div class="product-title"><h3><a href="#">'+data.name+'</a></h3></div>';--}}
{{--                         html +='<div class="product-price"><del>$24.99</del>';--}}
{{--                         $.each(data.product_attributes,function (val,mydata) {--}}
{{--                         html +='<ins>$'+mydata.unit_price+'</ins>';--}}
{{--                         });--}}
{{--                         html +='</div>';--}}
{{--                         html +='<div class="product-rating">';--}}
{{--                         html +=' <i class="icon-star3"></i>';--}}
{{--                         html +='<i class="icon-star3"></i>';--}}
{{--                         html +='<i class="icon-star3"></i>';--}}
{{--                         html +='<i class="icon-star3"></i>';--}}
{{--                         html +='<i class="icon-star-half-full"></i>';--}}
{{--                         html +='</div>';--}}
{{--                         html +='</div>';--}}
{{--                         html +='</div>';--}}


{{--		       });--}}
{{--                       html +='</div>';--}}


{{--		       $( "#category_search" ).html( html );--}}
{{--                },--}}
{{--               });--}}

{{--  });--}}


{{--            $(".color_search").on("click",function(){--}}

{{--         var  color_id = $(this).attr('id');--}}
{{--         $.ajax({--}}

{{--                method: 'get',--}}
{{--                url: '{{ URL::route('shop.search_by_color','color_id') }}',--}}
{{--                datatype:'json',--}}
{{--                data: {'color_id' : color_id},--}}
{{--                success: function(response){--}}

{{--                    $("#shop").hide();--}}
{{--                 var html ='';--}}
{{--                     html +='<div  class="shop product-3 grid-container clearfix">';--}}
{{--		       $.each(response,function(value,data){--}}


{{--                console.log(data.products.main_image);--}}





{{--		       	 html +='<div class="product sf-dress clearfix">';--}}
{{--                         html +='<div class="product-image">';--}}
{{--                         html +='<a href="#"><img src="storage/products/'+data.main_image+'" alt="Checked Short Dress"></a>';--}}
{{--                         html +=' <a href="#"><img src="storage/products/'+data.main_image+'" alt="Checked Short Dress"></a>';--}}
{{--                         html +='<div class="sale-flash">50% Off*</div>';--}}
{{--                         html +='<div class="product-overlay">';--}}
{{--                         html +='<a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>';--}}
{{--                         html +='<a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>';--}}
{{-- 			 html +='</div>';--}}
{{--                         html +='</div>';--}}
{{--                         html +='<div class="product-desc center">';--}}
{{--                         html +='<div class="product-title"><h3><a href="#">'+data.name+'</a></h3></div>';--}}
{{--                         html +='<div class="product-price"><del>$24.99</del>';--}}
{{--                         html +='<ins>$221</ins>';--}}
{{--                         html +='</div>';--}}
{{--                         html +='<div class="product-rating">';--}}
{{--                         html +=' <i class="icon-star3"></i>';--}}
{{--                         html +='<i class="icon-star3"></i>';--}}
{{--                         html +='<i class="icon-star3"></i>';--}}
{{--                         html +='<i class="icon-star3"></i>';--}}
{{--                         html +='<i class="icon-star-half-full"></i>';--}}
{{--                         html +='</div>';--}}
{{--                         html +='</div>';--}}
{{--                         html +='</div>';--}}


{{--		       });--}}
{{--                       html +='</div>';--}}


{{--		       $( "#category_search" ).html( html );--}}

{{--            },--}}
{{--         });--}}
{{--    });--}}
{{--});--}}


{{--        </script>--}}

{{--@endsection--}}
{{-->>>>>>> 28df7d3a93604457270ac32c44459b19fa01513b--}}
