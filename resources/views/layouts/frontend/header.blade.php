                             @extends('frontend.master')

                             @section('content')

                                                <div id="wrapper" class="clearfix">



                                                    <!-- Header

                                                    ============================================= -->

                                                    <header id="header" class="full-header">



                                                        <div id="header-wrap">



                                                            <div class="container clearfix">



                                                                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>



                                                                <!-- Logo

                                                                ============================================= -->

                                                                <div id="logo">

                                                                    <a href="index.html" class="standard-logo" data-dark-logo="{{ asset('frontend_assets/assets/logo.png') }}"><img src="{{ asset('frontend_assets/assets/logo.png') }}" alt="Amza Logo"></a>

                                                                    <a href="index.html" class="retina-logo" data-dark-logo="{{ asset('frontend_assets/images/logo-dark2x.png') }}"><img src="{{ asset('frontend_asset/images/logo@2x.png') }}" alt="Amza Logo"></a>

                                                                </div><!-- #logo end -->



                                                                <!-- Primary Navigation

                                                                ============================================= -->

                                                                <nav id="primary-menu">



                                                                    <ul>

                                                                        <li><a href="#" data-href="#home"><div>Home</div></a></li>

                                                                        <li><a href="#" data-href="#section-about"><div>About</div></a></li>

                                                                        <li><a href="#" data-href="#section-team"><div>Team</div></a></li>

                                                                        <li><a href="#" data-href="#section-pricing"><div>Packages</div></a></li>

                                                                        <li><a href="blog.html"><div>Blog</div></a></li>

                                                                        <li><a href="#" data-href="#section-testimonials" data-offset="60"><div>Testimonials</div></a></li>

                                                                        <li><a href="#" data-href="#section-faqs"><div>Faqs</div></a></li>

                                                                        <li><a href="#" data-href="#section-services"><div>Services</div></a></li>

                                                                        <li><a href="#" data-href="#section-contact"><div>Contact</div></a></li>

                                                                    </ul>



                                                                    <!-- Top Cart

                                                                    ============================================= -->

                                                                    <div id="top-cart">

                                                                        <a href="{{URL::to('')}}/shop/cartview" id="top-cart-trigger"><i class="icon-shopping-cart"></i><span>{{$cartTotalQuantity = Cart::getTotalQuantity()}}</span></a>

                                                                        <div class="top-cart-content">

                                                                            <div class="top-cart-title">

                                                                                <h4>Shopping Cart</h4>

                                                                            </div>
                                                                      <?php $cartcollection=Cart::getContent(); ?>
                                                                            @foreach ($cartcollection as $product)

                                                                            <div class="top-cart-items">

                                                                                <div class="top-cart-item clearfix">

                                                                                    <div class="top-cart-item-image">

                                                                                        <a href="#"><img src="{{asset('storage/products/'.$product->image) }}" alt="Blue Round-Neck Tshirt" /></a>

                                                                                    </div>

                                                                                    <div class="top-cart-item-desc">

                                                                                        <a href="#">{{$product->name}}</a>

                                                                                        <span class="top-cart-item-price">{{$product->price}}</span>

                                                                                        <span class="top-cart-item-quantity">x {{$product->quantity}}</span>

                                                                                    </div>

                                                                                </div>



                                                                            </div>
                                                                            @endforeach
                                                                            <div class="top-cart-action clearfix">

                                                                                <span class="fleft top-checkout-price">${{Cart::getSubTotal()}}</span>

                                                                                <a href="{{URL::to('')}}/shop/cartview" class="button button-3d button-small nomargin fright" >View Cart</a>

                                                                            </div>

                                                                        </div>

                                                                    </div><!-- #top-cart end -->



                                                                    <!-- Top Search

                                                                    ============================================= -->

                                                                    <div id="top-search">

                                                                        <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>

                                                                        <form action="search.html" method="get">

                                                                            <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">

                                                                        </form>

                                                                    </div><!-- #top-search end -->



                                                                </nav><!-- #primary-menu end -->



                                                            </div>



                                                        </div>



                                                    </header><!-- #header end -->

                                                    <!-- Page Title

                                                            ============================================= -->



                                            <section id="page-title">

                                                                    <div class="container clearfix">
                                                                            <h1>Cart</h1>
                                                                            <ol class="breadcrumb">
                                                                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                                                                    <li class="breadcrumb-item"><a href="#">Shop</a></li>
                                                                                    <li class="breadcrumb-item active" aria-current="page">Cart</li>
                                                                            </ol>
                                                                    </div>

                                                            </section><!-- #page-title end -->

                                                   @endsection