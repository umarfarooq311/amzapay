<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>Amzapay</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <!--end::Web font -->

    <!--begin::Global Theme Styles -->
    <link href="{{asset('vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css"/>

    <!--RTL version:<link href="assets/vendors/base/vendors.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="{{asset('demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css"/>

    <!--RTL version:<link href="assets/demo/default/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->
    <link href="{{asset('vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles -->

    <link href="{{asset('redactor/redactor.min.css')}}" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles -->

    <!--end::Page Vendors Styles -->
    <link rel="icon" href="{{ asset('frontend_assets/assets/logo-large.png') }}" type="image/gif" sizes="16x16">

    @yield('style')
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">

    <!-- BEGIN: Header -->
    <!-- BEGIN: Header -->
    <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">

                <!-- BEGIN: Brand -->
                <div class="m-stack__item m-brand  m-brand--skin-dark ">
                    <div class="m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
                            <a href="{{ route('home') }}" class="m-brand__logo-wrapper">
                                <img alt="Amzapay" src="{{asset('frontend_assets/assets/logo-white.png')}}" width="100">
                            </a>
                        </div>
                        <div class="m-stack__item m-stack__item--middle m-brand__tools">

                            <!-- BEGIN: Left Aside Minimize Toggle -->
                            <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                            <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Responsive Header Menu Toggler -->
                            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                <span></span>
                            </a>

                            <!-- END -->

                            <!-- BEGIN: Topbar Toggler -->
                            <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                <i class="flaticon-more"></i>
                            </a>

                            <!-- BEGIN: Topbar Toggler -->
                        </div>
                    </div>
                </div>

                <!-- END: Brand -->
                <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

                    <!-- BEGIN: Horizontal Menu -->
                    <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

                    <!-- BEGIN: Topbar -->
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click" aria-expanded="true">
                                    <a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
                                                    <img src="../../frontend_assets/assets/dummy_user.png" class="m--img-rounded m--marginless" alt="">
												</span>
                                        <span class="m-topbar__username m--hide"> </span>
                                    </a>
                                    <div class="m-dropdown__wrapper" style="z-index: 101;">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 12.5px;">{{ Sentinel::getUser()->user_name }}</span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__header m--align-center" style="background: url({{asset('frontend_assets/assets/dummy_user.png')}}); background-size: cover;">
                                                <div class="m-card-user m-card-user--skin-dark">
                                                    <div class="m-card-user__pic">
                                                        <img src="../../frontend_assets/assets/dummy_user.png" class="m--img-rounded m--marginless" alt="">
                                                    </div>
                                                    <div class="m-card-user__details">
                                                        <span class="m-card-user__name m--font-weight-500">{{ Sentinel::getUser()->user_name }}</span>
                                                        <a href="" class="m-card-user__email m--font-weight-300 m-link">{{ Sentinel::getUser()->email }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav m-nav--skin-light">
                                                        <li class="m-nav__section m--hide">
                                                            <span class="m-nav__section-text">Section</span>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="{{ url('profile') }}" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                <span class="m-nav__link-title">
																			<span class="m-nav__link-wrap">
																				<span class="m-nav__link-text">My Profile</span>
																			</span>
                                                                </span>
                                                            </a>
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="{{ url('setting/change-password') }}" class="m-nav__link">
                                                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                <span class="m-nav__link-title">
																			<span class="m-nav__link-wrap">
																				<span class="m-nav__link-text">Change Password</span>
																			</span>
                                                                </span>
                                                            </a>
                                                        </li>

                                                        <li class="m-nav__separator m-nav__separator--fit">
                                                        </li>
                                                        <li class="m-nav__item">
                                                            <a href="javascript:void(0);" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                                {{ csrf_field() }}
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!-- END: Topbar -->
                </div>
            </div>
        </div>
    </header>

    <!-- END: Header -->
@php

    $slug = Sentinel::getUser()->roles()->first()->slug;

@endphp
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
                    class="la la-close"></i></button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

            <!-- BEGIN: Aside Menu -->
            <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                 m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

                    @if($slug == 'user')

                        <li class="m-menu__item"><a href="{{ url('dashboard') }}" class="m-menu__link">

                                <i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Dashboard</span></a>

                        </li>

                        <li class="m-menu__item"><a href="{{ url('package-list') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-business"></i><span class="m-menu__link-text">Package</span></a>

                        </li>

                        <li class="m-menu__item"><a href="{{ url('deposit') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-piggy-bank"></i><span class="m-menu__link-text">Deposit</span></a>

                        </li>

                        <li class="m-menu__item"><a href="{{ url('withdraw') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-paper-plane"></i><span class="m-menu__link-text">Withdrawal</span></a>

                        </li>

                        <li class="m-menu__item"><a href="{{ url('invest') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-statistics"></i><span class="m-menu__link-text">Invest</span></a>

                        </li>

                        <li class="m-menu__item"><a href="{{ url('my-network') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-network"></i><span class="m-menu__link-text">Referrals</span></a>

                        </li>

                        <li class="m-menu__item"><a href="{{ url('history/commission') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-stopwatch"></i><span class="m-menu__link-text">History</span></a>

                        </li>

                    <!-- <li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom" aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-link-redirect="1" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="History"><a href="{{ url('history/commission') }}:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-stopwatch"></i><span

					 class="m-menu__link-text">History</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>

				<div class="m-menu__submenu "><span class="m-menu__arrow"></span>

					<ul class="m-menu__subnav">

						<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom" aria-haspopup="true" m-menu-link-redirect="1"><span class="m-menu__link"><span class="m-menu__link-text">History</span></span></li>

						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('history/deposit') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Deposit History</span></a></li>

						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('history/commission') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Commission History</span></a></li>



						<li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('history/withdraw') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">withdraw History</span></a></li>

					</ul>

				</div>

			</li> -->

                        <li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom-2" aria-haspopup="true" m-menu-submenu-toggle="click"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-settings"></i><span

                                        class="m-menu__link-text">Settings</span><i class="m-menu__ver-arrow la la-angle-top"></i></a>

                            <div class="m-menu__submenu m-menu__submenu--up"><span class="m-menu__arrow"></span>

                                <ul class="m-menu__subnav">

                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('profile') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Profile</span></a></li>

                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('setting/change-password') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Change Password</span></a></li>

                                </ul>

                            </div>

                        </li>
                        <li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom-1" aria-haspopup="true" m-menu-submenu-toggle="click"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-info"></i><span class="m-menu__link-text">Help</span><i

                                        class="m-menu__ver-arrow la la-angle-right"></i></a>

                            <div class="m-menu__submenu m-menu__submenu--up"><span class="m-menu__arrow"></span>

                                <ul class="m-menu__subnav">

                                    <li class="m-menu__item  m-menu__item--parent m-menu__item--bottom-1" aria-haspopup="true"><span class="m-menu__link"><span class="m-menu__link-text">Help</span></span></li>

                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('/') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Support</span></a></li>

                                    <!-- <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="#" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Blog</span></a></li> -->

                                    <!-- <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="#" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Terms</span></a></li> -->

                                </ul>

                            </div>

                        </li>

                    @elseif($slug == 'admin')

                        <li class="m-menu__item" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="Dashboard"><a href="{{ url('admin-dashboard') }}" class="m-menu__link">

                                <i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-text">Dashboard</span></a>

                        </li>

                        <li class="m-menu__item"><a href="{{ url('users-manage') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-users"></i><span class="m-menu__link-text">User Management</span></a>

                        </li>

                        <li class="m-menu__item"><a href="{{ url('withdraw-manage') }}" class="m-menu__link"><i class="m-menu__link-icon flaticon-paper-plane"></i><span class="m-menu__link-text">Withdrawal</span></a>

                        </li>

                        <li class="m-menu__item"><a href="https://dashboard.tawk.to/?lang=en#/dashboard" class="m-menu__link" target="_blank"><i class="m-menu__link-icon flaticon-speech-bubble-1"></i><span class="m-menu__link-text">Tawl Chat</span></a>

                        </li>

                        <li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom" aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-link-redirect="1" data-toggle="m-tooltip" data-placement="right" title="" data-original-title="History"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-stopwatch"></i><span

                                        class="m-menu__link-text">History</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>

                            <div class="m-menu__submenu "><span class="m-menu__arrow"></span>

                                <ul class="m-menu__subnav">

                                    <li class="m-menu__item  m-menu__item--parent m-menu__item--bottom" aria-haspopup="true" m-menu-link-redirect="1"><span class="m-menu__link"><span class="m-menu__link-text">History</span></span></li>

                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('deposit-history') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Deposit History</span></a></li>



                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('withdraw-history') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Witdraw History</span></a></li>

                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('invest-history') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">Invest History</span></a></li>

                                </ul>

                            </div>

                        </li>



                        <li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom-2" aria-haspopup="true" m-menu-submenu-toggle="click" ><a href="javascript:viod(0);" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-settings"></i><span

                                        class="m-menu__link-text">Settings</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>

                            <div class="m-menu__submenu m-menu__submenu--up"><span class="m-menu__arrow"></span>

                                <ul class="m-menu__subnav">

                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('company-setting') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Company Detials</span></a></li>

                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ url('myprofile') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Profile</span></a></li>

                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="password-change" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Change Password</span></a></li>

                                </ul>

                            </div>

                        </li>

                        <li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom-2" aria-haspopup="true" m-menu-submenu-toggle="click"><a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-home"></i><span

                                        class="m-menu__link-text">Store</span><i class="m-menu__ver-arrow la la-angle-top"></i></a>

                            <div class="m-menu__submenu m-menu__submenu--up"><span class="m-menu__arrow"></span>

                                <ul class="m-menu__subnav">

                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('category.index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Categories</span></a></li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('sub-categories.index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Sub Categories</span></a></li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('colors.index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Colors</span></a></li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('sizes.index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Sizes</span></a></li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('products.index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Products</span></a></li>
                                    <li class="m-menu__item " aria-haspopup="true" m-menu-link-redirect="1"><a href="{{ route('product.reviews.index') }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Product Reviews</span></a></li>

                                </ul>

                            </div>

                        </li>

                    @endif

                </ul>
            </div>

            <!-- END: Aside Menu -->
        </div>

        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            @yield('content')
        </div>
    </div>
    <!-- begin::Footer -->
    <footer class="m-grid__item		m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
					<span class="m-footer__copyright">
						@php echo date('Y') @endphp &copy; All rights reserved by <a href="javascript:void(0)" class="m-link">Amzapay</a>
					</span>
                </div>
            </div>
        </div>
    </footer>
    <!-- end::Footer -->
</div>

<script src="{{asset('vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
<!--end::Global Theme Bundle -->
<script src="{{asset('vendors/custom/datatables/datatables.bundle.js')}}" type="text/javascript"></script>
<!--end::Page Vendors -->
<script src="{{asset('redactor/redactor.min.js')}}"></script>
{{--<script src="{{asset('js/select2.js')}}"></script>--}}
<!--end::Page Scripts -->
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {

                $('#img').attr('src', e.target.result);

                $('#img').css("display","block");
                $('#hidden-field').val('');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function confirmDelete() {
        var r = confirm("Are you sure you want to perform this action");
        if (r === true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
@stack('script')
</body>
</html>