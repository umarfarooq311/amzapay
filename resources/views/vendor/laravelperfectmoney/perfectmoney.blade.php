@extends('layouts.back.master_new')



@section('title') Dashboard | Amzapay @endsection

@section('content')





	<div class="m-content">

		<div class="col-xl-4">

			<div class="m-portlet m-portlet--mobile">



				<div class="m-portlet__head">



					<div class="m-portlet__head-caption">



						<div class="m-portlet__head-title">



							<h3 class="m-portlet__head-text">



								Investing Amount



							</h3>



						</div>



					</div>



				</div>







				<div class="m-portlet__body">



					<div class="row">



						<form action="https://perfectmoney.is/api/step1.asp" method="POST">

							<input type="hidden" name="PAYEE_ACCOUNT" value="{{ $PAYEE_ACCOUNT }}">

							<input type="hidden" name="PAYEE_NAME" value="{{ $PAYEE_NAME }}">

							<div class="form-group">

								<label>Enter Amount</label><span>(Minimun Amount 100)</span>

								<input type="number"  min="100" placeholder="100" name="PAYMENT_AMOUNT" value="{{ $PAYMENT_AMOUNT }}" class="form-control">

							</div>

							<input type="hidden" name="PAYMENT_UNITS" value="{{ $PAYMENT_UNITS }}">

							<input type="hidden" name="PAYMENT_URL" value="{{ $PAYMENT_URL }}">

							<input type="hidden" name="NOPAYMENT_URL" value="{{ $NOPAYMENT_URL }}">

							@if($PAYMENT_ID)

								<input type="hidden" name="PAYMENT_ID" value="{{ $PAYMENT_ID }}">

							@endif

							@if($STATUS_URL)

								<input type="hidden" name="STATUS_URL" value="{{ $STATUS_URL }}">

							@endif

							@if($PAYMENT_URL_METHOD)

								<input type="hidden" name="PAYMENT_URL_METHOD" value="{{ $PAYMENT_URL_METHOD }}">

							@endif

							@if( $NOPAYMENT_URL_METHOD )

								<input type="hidden" name="NOPAYMENT_URL_METHOD" value="{{ $NOPAYMENT_URL_METHOD }}">

							@endif



							@if( $MEMO )

								<input type="hidden" name="SUGGESTED_MEMO" value="{{ $MEMO }}">

							@endif

							<div class="from-group">

								<input type="submit" class="btn btn-sm btn-primary" value="Proceed">

							</div>

						</form>



					</div>



				</div>



			</div>

		</div>



	</div>

@endsection
