@extends('layouts.back.master_new')



@section('title') Purchase History | Amzapay @endsection



@section('style')



@endsection



@section('content')

    <div class="m-subheader ">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="m-subheader__title m-subheader__title--separator">Purchase History</h3>

                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

                    <li class="m-nav__item m-nav__item--home">

                        <a href="{{ url('/admin-dashboard') }}" class="m-nav__link m-nav__link--icon">

                            <i class="m-nav__link-icon la la-home"></i>

                        </a>

                    </li>

                    <li class="m-nav__separator">-</li>

                    <li class="m-nav__item">

                        <a href="" class="m-nav__link">

                            <span class="m-nav__link-text">Purchase History</span>

                        </a>

                    </li>

                </ul>

            </div>

        </div>

    </div>

    <div class="m-content">

        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">

                <div class="m-portlet__head-caption">

                    <div class="m-portlet__head-title">

                        <h3 class="m-portlet__head-text">

                            Purchase History

                        </h3>

                    </div>

                </div>

            </div>

            <div class="m-portlet__body">

                <div class="table-responsive">

                    <!--begin: Datatable -->

                    <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">

                        <thead>

                        <tr>

                            <th>#</th>
                            <th>Hide</th>

                            <th>User Name</th>

                            <th>Package Name</th>

                            <th>Amount</th>

                            <th>Type</th>

                            <th>Duration</th>

                            <th>Status</th>

                            <th>Experience Date</th>

                        </tr>

                        </thead>

                        <tbody>

                        @php $i = 0; @endphp

                        @foreach($invest as $inves)

                            <tr>

                                <td>{{ $i++ }}</td>
                                @if($inves->is_hide == 0)
                                    <td><a href="{{ route('hide.purchase', $inves->id) }}"><button class="btn btn-primary btn-sm">Hide</button></a></td>
                                @else
                                    <td><a href="{{ route('show.purchase', $inves->id) }}"><button class="btn btn-danger btn-sm">Show</button></a></td>
                                @endif

                                <td>{{ ucfirst($inves->invest_user->user_name) }}</td>

                                <td>{{ $inves->package->title }}</td>

                                <td>{{ $inves->amount }}</td>

                                <td>{{ strtoupper($inves->type) }}</td>

                                <td>{{ $inves->package->duration }} weeks</td>

                                <td>@if($inves->final_status == 0)<span class="badge badge-warning">Pending</span>@elseif($inves->final_status == 1)<span class="badge badge-success">Complete</span>@endif</td>

                                <td>{{ $inves->valid_date }}</td>

                            </tr>

                        @endforeach

                        </tbody>

                    </table>

                </div>

            </div>


        </div>

    </div>

@endsection



@push('script')

    <script type="text/javascript">

        $(document).ready(function() {

            $('#m_table_1').DataTable();

        } );

    </script>

@endpush