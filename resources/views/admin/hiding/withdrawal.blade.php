@extends('layouts.back.master_new')



@section('title') Withdrawal History | Amzapay @endsection



@section('style')



@endsection



@section('content')



    <div class="m-subheader ">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="m-subheader__title m-subheader__title--separator">History</h3>

                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

                    <li class="m-nav__item m-nav__item--home">

                        <a href="#" class="m-nav__link m-nav__link--icon">

                            <i class="m-nav__link-icon la la-home"></i>

                        </a>

                    </li>

                    <li class="m-nav__separator">-</li>

                    <li class="m-nav__item">

                        <a href="" class="m-nav__link">

                            <span class="m-nav__link-text">Withdrawal History</span>

                        </a>

                    </li>

                </ul>

            </div>

        </div>

    </div>



    <div class="m-content">

        <div class="m-portlet m-portlet--mobile">

            <div class="m-portlet__head">

                <div class="m-portlet__head-caption">

                    <div class="m-portlet__head-title">

                        <h3 class="m-portlet__head-text">

                            Withdrawal History

                        </h3>

                    </div>

                </div>

            </div>

            <div class="m-portlet__body">

                <div class="table-responsive">

                    <!--begin: Datatable -->

                    <table class="table table-striped- table-bordered table-hover table-checkable text-center" id="m_table_1">

                        <thead>

                        <tr>

                            <th>#</th>

                            <th>Hide</th>

                            <th>Name</th>

                            <th>coin</th>

                            <th>Type</th>

                            <th>Address | Account no</th>

                            <th>Amount</th>

                            <th>Transaction id</th>

                            <th>Status</th>

                            <th>Date</th>

                        </tr>

                        </thead>

                        <tbody>

                        @php $i=1; @endphp

                        @foreach($withdraw as $withd)

                            <tr>

                                <td>{{ $i++ }}</td>

                                @if($withd->is_hide == 0)
                                    <td><a href="{{ route('hide.withdrawal', $withd->id) }}"><button class="btn btn-primary btn-sm">Hide</button></a></td>
                                @else
                                    <td><a href="{{ route('show.withdrawal', $withd->id) }}"><button class="btn btn-danger btn-sm">Show</button></a></td>
                                @endif

                                <td>{{ ucfirst($withd->withdr_user->user_name) }}</td>

                                <td>@if($withd->coin == 'COINBASE' || $withd->coin == 'PERFECT' ) ROI @else{{ strtoupper($withd->coin) }}@endif</td>

                                <td>@if($withd->partner_email)

                                        <span class="m-badge m-badge--info m-badge--wide" >Withdraw for partner</span>

                                    @elseif($withd->type == 'perfect_money')

                                        <span class="m-badge m-badge--focus m-badge--wide" >Perfect Money</span>

                                    @elseif($withd->type == 'skrill')

                                        <span class="m-badge m-badge--primary m-badge--wide" >Skrill</span>

                                    @elseif($withd->type == 'Perfect Money Roi')

                                        <span class="m-badge m-badge--perfect-roi m-badge--wide" >Perfect Money Roi</span>

                                    @elseif($withd->type == 'Coinbase Roi')

                                        <span class="m-badge m-badge--coinbase-roi m-badge--wide" >Coinbase Roi</span>

                                    @else

                                        <span class="m-badge m-badge--success m-badge--wide" >coinbase</span>

                                    @endif

                                </td>

                                <td>@if($withd->type == 'partner'){{ $withd->partner_email }} @else{{ $withd->address }}@endif</td>

                                <td>{{ $withd->amount }}</td>

                                <td>@if($withd->txid == Null) N/A @else{{ $withd->txid }}@endif</td>

                                <td>@if($withd->status == 0)<span class="m-badge m-badge--primary m-badge--wide"><i class="fas fa-spinner"></i> Pending</span>

                                    @elseif($withd->status == 1)<span class="m-badge m-badge--success m-badge--wide"><i class="fa fa-check"></i> Complete</span>

                                    @elseif($withd->status == 2)<span class="m-badge m-badge--danger m-badge--wide"><i class="fa fa-times"></i> Reject</span>

                                    @endif

                                </td>

                                <td>{{ $withd->created_at->format('d M Y') }}</td>

                            </tr>

                        @endforeach

                        </tbody>

                    </table>

                </div>

            </div>

        </div>

    </div>

@endsection



@push('script')

    <script type="text/javascript">

        $(document).ready(function() {

            $('#m_table_1').DataTable();

        } );

    </script>

@endpush