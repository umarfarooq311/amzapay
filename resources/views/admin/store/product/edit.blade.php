@extends('layouts.back.master_new')



@section('title') Store | Products @endsection



@section('style')



@endsection



@section('content')



    <div class="m-subheader ">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="m-subheader__title m-subheader__title--separator">Store</h3>

                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">

                    <li class="m-nav__item m-nav__item--home">

                        <a href="#" class="m-nav__link m-nav__link--icon">

                            <i class="m-nav__link-icon la la-home"></i>

                        </a>

                    </li>

                    <li class="m-nav__separator">-</li>

                    <li class="m-nav__item">

                        <a href="" class="m-nav__link">

                            <span class="m-nav__link-text">Product</span>

                        </a>

                    </li>

                </ul>

            </div>

        </div>

    </div>



    <div class="m-content">
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                        <h3 class="m-portlet__head-text">
                            Create New Product
                        </h3>
                    </div>
                </div>
            </div>
        @include('admin.notifications.errors')
        <!--begin::Form-->
            <form class="m-form m-form--fit m-form--label-align-right" action="{{ route('products.update', $product->id) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="m-portlet__body">
                    <div class="form-group m-form__group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control m-input m-input--solid" placeholder="Enter Product Name" name="product_name" value="{{ $product->name }}">
                    </div>

                    <div class="form-group m-form__group">
                        <label for="">Category</label>
                        <select name="category" class="form-control m-input m-input--solid" id="category">
                            @if(!empty($categories))
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            @else
                                <option value="">No Category Available</option>
                            @endif
                        </select>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="">Sub Category</label>
                        <select name="sub_category" class="form-control m-input m-input--solid" id="sub-category">
                            @foreach($sub_categories as $sub_category)
                                <option value="{{ $sub_category->id }}" {{ ($sub_category->id == $product->sub_category_id) }}>{{ $sub_category->name }}</option>
                           @endforeach
                        </select>
                    </div>

                    <div id="product-attributes-container" style="padding: 20px;">
                        <h4>Product Attributes</h4>
                        @foreach($product->product_attributes as $rec)
                            <div class="row">
                            <div class="form-group col-sm-3">
                                <label for="exampleInputEmail1">Size</label>
                                <select name="sizes[]" class="form-control m-input m-input--solid" id="sizes">
                                    @if(!empty($sizes))
                                        @foreach($sizes as $size)
                                            <option value="{{ $size->id }}" {{ ($rec->size_id == $size->id) ? 'selected' : '' }}>{{ $size->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group col-sm-3">
                                <label for="exampleInputEmail1">Color</label>
                                <select name="colors[]" class="form-control m-input m-input--solid" id="colors">
                                    @if(!empty($colors))
                                        @foreach($colors as $color)
                                            <option value="{{ $color->id }}" {{ ($rec->color_id == $color->id) ? 'selected' : '' }}>{{ $color->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group col-sm-2">
                                <label for="exampleInputEmail1">Quantity</label>
                                <input type="number" class="form-control m-input m-input--solid" placeholder="quantity" name="quantity[]" value="{{ $rec->quantity }}">
                            </div>

                            <div class="form-group col-sm-2">
                                <label for="exampleInputEmail1">Unit Price</label>
                                <input type="number" class="form-control m-input m-input--solid" placeholder="100" name="unit_price[]" value="{{ $rec->unit_price }}">
                            </div>
                            @if($loop->last)
                                <div class="from-group col-sm-2">
                                    <button class="btn btn-success btn-sm" style="margin-top:27px;" id="add-more-attribute">Add</button>
                                </div>
                            @endif
                        </div>
                        @endforeach
                    </div>

                    <div class="form-group m-form__group">
                        <label for="exampleInputEmail1">Bulk</label><br>
                        <input type="radio" class="m-input m-input--solid" value="yes" name="bulk" onchange="showBulkPrices()" {{ ($product->is_bulk == 1) ? 'checked' : '' }}> Yes
                        <input type="radio" class="m-input m-input--solid" value="no" name="bulk" onchange="showBulkPrices()" {{ ($product->is_bulk == 0) ? 'checked' : '' }}> No
                    </div>

                    <div id="bulk-price-container" style="display: {{ ($product->is_bulk == 1) ? 'block' : 'none' }}; padding: 20px;">
                        <h4>Bulk Prices</h4>
                        @if(!empty($product->bulk_products))
                            @foreach($product->bulk_products as $rec)
                            <div class="row">
                                <div class="form-group col-sm-3">
                                    <label for="exampleInputEmail1">From</label>
                                    <input type="number" class="form-control m-input m-input--solid" placeholder="Unit From" name="from[]" value="{{ $rec->from }}">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label for="exampleInputEmail1">To</label>
                                    <input type="number" class="form-control m-input m-input--solid" placeholder="Unit To" name="to[]" value="{{ $rec->to }}">
                                </div>

                                <div class="form-group col-sm-3">
                                    <label for="exampleInputEmail1">Bulk Unit Price</label>
                                    <input type="Number" class="form-control m-input m-input--solid" placeholder="Bulk Unit Price" name="price[]" value="{{ $rec->price }}">
                                </div>
                                @if($loop->last)
                                    <div class="from-group col-sm-3">
                                        <button class="btn btn-success btn-sm" style="margin-top:27px;" id="add-more">Add</button>
                                    </div>
                                @endif
                            </div>
                            @endforeach
                        @endif
                    </div>

                    <div class="form-group m-form__group">
                        <label for="exampleInputEmail1">Feature</label><br>
                        <input type="radio" class="m-input m-input--solid" value="1" name="featured" {{ ($product->is_feature == 1) ? "checked" : '' }}> Yes
                        <input type="radio" class="m-input m-input--solid" value="0" name="featured" {{ ($product->is_feature == 0) ? "checked" : '' }}> No
                    </div>

                    <div class="form-group m-form__group">
                        <label for="editor">Description</label>
                        <textarea name="description" id="editor">{{ $product->description }}</textarea>
                    </div>

                    <div class="form-group m-form__group">
                        <label for="exampleInputEmail1">Main Image</label><br>
                        <input type="file" class="m-input m-input--solid" name="main_image" onchange="readURL(this)">
                        <img src="{{ ($product->main_image) ? asset('storage/products/'.$product->main_image) : '' }}" alt="main image" id="img" style="height: 100px; width: 100px; display: {{ ($product->main_image) ? 'block' : 'none' }}">
                    </div>

                    <div class="form-group m-form__group">
                        <label for="exampleInputEmail1">Gallery Image</label><br>
                        <input type="file" class="m-input m-input--solid" name="gallery_images[]" id="gallery-photo-add" style="padding: 0px; cursor: pointer" multiple>
                        <div class="gallery">
                            @if(!empty($product->gallery_images))
                                @foreach(json_decode($product->gallery_images) as $galley_image)
                                    <img width="100" height="100" class="img-thumbnail" style="display:block;" id="img" src="{{ URL::asset('storage/products/'.$galley_image)}}"
                                         alt="main image"/>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-success">Update</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

    </div>

@endsection



@section('script')

    <script type="text/javascript">
        $(document).ready(function() {
            $('#m_table_1').DataTable();
            //$("#editor").redactor();

            //bulk price add more
            $(document).ready(function() {
                var max_fields      = 10; //maximum input boxes allowed
                var wrapper   		= $("#bulk-price-container"); //Fields wrapper

                var x = 1; //initlal text box count
                $("#add-more").click(function(e){ //on add input button click
                    e.preventDefault();
                    if(x < max_fields){ //max input box allowed
                        x++; //text box increment
                        $(wrapper).append(' <div class="row">\n' +
                            '<div class="form-group col-sm-3">\n' +
                            '    <label for="exampleInputEmail1">From</label>\n' +
                            '    <input type="number" class="form-control m-input m-input--solid" placeholder="Unit From" name="from[]">\n' +
                            '</div>\n' +
                            '\n' +
                            '<div class="form-group col-sm-3">\n' +
                            '    <label for="exampleInputEmail1">To</label>\n' +
                            '    <input type="number" class="form-control m-input m-input--solid" placeholder="Unit To" name="to[]">\n' +
                            '</div>\n' +
                            '\n' +
                            '<div class="form-group col-sm-3">\n' +
                            '     <label for="exampleInputEmail1">Bulk Unit Price</label>\n' +
                            '     <input type="Number" class="form-control m-input m-input--solid" placeholder="Bulk Unit Price" name="price[]">\n' +
                            '</div>\n' +
                            '\n' +
                            '<a href="#" class="remove_field" style="margin-top:27px;">x</a>' +
                            '</div>');}
                });

                $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                    e.preventDefault(); $(this).parent('div').remove(); x--;
                })

                //product attributes
                var max_fields_attributes = 10; //maximum input boxes allowed
                var wrapper_attribute = $("#product-attributes-container"); //Fields wrapper
                var y = 1; //initlal text box count
                $("#add-more-attribute").click(function(e){ //on add input button click
                    e.preventDefault();
                    if(y < max_fields_attributes){ //max input box allowed
                        y++; //text box increment
                        $(wrapper_attribute).append('<div class="row">\n' +
                            '                            <div class="form-group col-sm-3">\n' +
                            '                                <label for="exampleInputEmail1">Size</label>\n' +
                            '                                <select name="sizes[]" class="form-control m-input m-input--solid" id="sizes">\n' +
                            '                                    @if(!empty($sizes))\n' +
                            '                                        @foreach($sizes as $size)\n' +
                            '                                            <option value="{{ $size->id }}">{{ $size->name }}</option>\n' +
                            '                                        @endforeach\n' +
                            '                                    @endif\n' +
                            '                                </select>\n' +
                            '                            </div>\n' +
                            '\n' +
                            '                            <div class="form-group col-sm-3">\n' +
                            '                                <label for="exampleInputEmail1">Color</label>\n' +
                            '                                <select name="colors[]" class="form-control m-input m-input--solid" id="colors">\n' +
                            '                                    @if(!empty($colors))\n' +
                            '                                        @foreach($colors as $color)\n' +
                            '                                            <option value="{{ $color->id }}">{{ $color->name }}</option>\n' +
                            '                                        @endforeach\n' +
                            '                                    @endif    \n' +
                            '                                </select>\n' +
                            '                            </div>\n' +
                            '\n' +
                            '                            <div class="form-group col-sm-2">\n' +
                            '                                <label for="exampleInputEmail1">Quantity</label>\n' +
                            '                                <input type="number" class="form-control m-input m-input--solid" placeholder="Unit To" name="quantity[]">\n' +
                            '                            </div>\n' +
                            '\n' +
                            '                            <div class="form-group col-sm-2">\n' +
                            '                                <label for="exampleInputEmail1">Unit Price</label>\n' +
                            '                                <input type="number" class="form-control m-input m-input--solid" placeholder="100" name="unit_price[]">\n' +
                            '                            </div>\n' +
                            '\n' +
                            '<a href="#" class="remove_field" style="margin-top:27px;">x</a>' +
                            '                        </div>');}
                });

                $(wrapper_attribute).on("click",".remove_field", function(e){ //user click on remove text
                    e.preventDefault(); $(this).parent('div').remove(); x--;
                })
            });

            $("#category").change( function(){
                var cat_id = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ route('getSubCategories') }}",
                    data: { data: cat_id},
                    dataType: 'JSON',
                    cache: true,
                    success: function(response){
                        if(response.status == 'success'){
                            html = '';
                            $(response.data).each( function(i,d){
                                html += '<option value="'+ d.id +'">'+ d.name +'</option>';
                            });

                            $("#sub-category").html(html);
                        }else{
                            toastr.warning('No sub category belong to this category');
                        }
                    }
                });
            })

            $(function() {
                var imagesPreview = function(input, placeToInsertImagePreview) {
                    if (input.files) {
                        var filesAmount = input.files.length;
                        for (i = 0; i < filesAmount; i++) {
                            var reader = new FileReader();
                            reader.onload = function(event) {
                                $($.parseHTML('<img style="width=100px;height: 100px;margin: 10px">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            }
                            reader.readAsDataURL(input.files[i]);
                        }
                    }
                };

                $('#gallery-photo-add').on('change', function() {
                    $(".gallery").html('');
                    imagesPreview(this, 'div.gallery');
                });
            });
        });

        //show the bulk prices
        function showBulkPrices(){
            var is_bulk = $("input[name = 'bulk']:checked").val();
            if(is_bulk === "yes"){
                $("#bulk-price-container").show();
            }else{
                $("#bulk-price-container").hide();
            }
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img').attr('src', e.target.result);
                    $('#img').css("display","block");
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        function confirmDelete() {
            var r = confirm("Are you sure you want to perform this action");
            if (r === true) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>

@endsection