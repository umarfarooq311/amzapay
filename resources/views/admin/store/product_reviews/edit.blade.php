@extends('layouts.back.master_new')

@section('title') Reviews | Amzapay @endsection


@section('content')

    <div class="m-subheader ">



        <div class="d-flex align-items-center">



            <div class="mr-auto">



                <h3 class="m-subheader__title m-subheader__title--separator">Store</h3>



                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">



                    <li class="m-nav__item m-nav__item--home">



                        <a href="#" class="m-nav__link m-nav__link--icon">



                            <i class="m-nav__link-icon la la-home"></i>



                        </a>



                    </li>



                    <li class="m-nav__separator">-</li>



                    <li class="m-nav__item">



                        <a href="" class="m-nav__link">



                            <span class="m-nav__link-text">Product Reviews</span>



                        </a>



                    </li>



                </ul>



            </div>



        </div>



    </div>







    <div class="m-content">

        <div class="m-portlet m-portlet--tab">

            <div class="m-portlet__head">

                <div class="m-portlet__head-caption">

                    <div class="m-portlet__head-title">

												<span class="m-portlet__head-icon m--hide">

													<i class="la la-gear"></i>

												</span>

                        <h3 class="m-portlet__head-text">

                            Update Review

                        </h3>

                    </div>

                </div>

            </div>

        @include('admin.notifications.errors')

        <!--begin::Form-->

            <form class="m-form m-form--fit m-form--label-align-right" action="{{ route('product.reviews.update', $review->id) }}" method="post">

                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="m-portlet__body">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputEmail1">Product</label>
                                <select name="product_id" class="form-control m-input m-input--solid">
                                    <option value="">--Select Product--</option>
                                    @if(!empty($products))
                                        @foreach($products as $key => $product)
                                            <option value="{{ $key }}" {{ ($key == $review->product_id) ? 'selected' : '' }}>{{ $product }}</option>
                                        @endforeach
                                    @else
                                        <option value="">No Product Available</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="text" class="form-control m-input m-input--solid"  placeholder="Enter Username" value="{{ $review->username }}" name="username">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputEmail1">Rating(Out of 5)</label>
                                <input type="number" min="1" max="5" name="rating" class="form-control" value="{{ $review->rating }}">
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group m-form__group">
                                <label for="exampleInputEmail1">Review</label>
                                <textarea name="review" cols="30" rows="10" class="form-control">{{ $review->review }}</textarea>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">

                    <div class="m-form__actions">

                        <button type="submit" class="btn btn-success">Update</button>

                        <button type="reset" class="btn btn-secondary">Cancel</button>

                    </div>

                </div>

            </form>



            <!--end::Form-->

        </div>



    </div>



@endsection







@section('script')



    <script type="text/javascript">

        $(document).ready(function() {

            $('#m_table_1').DataTable();

        } );

    </script>



@endsection